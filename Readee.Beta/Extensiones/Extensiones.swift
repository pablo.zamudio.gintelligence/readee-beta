//
//  Extensiones.swift
//  Readee.Beta
//
//  Created by Pablo Luis Velazquez Zamudio on 22/05/20.
//  Copyright © 2020 P.Z.Scorpion. All rights reserved.
//

import Foundation
import UIKit

struct AppUtility {

    static func lockOrientation(_ orientation: UIInterfaceOrientationMask) {

        if let delegate = UIApplication.shared.delegate as? AppDelegate {
            delegate.orientationLock = orientation
        }
    }

    /// OPTIONAL Added method to adjust lock and rotate to the desired orientation
    static func lockOrientation(_ orientation: UIInterfaceOrientationMask, andRotateTo rotateOrientation:UIInterfaceOrientation) {

        self.lockOrientation(orientation)

        UIDevice.current.setValue(rotateOrientation.rawValue, forKey: "orientation")
        UINavigationController.attemptRotationToDeviceOrientation()
    }

}


extension UILabel {
    
    func customLabel() {
        
        textColor = UIColor.white
        font = UIFont.systemFont(ofSize: 19)
        adjustsFontSizeToFitWidth = true
        backgroundColor = UIColor.systemBlue
        textAlignment = NSTextAlignment.center
        
    }
    
}

extension UIScrollView {

    var isAtTop: Bool {
        return contentOffset.y <= verticalOffsetForTop
    }

    var isAtBottom: Bool {
        return contentOffset.y >= verticalOffsetForBottom
    }

    var verticalOffsetForTop: CGFloat {
        let topInset = contentInset.top
        return -topInset
    }

    var verticalOffsetForBottom: CGFloat {
        let scrollViewHeight = bounds.height
        let scrollContentSizeHeight = contentSize.height
        let bottomInset = contentInset.bottom
        let scrollViewBottomOffset = scrollContentSizeHeight + bottomInset - scrollViewHeight
        return scrollViewBottomOffset
    }

        
}

extension UIView {
    
    func bounce() {
        
        UIView.animate(withDuration: 0.1, animations: {
            self.transform = CGAffineTransform(scaleX: 1.1, y: 1.1)
        }) { (completion) in
            UIView.animate(withDuration: 0.1, animations: {
                self.transform = .identity
            })
        }
        
    }
    
    func bounceTap() {
        
    UIView.animate(withDuration: 0.6, animations: {
               self.transform = CGAffineTransform(scaleX: 1.1, y: 1.1)
           }) { (completion) in
               UIView.animate(withDuration: 0.1, animations: {
                   self.transform = .identity
               })
           }
        
    }
    
    
    func StopBounce() {
    
            UIView.animate(withDuration: 0, animations: {
                self.transform = .identity
            })
    }
}
