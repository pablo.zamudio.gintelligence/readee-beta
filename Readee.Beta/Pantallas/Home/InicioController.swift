//
//  InicioController.swift
//  Readee.Beta
//
//  Created by Pablo Luis Velazquez Zamudio on 22/05/20.
//  Copyright © 2020 P.Z.Scorpion. All rights reserved.
//

import UIKit
import ImageIO
import SwiftGifOrigin
import CoreData

class InicioController: UIViewController, UIScrollViewDelegate {

    @IBOutlet var scrollLoader: [UIActivityIndicatorView]!
    @IBOutlet weak var principalScroll: UIScrollView!
    @IBOutlet weak var carruselScroll: UIScrollView!
    @IBOutlet weak var masNuevoScroll: UIScrollView!
    @IBOutlet weak var segundoScroll: UIScrollView!
    @IBOutlet weak var tercerScroll: UIScrollView!
    @IBOutlet weak var cuartoScroll: UIScrollView!
    @IBOutlet weak var quintoScroll: UIScrollView!
    @IBOutlet weak var sextoScroll: UIScrollView!
    @IBOutlet weak var septimoScroll: UIScrollView!
    @IBOutlet weak var octavoScroll: UIScrollView!
    @IBOutlet weak var novenoScroll: UIScrollView!
    @IBOutlet weak var decimoScroll: UIScrollView!
    @IBOutlet weak var onceavoScroll: UIScrollView!
    @IBOutlet weak var doceavoScroll: UIScrollView!
    @IBOutlet weak var treceavoScroll: UIScrollView!
    @IBOutlet weak var segundoTitulo: UILabel!
    @IBOutlet weak var tercerLabel: UILabel!
    @IBOutlet weak var cuartoLabel: UILabel!
    @IBOutlet weak var quintoLabel: UILabel!
    @IBOutlet weak var sextoLabel: UILabel!
    @IBOutlet weak var septimoLabel: UILabel!
    @IBOutlet weak var octavoLabel: UILabel!
    @IBOutlet weak var novenoLabel: UILabel!
    @IBOutlet weak var decimoLabel: UILabel!
    @IBOutlet weak var onceavo: UILabel!
    @IBOutlet weak var doceavoLabel: UILabel!
    @IBOutlet weak var treceavoLabel: UILabel!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var loaderView: UIView!
    @IBOutlet weak var gifLoader: UIImageView!
    
    
    var navBar = UINavigationBar()
    var fondo1 = UIImageView()
    
    // MARK: - FUNCION CRUD CORE DATA
    
    func conexion()-> NSManagedObjectContext {
        let delegate = UIApplication.shared.delegate as! AppDelegate
        return delegate.persistentContainer.viewContext
    }
    
    //  MARK: - ELEMENTOS LO MAS VISTO
    
    var datosID = 0
    var botonTag = 0
    let add = 1
    
    // ELEMENTOS TE RECOMENDAMOS
    
    var datosID2 = 0
    var botonTag2 = 0
    
    // ELEMENTOS LO MAS VISTO
    
    var datosID3 = 0
    var botonTag3 = 0
    
    var datosID4 = 0
    var botonTag4 = 0
    
    var datosID5 = 0
    var botonTag5 = 0
    
    var datosID6 = 0
    var botonTag6 = 0
    
    var datosID7 = 0
    var botonTag7 = 0
    
    var datosID8 = 0
    var botonTag8 = 0
    
    var datosID9 = 0
    var botonTag9 = 0
    
    var datosID10 = 0
    var botonTag10 = 0
    var datosID11 = 0
    var botonTag11 = 0
    
    var datosID12 = 0
    var botonTag12 = 0
    
    var datosID13 = 0
    var botonTag13 = 0
    
    // MARK: - VARIABLES GLOBALES
    
    var fechaRevista : String!
    var fechaRevista2 : String!
    var catID : Int!
    var hiloID = ""
    
    // MARK: - VARIBALES ID
    var idCarrusel = false
    var p = 0
    var idScroll = 0
    var idAsync = 0
    var idAsync2 = 0
    var idAsync3 = 0
    var idAsync4 = 0
    var idAsync5 = 0
    var idAsync6 = 0
    var idAsync7 = 0
    var idAsync8 = 0
    var idAsync9 = 0
    var idAsync10 = 0
    var idAsync11 = 0
    var idAsync12 = 0
    var idAsync13 = 0
    
    
    // MARK: - VARIABLES DE ARRAYS LO MAS NUEVO
    var arrayImage : [String] = Array()
    var revistaTitulo : [String] = Array()
    var arrayDescripcion : [String] = Array()
    var arrayEtiquetas : [String] = Array()
    var arrayIDs : [String] = Array()
    var urlImg : [String] = Array()
    var imagenes : [UIImageView] = Array()
    var tags : [NSArray] = Array()
    var arrayF : [String] = Array()
    var arrayTipo : [String] = Array()
    
    // MARK: - VARIABLES DE ARRAYS TERECOMENDAMOS
    var arrayImage2 : [String] = Array()
    var revistaTitulo2 : [String] = Array()
    var arrayDescripcion2 : [String] = Array()
    var arrayEtiquetas2 : [String] = Array()
    var arrayIDs2 : [String] = Array()
    var urlImg2 : [String] = Array()
    var imagenes2 : [UIImageView] = Array()
    var tags2 : [NSArray] = Array()
    var arrayF2 : [String] = Array()
    var arrayTipo2 : [String] = Array()
    
    // MARK: - VARIBLES DE ARRAYS LO MAS VISTO
    var arrayImage3 : [String] = Array()
    var revistaTitulo3 : [String] = Array()
    var arrayDescripcion3 : [String] = Array()
    var arrayEtiquetas3 : [String] = Array()
    var arrayIDs3 : [String] = Array()
    var urlImg3 : [String] = Array()
    var imagenes3 : [UIImageView] = Array()
    var tags3 : [NSArray] = Array()
    var arrayF3 : [String] = Array()
    var arrayTipo3 : [String] = Array()
    
    var arrayImage4 : [String] = Array()
    var revistaTitulo4 : [String] = Array()
    var arrayDescripcion4 : [String] = Array()
    var arrayEtiquetas4 : [String] = Array()
    var arrayIDs4 : [String] = Array()
    var urlImg4 : [String] = Array()
    var imagenes4 : [UIImageView] = Array()
    var tags4 : [NSArray] = Array()
    var arrayF4 : [String] = Array()
    var arrayTipo4 : [String] = Array()
    
    var arrayImage6 : [String] = Array()
    var revistaTitulo6 : [String] = Array()
    var arrayDescripcion6 : [String] = Array()
    var arrayEtiquetas6 : [String] = Array()
    var arrayIDs6 : [String] = Array()
    var urlImg6 : [String] = Array()
    var imagenes6 : [UIImageView] = Array()
    var tags6 : [NSArray] = Array()
    var arrayF6 : [String] = Array()
    var arrayTipo6 : [String] = Array()
    
    var arrayImage5 : [String] = Array()
    var revistaTitulo5 : [String] = Array()
    var arrayDescripcion5 : [String] = Array()
    var arrayEtiquetas5 : [String] = Array()
    var arrayIDs5 : [String] = Array()
    var urlImg5 : [String] = Array()
    var imagenes5 : [UIImageView] = Array()
    var tags5 : [NSArray] = Array()
    var arrayF5 : [String] = Array()
    var arrayTipo5 : [String] = Array()
    
    var arrayImage7 : [String] = Array()
    var revistaTitulo7 : [String] = Array()
    var arrayDescripcion7 : [String] = Array()
    var arrayEtiquetas7 : [String] = Array()
    var arrayIDs7 : [String] = Array()
    var urlImg7 : [String] = Array()
    var imagenes7 : [UIImageView] = Array()
    var tags7 : [NSArray] = Array()
    var arrayF7 : [String] = Array()
    var arrayTipo7 : [String] = Array()
    
    var arrayImage8 : [String] = Array()
    var revistaTitulo8 : [String] = Array()
    var arrayDescripcion8 : [String] = Array()
    var arrayEtiquetas8 : [String] = Array()
    var arrayIDs8 : [String] = Array()
    var urlImg8 : [String] = Array()
    var imagenes8 : [UIImageView] = Array()
    var tags8 : [NSArray] = Array()
    var arrayF8 : [String] = Array()
    var arrayTipo8 : [String] = Array()
    
    var arrayImage9 : [String] = Array()
    var revistaTitulo9 : [String] = Array()
    var arrayDescripcion9 : [String] = Array()
    var arrayEtiquetas9 : [String] = Array()
    var arrayIDs9 : [String] = Array()
    var urlImg9 : [String] = Array()
    var imagenes9 : [UIImageView] = Array()
    var tags9 : [NSArray] = Array()
    var arrayF9 : [String] = Array()
    var arrayTipo9 : [String] = Array()
    
    var arrayImage10 : [String] = Array()
    var revistaTitulo10 : [String] = Array()
    var arrayDescripcion10 : [String] = Array()
    var arrayEtiquetas10 : [String] = Array()
    var arrayIDs10 : [String] = Array()
    var urlImg10 : [String] = Array()
    var imagenes10 : [UIImageView] = Array()
    var tags10 : [NSArray] = Array()
    var arrayF10 : [String] = Array()
    var arrayTipo10 : [String] = Array()
    
    var arrayImage11 : [String] = Array()
    var revistaTitulo11 : [String] = Array()
    var arrayDescripcion11 : [String] = Array()
    var arrayEtiquetas11 : [String] = Array()
    var arrayIDs11 : [String] = Array()
    var urlImg11 : [String] = Array()
    var imagenes11 : [UIImageView] = Array()
    var tags11 : [NSArray] = Array()
    var arrayF11 : [String] = Array()
    var arrayTipo11 : [String] = Array()
    
    var arrayImage12 : [String] = Array()
    var revistaTitulo12 : [String] = Array()
    var arrayDescripcion12 : [String] = Array()
    var arrayEtiquetas12 : [String] = Array()
    var arrayIDs12 : [String] = Array()
    var urlImg12 : [String] = Array()
    var imagenes12 : [UIImageView] = Array()
    var tags12 : [NSArray] = Array()
    var arrayF12 : [String] = Array()
    var arrayTipo12 : [String] = Array()
    
    var arrayImage13 : [String] = Array()
    var revistaTitulo13 : [String] = Array()
    var arrayDescripcion13 : [String] = Array()
    var arrayEtiquetas13 : [String] = Array()
    var arrayIDs13 : [String] = Array()
    var urlImg13 : [String] = Array()
    var imagenes13 : [UIImageView] = Array()
    var tags13 : [NSArray] = Array()
    var arrayF13 : [String] = Array()
    var arrayTipo13 : [String] = Array()
    
    
    // ELEMENTOS PRIMER SCROLL
    var ejeX : Double = 20
    var ejexCorner : Double = 123.9
    var ejeLabelX : Double = 28
    var ejeXFechaR : Double = 20
    let z : Double = 210
    
    
    // ELEMENTOS SEGUNDO SCROLL
    var ejeX2 : Double = 20
    var ejeLabelX2 : Double = 28
    var ejexCorner2 : Double = 123.9
    var ejeXFechaR2 : Double = 20
    let a : Double = 210
    let add2 = 1
    
    // ELEMENTOS TERCER SCROLL
    var ejeX3 : Double = 20
    var ejeLabelX3 : Double = 28
    var ejexCorner3 : Double = 123.9
    var ejeXFechaR3 : Double = 20
    let b: Double = 210
    let add3 = 1
    
    // ELEMENTOS CUARTO SCROLL
    var ejeX4 : Double = 20
    var ejeLabelX4 : Double = 28
    var ejexCorner4 : Double = 123.9
    var ejeXFechaR4 : Double = 20
    let c: Double = 210
    let add4 = 1
    
    // ELEMENTOS QUINTO SCROLL
    var ejeX5 : Double = 20
    var ejeLabelX5 : Double = 28
    var ejexCorner5 : Double = 123.9
    var ejeXFechaR5 : Double = 20
    let d: Double = 210
    let add5 = 1
    
    // ELEMENTOS SEXTO SCROLL
    var ejeX6 : Double = 20
    var ejeLabelX6 : Double = 28
    var ejexCorner6 : Double = 123.9
    var ejeXFechaR6 : Double = 20
    let e: Double = 210
    let add6 = 1
    
    // ELEMENTOS SEPTIMO SCROLL
    var ejeX7 : Double = 20
    var ejeLabelX7 : Double = 28
    var ejexCorner7 : Double = 123.9
    var ejeXFechaR7 : Double = 20
    let f: Double = 210
    let add7 = 1
    
    // ELEMENTOS OCTAVO SCROLL
    var ejeX8 : Double = 20
    var ejeLabelX8 : Double = 28
    var ejexCorner8 : Double = 123.9
    var ejeXFechaR8 : Double = 20
    let g: Double = 210
    let add8 = 1
    
    // ELEMENTOS NOVENO SCROLL
    var ejeX9 : Double = 20
    var ejeLabelX9 : Double = 28
    var ejexCorner9 : Double = 123.9
    var ejeXFechaR9 : Double = 20
    let h: Double = 210
    let add9 = 1
    
    // ELEMENTOS DECIMO SCROLL
    var ejeX10 : Double = 20
    var ejeLabelX10 : Double = 28
    var ejexCorner10 : Double = 123.9
    var ejeXFechaR10 : Double = 20
    let i: Double = 210
    let add10 = 1
    
    // ELEMENTOS ONCEAVO SCROLL
    var ejeX11 : Double = 20
    var ejeLabelX11 : Double = 28
    var ejexCorner11 : Double = 123.9
    var ejeXFechaR11 : Double = 20
    let j: Double = 210
    let add11 = 1
    
    // ELEMENTOS DOCEAVO SCROLL
    var ejeX12 : Double = 20
    var ejeLabelX12 : Double = 28
    var ejexCorner12 : Double = 123.9
    var ejeXFechaR12 : Double = 20
    let k: Double = 210
    let add12 = 1
    
    // ELEMENTOS TRECEAVO SCROLL
    var ejeX13 : Double = 20
    var ejeLabelX13 : Double = 28
    var ejexCorner13 : Double = 123.9
    var ejeXFechaR13 : Double = 20
    let l: Double = 210
    let add13 = 1
    
    let  backGroundGray = UIColor(red: 33/255, green: 33/255, blue: 33/255, alpha: 1)
    var isScrollFinish : String!
    
    // MARK: - VARIABLE SINGLETON
    
    static let singleton = InicioController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        SetUpNavigationBar()
        idScroll = 0
        principalScroll.delegate = self
        
        let heightScrollPrincipal3 = cuartoScroll.frame.maxY
        
        UIView.animate(withDuration: 0.3, animations: {
            self.masNuevoScroll.alpha = 1
            self.segundoScroll.alpha = 1
            self.tercerScroll.alpha = 1
            self.cuartoScroll.alpha = 1
        })
        
        scrollLoader.forEach { (loader1) in
            loader1.isHidden = true
        }
        Timer.scheduledTimer(timeInterval: 4, target: self, selector: #selector(moveToNextPage), userInfo: nil, repeats: true)
        self.gifLoader.loadGif(name: "loader_readee")
        
        // MARK: - CONTENIDO DE DATOS
    principalScroll.contentSize = CGSize(width: 375, height: heightScrollPrincipal3)
        
        self.tabBarController?.tabBar.backgroundColor = UIColor.init(red: 15/255, green: 15/255, blue: 15/255, alpha: 1)
        
        sizeScreen()
        Home()
        Timer.scheduledTimer(withTimeInterval: 10 , repeats: false){ (timer) in
            
            UIView.animate(withDuration: 0.3, animations: {
                
                self.loaderView.alpha = 0
                
            })
            
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        AppUtility.lockOrientation(.portrait)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
       AppUtility.lockOrientation(.all)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "TOINFOREVISTA" {
            let destino = segue.destination as! RevistaInfoController
            destino.queryParamID = arrayIDs[self.datosID]
        }else if segue.identifier == "TOINFOREVISTA2" {
            let destino = segue.destination as! RevistaInfoController
            destino.queryParamID = arrayIDs2[self.datosID2]
        }else if segue.identifier == "TOINFOREVISTA3" {
            let destino = segue.destination as! RevistaInfoController
            destino.queryParamID = arrayIDs3[self.datosID3]
        }else if segue.identifier == "TOINFOREVISTA4" {
            let destino = segue.destination as! RevistaInfoController
            destino.queryParamID = arrayIDs4[self.datosID4]
        }else if segue.identifier == "TOINFOREVISTA5" {
            let destino = segue.destination as! RevistaInfoController
            destino.queryParamID = arrayIDs5[self.datosID5]
        }else if segue.identifier == "TOINFOREVISTA6" {
            let destino = segue.destination as! RevistaInfoController
            destino.queryParamID = arrayIDs6[self.datosID6]
        }else if segue.identifier == "TOINFOREVISTA7" {
            let destino = segue.destination as! RevistaInfoController
            destino.queryParamID = arrayIDs7[self.datosID7]
        }else if segue.identifier == "TOINFOREVISTA8" {
            let destino = segue.destination as! RevistaInfoController
            destino.queryParamID = arrayIDs8[self.datosID8]
        }else if segue.identifier == "TOINFOREVISTA9" {
            let destino = segue.destination as! RevistaInfoController
            destino.queryParamID = arrayIDs9[self.datosID9]
        }else if segue.identifier == "TOINFOREVISTA10" {
            let destino = segue.destination as! RevistaInfoController
            destino.queryParamID = arrayIDs10[self.datosID10]
        }else if segue.identifier == "TOINFOREVISTA11" {
            let destino = segue.destination as! RevistaInfoController
            destino.queryParamID = arrayIDs11[self.datosID11]
        }else if segue.identifier == "TOINFOREVISTA12" {
            let destino = segue.destination as! RevistaInfoController
            destino.queryParamID = arrayIDs12[self.datosID12]
        }else if segue.identifier == "TOINFOREVISTA13" {
            let destino = segue.destination as! RevistaInfoController
            destino.queryParamID = arrayIDs13[self.datosID13]
        }
    }
    
    func SetUpNavigationBar() {
        
    navBar = UINavigationBar(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: 80))
    view.addSubview(navBar)
    navBar.barStyle = .black
    let photoButtton = UIButton(type: .system)
    let nameButtton = UILabel(frame: CGRect(x: 60, y: 50, width: 300, height: 21))
    nameButtton.textColor = .systemBlue
    nameButtton.font = UIFont.boldSystemFont(ofSize: 20)
    let imagen = UIImage(named: "cuenta_icon")
    photoButtton.setBackgroundImage(imagen?.withRenderingMode(.alwaysOriginal), for: .normal)
    photoButtton.frame = CGRect(x: 20, y: 40, width: 35, height: 35)
    navigationItem.leftBarButtonItem = UIBarButtonItem(customView: photoButtton)
    let singleton = LoginController.singletonLogin
    nameButtton.text = ("\(singleton.name ?? "ouch")")
    navBar.addSubview(photoButtton)
    navBar.addSubview(nameButtton)
        
    }
    
    // MARK: - FUNCIONES GENERALES
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView){
        // Test the offset and calculate the current page after scrolling ends
        let pageWidth:CGFloat = carruselScroll.frame.width
        let currentPage:CGFloat = floor((carruselScroll.contentOffset.x-pageWidth/2)/pageWidth)+1
        // Change the indicator
        self.pageControl.currentPage = Int(currentPage);
        // Change the text accordingly
        if Int(currentPage) == 0{
            
        }else if Int(currentPage) == 1{
            
        }else if Int(currentPage) == 2{
            
        }else{
            
        }
        
        scrollingFinish()
        
    }
    
    func scrollingFinish() -> Void {
        
        let heightScrollPrincipa4 = novenoScroll.frame.minY
        let heightScrollPrincipa5 = quintoScroll.frame.minY
        
        if principalScroll.isAtBottom {
            if idScroll == 0 {
                self.principalScroll.contentSize = CGSize(width: 375, height: heightScrollPrincipa5)
                self.scrollLoader[0].startAnimating()
                self.scrollLoader[0].isHidden = false
                print("scroll finish!!!")
                
            }
            
            if idScroll == 1 {
                print("scroll finish again!!!")
                self.principalScroll.contentSize = CGSize(width: 375, height: heightScrollPrincipa4)
                self.scrollLoader[1].startAnimating()
                self.scrollLoader[1].isHidden = false
                self.idScroll = 2
                
            }
        }
        
        
    }
    
    @objc func moveToNextPage (){
        
        let pageWidth:CGFloat = self.carruselScroll.frame.width
        let maxWidth:CGFloat = pageWidth * CGFloat(self.arrayImage.count)
        let contentOffset:CGFloat = self.carruselScroll.contentOffset.x
        
        var slideToX = contentOffset + pageWidth
        
        if  contentOffset + pageWidth == maxWidth
        {
            slideToX = 0
        }
        self.carruselScroll.scrollRectToVisible(CGRect(x:slideToX, y:0, width:pageWidth, height:self.carruselScroll.frame.height), animated: true)
    }
    
    func downloadImage(_ uri : String, inView: UIImageView) {
        let url = URL(string: uri)
        
        let task = URLSession.shared.dataTask(with: url!) {responseData,response,error in
            if error == nil {
                if let data = responseData {
                    
                    DispatchQueue.main.async {
                        
                        inView.image = UIImage(data: data)
                        self.DescargaImg()
                        
                    }
                    
                }else {
                    print("no data")
                }
            }else{
                print("error")
            }
        }
        task.resume()
        
    }
    
    func downloadImage2(_ uri : String, inView: UIImageView) {
        let url = URL(string: uri)
        
        let task = URLSession.shared.dataTask(with: url!) {responseData,response,error in
            if error == nil {
                if let data = responseData {
                    
                    DispatchQueue.main.async {
                        
                        inView.image = UIImage(data: data)
                        self.DescargaImg2()
                        
                    }
                    
                }else {
                    print("no data")
                }
            }else{
                print("error")
            }
        }
        task.resume()
    }
    
    func downloadImage3(_ uri : String, inView: UIImageView) {
        let url = URL(string: uri)
        
        let task = URLSession.shared.dataTask(with: url!) {responseData,response,error in
            if error == nil {
                if let data = responseData {
                    DispatchQueue.main.async {
                        inView.image = UIImage(data: data)
                        self.DescargaImg3()
                    }
                }else {
                    print("no data")
                }
            }else{
                print("error")
            }
        }
        task.resume()
    }
        
    
    func downloadImage4(_ uri : String, inView: UIImageView) {
        let url = URL(string: uri)
        
        let task = URLSession.shared.dataTask(with: url!) {responseData,response,error in
            if error == nil {
                if let data = responseData {
                    DispatchQueue.main.async {
                        
                        inView.image = UIImage(data: data)
                        self.DescargaImg4()
                    }
                }else {
                    print("no data")
                }
            }else{
                print("error")
            }
        }
        task.resume()
    }
        
    
    func downloadImage5(_ uri : String, inView: UIImageView) {
        let url = URL(string: uri)
        
        let task = URLSession.shared.dataTask(with: url!) {responseData,response,error in
            if error == nil {
                if let data = responseData {
                    DispatchQueue.main.async {
                        
                        inView.image = UIImage(data: data)
                        self.DescargaImg5()
                        
                    }
                }else {
                    print("no data")
                }
            }else{
                print("error")
            }
        }
        task.resume()
    }
    
    func downloadImage6(_ uri : String, inView: UIImageView) {
        let url = URL(string: uri)
        
        let task = URLSession.shared.dataTask(with: url!) {responseData,response,error in
            if error == nil {
                if let data = responseData {
                    DispatchQueue.main.async {
                        
                        inView.image = UIImage(data: data)
                        self.DescargaImg6()
                    }
                    
                }else {
                    print("no data")
                }
            }else{
                print("error")
                
            }
        }
        task.resume()
    }
    
    func downloadImage7(_ uri : String, inView: UIImageView) {
        let url = URL(string: uri)
        
        let task = URLSession.shared.dataTask(with: url!) {responseData,response,error in
            if error == nil {
                if let data = responseData {
                    DispatchQueue.main.async {
                        inView.image = UIImage(data: data)
                        self.DescargaImg7()
                        
                    }
                }else {
                    print("no data")
                }
            }else{
                print("error")
            }
        }
        task.resume()
    }
        
    
    func downloadImage8(_ uri : String, inView: UIImageView) {
        let url = URL(string: uri)
        
        let task = URLSession.shared.dataTask(with: url!) {responseData,response,error in
            if error == nil {
                if let data = responseData {
                    DispatchQueue.main.async {
                        
                        inView.image = UIImage(data: data)
                        self.DescargaImg8()
                        
                    }
                }else {
                    print("no data")
                }
            }else{
                print("error")
            }
        }
        task.resume()
    }
        
    
    func downloadImage9(_ uri : String, inView: UIImageView) {
        let url = URL(string: uri)
        
        let task = URLSession.shared.dataTask(with: url!) {responseData,response,error in
            if error == nil {
                if let data = responseData {
                    DispatchQueue.main.async {
                        inView.image = UIImage(data: data)
                        self.DescargaImg9()
                        
                    }
                    
                }else{
                    print("no data")
                }
            }else{
                print("error")
            }
        }
        task.resume()
    }
        
    func downloadImage10(_ uri : String, inView: UIImageView) {
        let url = URL(string: uri)
        
        let task = URLSession.shared.dataTask(with: url!) {responseData,response,error in
            if error == nil {
                if let data = responseData {
                    DispatchQueue.main.async {
                        
                        inView.image = UIImage(data: data)
                        self.DescargaImg10()
                    }
                }else {
                    print("no data")
                }
            }else{
                print("error")
            }
        }
        task.resume()
    }
        
    
    func downloadImage11(_ uri : String, inView: UIImageView) {
        let url = URL(string: uri)
        
        let task = URLSession.shared.dataTask(with: url!) {responseData,response,error in
            if error == nil {
                if let data = responseData {
                    DispatchQueue.main.async {
                        
                        inView.image = UIImage(data: data)
                        self.DescargaImg11()
                    }
                }else {
                    print("no data")
                }
            }else{
                print("error")
            }
        }
        task.resume()
    }
    
    func downloadImage12(_ uri : String, inView: UIImageView) {
        let url = URL(string: uri)
        
        let task = URLSession.shared.dataTask(with: url!) {responseData,response,error in
            if error == nil {
                if let data = responseData {
                    DispatchQueue.main.async {
                        
                        inView.image = UIImage(data: data)
                        self.DescargaImg12()
                    }
                }else {
                    print("no data")
                }
            }else{
                print("error")
            }
        }
        task.resume()
    }
        
    
    func downloadImage13(_ uri : String, inView: UIImageView) {
        let url = URL(string: uri)
        
        let task = URLSession.shared.dataTask(with: url!) {responseData,response,error in
            if error == nil {
                if let data = responseData {
                    DispatchQueue.main.async {
                        inView.image = UIImage(data: data)
                        self.DescargaImg13()
                    }
                }else {
                    print("no data")
                }
            }else{
                print("error")
            }
        }
        task.resume()
    }
    
    func downloadImageSlider(_ uri : String, inView: UIImageView) {
        let url = URL(string: uri)
        
        let task = URLSession.shared.dataTask(with: url!) {responseData,response,error in
            if error == nil {
                if let data = responseData {
                    
                    DispatchQueue.main.async {
                        inView.image = UIImage(data: data)
                        
                    }
                }else {
                    print("no data")
                }
            }else{
                print("error")
                self.Home()
            }
        }
        task.resume()
    }
    
    func DescargaImg() {
        
        let count = self.imagenes.count
        
        if self.idAsync < count {
            
            self.downloadImage(urlImg[self.idAsync], inView: imagenes[self.idAsync])
            self.idAsync = self.idAsync + 1
            
        }else{
            
            self.DescargaImg2()
        }
        
    }
    
    func DescargaImg2() {
        
        let count2 = self.imagenes2.count
        if self.idAsync2 < count2 {
            
            self.downloadImage2(urlImg2[self.idAsync2], inView: imagenes2[self.idAsync2])
            self.idAsync2 = self.idAsync2 + 1
            
        }else{
            
            DescargaImg3()
        }
        
    }
        
    
    func DescargaImg3() {
        
        let count3 = self.imagenes3.count
        if self.idAsync3 < count3 {
            
            self.downloadImage3(urlImg3[self.idAsync3], inView: imagenes3[self.idAsync3])
            self.idAsync3 = self.idAsync3 + 1
            
        }else{
            
            self.DescargaImg4()
            
        }
        
    }
    
    func DescargaImg4() {
        let count4 = self.imagenes4.count
        if self.idAsync4 < count4 {
            
            self.downloadImage4(urlImg4[self.idAsync4], inView: imagenes4[self.idAsync4])
            self.idAsync4 = self.idAsync4 + 1
            
        }else{
            
            self.DescargaImg5()
            
        }
    }
    func DescargaImg5() {
        let count5 = self.imagenes5.count
        if self.idAsync5 < count5 {
                        
            self.downloadImage5(urlImg5[self.idAsync5], inView: imagenes5[self.idAsync5])
            
            self.idAsync5 = self.idAsync5 + 1
            
        }else{
            
            self.DescargaImg6()
            
        }
    }
    func DescargaImg6() {
        let count6 = self.imagenes6.count
        if self.idAsync6 < count6 {
            
            UIView.animate(withDuration: 0.3, animations: {
                
                self.scrollLoader[0].stopAnimating()
                self.scrollLoader[0].alpha = 0
                self.quintoLabel.alpha = 1
                self.quintoScroll.alpha = 1
                self.sextoScroll.alpha = 1
                self.septimoScroll.alpha = 1
                self.octavoScroll.alpha = 1
                self.principalScroll.contentSize = CGSize(width: 375, height: self.octavoScroll.frame.maxY)
            })
            
            self.downloadImage6(urlImg6[self.idAsync6], inView: imagenes6[self.idAsync6])
            
            self.idAsync6 = self.idAsync6 + 1
            self.idScroll = 1
            
            
            
        }else{
            
            self.DescargaImg7()
        }
    }
    func DescargaImg7() {
        
        let count7 = self.imagenes7.count
        if self.idAsync7 < count7 {
                        
            self.downloadImage7(urlImg7[self.idAsync7], inView: imagenes7[self.idAsync7])
            
            self.idAsync7 = self.idAsync7 + 1
            
        }else{
            
            self.DescargaImg8()
            
        }
        
    }
    func DescargaImg8() {
        let count8 = self.imagenes8.count
        if self.idAsync8 < count8 {
                        
            self.downloadImage8(urlImg8[self.idAsync8], inView: imagenes8[self.idAsync8])
            
            self.idAsync8 = self.idAsync8 + 1
            
        }else{
            
            self.DescargaImg9()
        }
    }
    func DescargaImg9() {
        let count9 = self.imagenes9.count
        if self.idAsync9 < count9 {
                        
            self.downloadImage9(urlImg9[self.idAsync9], inView: imagenes9[self.idAsync9])
            
            self.idAsync9 = self.idAsync9 + 1
            
        }else{
            
            self.DescargaImg10()
            Timer.scheduledTimer(withTimeInterval: 1 , repeats: false){ (timer) in
                UIView.animate(withDuration: 0.5, animations: {
                    self.scrollLoader[1].stopAnimating()
                    self.scrollLoader[1].alpha = 0
                    self.novenoLabel.alpha = 1
                    self.novenoScroll.alpha = 1
                    self.decimoScroll.alpha = 1
                    self.onceavoScroll.alpha = 1
                    self.doceavoScroll.alpha = 1
                    self.treceavoScroll.alpha = 1
                    self.principalScroll.contentSize = CGSize(width: 375, height: self.treceavoScroll.frame.maxY)
                    
                })
                self.idScroll = 2
            }
            
            
        }
        
        
    }
    func DescargaImg10() {
        let count10 = self.imagenes10.count
        if self.idAsync10 < count10 {
                        
            self.downloadImage10(urlImg10[self.idAsync10], inView: imagenes10[self.idAsync10])
            
            self.idAsync10 = self.idAsync10 + 1
            
        }else{
            
            DescargaImg11()
            
        }
    }
    func DescargaImg11() {
        let count11 = self.imagenes11.count
        if self.idAsync11 < count11 {
                        
            self.downloadImage11(urlImg11[self.idAsync11], inView: imagenes11[self.idAsync11])
            
            self.idAsync11 = self.idAsync11 + 1
            
        }else{
            
            self.DescargaImg12()
            
        }
        
        
    }
    func DescargaImg12() {
        let count12 = self.imagenes12.count
        if self.idAsync12 < count12 {
                        
            self.downloadImage12(urlImg12[self.idAsync12], inView: imagenes12[self.idAsync12])
            
            self.idAsync12 = self.idAsync12 + 1
            
        }else{
            
            self.DescargaImg13()
            
        }
    }
    func DescargaImg13() {
        let count13 = self.imagenes13.count
        if self.idAsync13 < count13 {
                        
            self.downloadImage13(urlImg13[self.idAsync13], inView: imagenes13[self.idAsync13])
            
            self.idAsync13 = self.idAsync13 + 1
            
        }else{
            
            print("FIN DE TODOS LOS HILOS...")
            
        }
        
        
    }
        
        // MARK: - FUCNIONES SERVICIO API REST
        
    
    func Home() {
        
        guard let datos = URL(string: "https://testapi.ginmag.ga/v1/home") else { return }
        var request = URLRequest(url: datos)
        
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        
        URLSession.shared.dataTask(with: request) { (data, response, _) in
            
            do {
                
                let json = try JSONSerialization.jsonObject(with: data!, options: .allowFragments)
                //  print(json)
                
                DispatchQueue.main.async {
                    
                    let revistas : NSDictionary = json as! NSDictionary
                    
                    // CICLO DE REVISTAS
                    for dic in revistas {
                        
                        let keys : String = dic.key as! String
                        let values = dic.value
                        
                        
                        if keys == "Slider" {
                            
                            let scrollViewWidth:CGFloat = self.carruselScroll.frame.width
                            let scrollViewHeight:CGFloat = self.carruselScroll.frame.height
                            var ejex : CGFloat = 0
                            
                            let urlImage : NSArray = values as! NSArray
                            
                            for item in urlImage {
                                
                                let url : String = item as! String
                                self.arrayImage.append(url)
                                
                                
                                let imgOne = UIImageView(frame: CGRect(x:ejex, y:0,width:scrollViewWidth, height:scrollViewHeight))
                                imgOne.contentMode = .scaleToFill
                                ejex = ejex + scrollViewWidth
                                
                                self.downloadImageSlider(url, inView: imgOne)
                                self.carruselScroll.addSubview(imgOne)
                                
                                self.carruselScroll.contentSize = CGSize(width:self.carruselScroll.frame.width * CGFloat(self.arrayImage.count), height:self.carruselScroll.frame.height)
                                self.carruselScroll.delegate = self
                                self.pageControl.currentPage = 0
                                
                            }
                            
                        }
                        
                        if keys == "Lista" {
                            
                            let arrayRevistas : NSArray = values as! NSArray
                            
                            for dic2 in arrayRevistas {
                                
                                let dictionary : NSDictionary = dic2 as! NSDictionary
                                
                                for subDic in dictionary {
                                    
                                    let subKeys : String = subDic.key as! String
                                    let subValues = subDic.value
                                    
                                    let titulo : String = dictionary["titulo"] as! String
                                    if titulo == "Lo más nuevo" {
                                        
                                        if subKeys == "elementos" {
                                            
                                            let arrayElementos : NSArray = subValues as! NSArray
                                            
                                            for element in arrayElementos {
                                                
                                                let dic3 : NSDictionary = element as! NSDictionary
                                                
                                                for i in dic3 {
                                                    
                                                    let subKey : String = i.key as! String
                                                    let subVal = i.value
                                                    
                                                    let mes : Double = dic3["mes"] as! Double
                                                    let año : Double = dic3["year"] as! Double
                                                    
                                                    if subKey == "_id" {
                                                        
                                                        let idRevista : String = subVal as! String
                                                        self.arrayIDs.append(idRevista)
                                                        
                                                    }
                                                    
                                                    if subKey == "imagen" {
                                                        
                                                        let urlImage : String = subVal as! String
                                                        self.urlImg.append(urlImage)
                                                        
                                                        let botonDetalles = UIButton(frame: CGRect(x: self.ejeX, y: 39, width: 175, height: 225))
                                                        
                                                        botonDetalles.tag = self.botonTag
                                                        self.botonTag = self.botonTag + self.add
                                                        let imagenRevista = UIImageView(frame: CGRect(x: self.ejeX, y: 39, width: 175, height: 225))
                                                        let imagenEsquina = UIImageView(frame: CGRect(x: self.ejexCorner, y: 169, width: 71, height: 95))
                                                        imagenRevista.backgroundColor = self.backGroundGray
                                                        imagenEsquina.contentMode = .scaleToFill
                                                        imagenEsquina.image = UIImage(named: "Hoja2")
                                                        imagenRevista.backgroundColor = self.backGroundGray
                                                        self.imagenes.append(imagenRevista)
                                                        self.ejeX = self.ejeX + self.z
                                                        self.ejexCorner = self.ejexCorner + self.z
                                                        let scrollW = self.ejeX
                                                        self.masNuevoScroll.contentSize = CGSize(width: scrollW, height: 272)
                                                        self.masNuevoScroll.addSubview(imagenRevista)
                                                        self.masNuevoScroll.addSubview(imagenEsquina)
                                                        self.masNuevoScroll.addSubview(botonDetalles)
                                                        
                                                        botonDetalles.addTarget(self, action: #selector(self.ClickRevista), for: .touchUpInside)
                                                        
                                                    }
                                                    
                                                    if subKey == "titulo" {
                                                        
                                                        var mesRevista = ""
                                                        var añoRevista = ""
                                                        let tit : String = subVal as! String
                                                        self.revistaTitulo.append(tit)
                                                        
                                                        
                                                        let tituloLabel = UILabel(frame: CGRect(x: self.ejeLabelX, y: 15, width: 130, height: 15))
                                                        self.ejeLabelX = self.ejeLabelX + self.z
                                                        tituloLabel.text = tit
                                                        tituloLabel.textColor = UIColor.white
                                                        tituloLabel.font = UIFont.boldSystemFont(ofSize: 15)
                                                        tituloLabel.adjustsFontSizeToFitWidth = true
                                                        
                                                        let fechaRevista = UILabel(frame: CGRect(x: self.ejeXFechaR, y: 276, width: 130, height: 15))
                                                        
                                                        self.ejeXFechaR = self.ejeXFechaR + self.z
                                                        fechaRevista.textColor = UIColor.white
                                                        fechaRevista.font = UIFont.boldSystemFont(ofSize: 15)
                                                        fechaRevista.adjustsFontSizeToFitWidth = true
                                                        
                                                        if mes == 1 {
                                                            mesRevista = "enero"
                                                        }else if mes == 2 {
                                                            mesRevista = "febrero"
                                                        }else if mes == 3 {
                                                            mesRevista = "marzo"
                                                        }else if mes == 4 {
                                                            mesRevista = "abril"
                                                        }else if mes == 5 {
                                                            mesRevista = "mayo"
                                                        }else if mes == 6 {
                                                            mesRevista = "junio"
                                                        }else if mes == 7 {
                                                            mesRevista = "julio"
                                                        }else if mes == 8 {
                                                            mesRevista = "agosto"
                                                        }else if mes == 9 {
                                                            mesRevista = "septiembre"
                                                        }else if mes == 10 {
                                                            mesRevista = "octubre"
                                                        }else if mes == 11 {
                                                            mesRevista = "noviembre"
                                                        }else if mes == 12 {
                                                            mesRevista = "diciembre"
                                                        }
                                                        
                                                        
                                                        if año == 2015 {
                                                            añoRevista = "2015"
                                                        }else if año == 2016 {
                                                            añoRevista = "2016"
                                                        }else if año == 2017 {
                                                            añoRevista = "2017"
                                                        }else if año == 2018 {
                                                            añoRevista = "2018"
                                                        }else if año == 2019 {
                                                            añoRevista = "2019"
                                                        }else if año == 2020 {
                                                            añoRevista = "2020"
                                                        }else if año == 2021 {
                                                            añoRevista = "2021"
                                                        }else if año == 2022 {
                                                            añoRevista = "2022"
                                                        }else if año == 2023 {
                                                            añoRevista = "2023"
                                                        }else if año == 2024 {
                                                            añoRevista = "2024"
                                                        }else if año == 2025 {
                                                            añoRevista = "2025"
                                                        }else if año == 2026 {
                                                            añoRevista = "2026"
                                                        }else if año == 2027 {
                                                            añoRevista = "2027"
                                                        }else if año == 2028 {
                                                            añoRevista = "2028"
                                                        }else if año == 2029 {
                                                            añoRevista = "2029"
                                                        }else if año == 2030 {
                                                            añoRevista = "2030"
                                                        }
                                                        
                                                        let dates = "\(mesRevista) \(añoRevista)"
                                                        fechaRevista.text = "\(mesRevista) \(añoRevista)"
                                                        self.fechaRevista = fechaRevista.text
                                                        self.arrayF.append(dates)
                                                        self.masNuevoScroll.addSubview(fechaRevista)
                                                        self.masNuevoScroll.addSubview(tituloLabel)
                                                        
                                                    }
                                                    
                                                }
                                                
                                            }
                                            
                                        }
                                        
                                    }
                                    
                                    if titulo == "Te recomendamos" {
                                        
                                        if subKeys == "titulo" {
                                            
                                            let tituloLabel : String = subValues as! String
                                            self.segundoTitulo.text = "\(tituloLabel.uppercased())"
                                            
                                        }
                                        
                                        if subKeys == "elementos" {
                                            
                                            let arrayElementos : NSArray = subValues as! NSArray
                                            
                                            for element in arrayElementos {
                                                
                                                let dic3 : NSDictionary = element as! NSDictionary
                                                
                                                for i in dic3 {
                                                    
                                                    let subKey : String = i.key as! String
                                                    let subVal = i.value
                                                    
                                                    let mes : Double = dic3["mes"] as! Double
                                                    let año : Double = dic3["year"] as! Double
                                                    
                                                    if subKey == "_id" {
                                                        
                                                        let idRevista2 : String = subVal as! String
                                                        
                                                        self.arrayIDs2.append(idRevista2)
                                                        
                                                    }
                                                    
                                                    if subKey == "imagen" {
                                                        
                                                        let urlImage2 : String = subVal as! String
                                                        self.urlImg2.append(urlImage2)
                                                        
                                                        let imagenRevista2 = UIImageView(frame: CGRect(x: self.ejeX2, y: 39, width: 175, height: 225))
                                                        let imagenEsquina = UIImageView(frame: CGRect(x: self.ejexCorner2, y: 169, width: 71, height: 95))
                                                        imagenRevista2.backgroundColor = self.backGroundGray
                                                        imagenEsquina.contentMode = .scaleToFill
                                                        imagenEsquina.image = UIImage(named: "Hoja2")
                                                        self.imagenes2.append(imagenRevista2)
                                                        let botonDetalles2 = UIButton(frame: CGRect(x: self.ejeX2, y: 39, width: 175, height: 225))
                                                        self.ejeX2 = self.ejeX2 + self.a
                                                        let scrollW2 = self.ejeX2
                                                        self.ejexCorner2 = self.ejexCorner2 + self.z
                                                        botonDetalles2.tag = self.botonTag2
                                                        self.botonTag2 = self.botonTag2 + self.add2
                                                        self.segundoScroll.contentSize = CGSize(width: scrollW2, height: 272)
                                                        self.segundoScroll.addSubview(imagenRevista2)
                                                        self.segundoScroll.addSubview(imagenEsquina)
                                                        self.segundoScroll.addSubview(botonDetalles2)
                                                        
                                                        botonDetalles2.addTarget(self, action: #selector(self.ClickRevista2), for: .touchUpInside)
                                                        
                                                    }
                                                    if subKey == "titulo" {
                                                        
                                                        var mesRevista2 = ""
                                                        var añoRevista2 = ""
                                                        let tit2 : String = subVal as! String
                                                        self.revistaTitulo2.append(tit2)
                                                        
                                                        let tituloLabel2 = UILabel(frame: CGRect(x: self.ejeLabelX2, y: 15, width: 130, height: 15))
                                                        self.ejeLabelX2 = self.ejeLabelX2 + self.a
                                                        tituloLabel2.text = tit2
                                                        tituloLabel2.textColor = UIColor.white
                                                        tituloLabel2.font = UIFont.boldSystemFont(ofSize: 15)
                                                        tituloLabel2.adjustsFontSizeToFitWidth = true
                                                        
                                                        let fechaRevista2 = UILabel(frame: CGRect(x: self.ejeXFechaR2, y: 276, width: 130, height: 15))
                                                        
                                                        self.ejeXFechaR2 = self.ejeXFechaR2 + self.a
                                                        fechaRevista2.textColor = UIColor.white
                                                        fechaRevista2.font = UIFont.boldSystemFont(ofSize: 15)
                                                        fechaRevista2.adjustsFontSizeToFitWidth = true
                                                        
                                                        if mes == 1 {
                                                            mesRevista2 = "enero"
                                                        }else if mes == 2 {
                                                            mesRevista2 = "febrero"
                                                        }else if mes == 3 {
                                                            mesRevista2 = "marzo"
                                                        }else if mes == 4 {
                                                            mesRevista2 = "abril"
                                                        }else if mes == 5 {
                                                            mesRevista2 = "mayo"
                                                        }else if mes == 6 {
                                                            mesRevista2 = "junio"
                                                        }else if mes == 7 {
                                                            mesRevista2 = "julio"
                                                        }else if mes == 8 {
                                                            mesRevista2 = "agosto"
                                                        }else if mes == 9 {
                                                            mesRevista2 = "septiembre"
                                                        }else if mes == 10 {
                                                            mesRevista2 = "octubre"
                                                        }else if mes == 11 {
                                                            mesRevista2 = "noviembre"
                                                        }else if mes == 12 {
                                                            mesRevista2 = "diciembre"
                                                        }
                                                        
                                                        
                                                        if año == 2015 {
                                                            añoRevista2 = "2015"
                                                        }else if año == 2016 {
                                                            añoRevista2 = "2016"
                                                        }else if año == 2017 {
                                                            añoRevista2 = "2017"
                                                        }else if año == 2018 {
                                                            añoRevista2 = "2018"
                                                        }else if año == 2019 {
                                                            añoRevista2 = "2019"
                                                        }else if año == 2020 {
                                                            añoRevista2 = "2020"
                                                        }else if año == 2021 {
                                                            añoRevista2 = "2021"
                                                        }else if año == 2022 {
                                                            añoRevista2 = "2022"
                                                        }else if año == 2023 {
                                                            añoRevista2 = "2023"
                                                        }else if año == 2024 {
                                                            añoRevista2 = "2024"
                                                        }else if año == 2025 {
                                                            añoRevista2 = "2025"
                                                        }else if año == 2026 {
                                                            añoRevista2 = "2026"
                                                        }else if año == 2027 {
                                                            añoRevista2 = "2027"
                                                        }else if año == 2028 {
                                                            añoRevista2 = "2028"
                                                        }else if año == 2029 {
                                                            añoRevista2 = "2029"
                                                        }else if año == 2030 {
                                                            añoRevista2 = "2030"
                                                        }
                                                        
                                                        let dates2 = "\(mesRevista2) \(añoRevista2)"
                                                        fechaRevista2.text = "\(mesRevista2) \(añoRevista2)"
                                                        self.fechaRevista2 = fechaRevista2.text
                                                        self.arrayF2.append(dates2)
                                                        self.segundoScroll.addSubview(fechaRevista2)
                                                        self.segundoScroll.addSubview(tituloLabel2)
                                                        
                                                    }
                                                    
                                                }
                                                
                                            }
                                            
                                        }
                                        
                                    }
                                    
                                    if titulo == "Lo más visto" {
                                        
                                        if subKeys == "titulo" {
                                            
                                            
                                            let tituloLabel : String = subValues as! String
                                            self.tercerLabel.text = "\(tituloLabel.uppercased())"
                                            
                                        }
                                        
                                        if subKeys == "elementos" {
                                            
                                            let arrayElementos : NSArray = subValues as! NSArray
                                            
                                            for element in arrayElementos {
                                                
                                                let dic3 : NSDictionary = element as! NSDictionary
                                                
                                                for i in dic3 {
                                                    
                                                    let subKey : String = i.key as! String
                                                    let subVal = i.value
                                                    
                                                    let mes3 : Double = dic3["mes"] as! Double
                                                    let año3 : Double = dic3["year"] as! Double
                                                    
                                                    if subKey == "imagen" {
                                                        
                                                        let urlImage3 : String = subVal as! String
                                                        self.urlImg3.append(urlImage3)
                                                        let imagenRevista3 = UIImageView(frame: CGRect(x: self.ejeX3, y: 39, width: 175, height: 225))
                                                        let botonDetalles3 = UIButton(frame: CGRect(x: self.ejeX3, y: 39, width: 175, height: 225))
                                                        let imagenEsquina = UIImageView(frame: CGRect(x: self.ejexCorner3, y: 169, width: 71, height: 95))
                                                        imagenRevista3.backgroundColor = self.backGroundGray
                                                        imagenEsquina.contentMode = .scaleToFill
                                                        imagenEsquina.image = UIImage(named: "Hoja2")
                                                        self.ejexCorner3 = self.ejexCorner3 + self.z
                                                        botonDetalles3.tag = self.botonTag3
                                                        self.botonTag3 = self.botonTag3 + self.add3
                                                        self.imagenes3.append(imagenRevista3)
                                                        self.ejeX3 = self.ejeX3 + self.b
                                                        let scrollW3 = self.ejeX3
                                                        self.tercerScroll.contentSize = CGSize(width: scrollW3, height: 272)
                                                        self.tercerScroll.addSubview(imagenRevista3)
                                                        self.tercerScroll.addSubview(imagenEsquina)
                                                        self.tercerScroll.addSubview(botonDetalles3)
                                                        botonDetalles3.addTarget(self, action: #selector(self.ClickRevista3), for: .touchUpInside)
                                                        
                                                    }
                                                    
                                                    if subKey == "titulo" {
                                                        
                                                        var mesRevista3 = ""
                                                        var añoRevista3 = ""
                                                        let tit3 : String = subVal as! String
                                                        self.revistaTitulo3.append(tit3)
                                                        let tituloLabel3 = UILabel(frame: CGRect(x: self.ejeLabelX3, y: 15, width: 130, height: 15))
                                                        self.ejeLabelX3 = self.ejeLabelX3 + self.b
                                                        tituloLabel3.text = tit3
                                                        tituloLabel3.textColor = UIColor.white
                                                        tituloLabel3.font = UIFont.boldSystemFont(ofSize: 15)
                                                        tituloLabel3.adjustsFontSizeToFitWidth = true
                                                        
                                                        let fechaRevista3 = UILabel(frame: CGRect(x: self.ejeXFechaR3, y: 276, width: 130, height: 15))
                                                        
                                                        self.ejeXFechaR3 = self.ejeXFechaR3 + self.a
                                                        fechaRevista3.textColor = UIColor.white
                                                        fechaRevista3.font = UIFont.boldSystemFont(ofSize: 15)
                                                        fechaRevista3.adjustsFontSizeToFitWidth = true
                                                        
                                                        if mes3 == 1 {
                                                            mesRevista3 = "enero"
                                                        }else if mes3 == 2 {
                                                            mesRevista3 = "febrero"
                                                        }else if mes3 == 3 {
                                                            mesRevista3 = "marzo"
                                                        }else if mes3 == 4 {
                                                            mesRevista3 = "abril"
                                                        }else if mes3 == 5 {
                                                            mesRevista3 = "mayo"
                                                        }else if mes3 == 6 {
                                                            mesRevista3 = "junio"
                                                        }else if mes3 == 7 {
                                                            mesRevista3 = "julio"
                                                        }else if mes3 == 8 {
                                                            mesRevista3 = "agosto"
                                                        }else if mes3 == 9 {
                                                            mesRevista3 = "septiembre"
                                                        }else if mes3 == 10 {
                                                            mesRevista3 = "octubre"
                                                        }else if mes3 == 11 {
                                                            mesRevista3 = "noviembre"
                                                        }else if mes3 == 12 {
                                                            mesRevista3 = "diciembre"
                                                        }
                                                        
                                                        
                                                        if año3 == 2015 {
                                                            añoRevista3 = "2015"
                                                        }else if año3 == 2016 {
                                                            añoRevista3 = "2016"
                                                        }else if año3 == 2017 {
                                                            añoRevista3 = "2017"
                                                        }else if año3 == 2018 {
                                                            añoRevista3 = "2018"
                                                        }else if año3 == 2019 {
                                                            añoRevista3 = "2019"
                                                        }else if año3 == 2020 {
                                                            añoRevista3 = "2020"
                                                        }else if año3 == 2021 {
                                                            añoRevista3 = "2021"
                                                        }else if año3 == 2022 {
                                                            añoRevista3 = "2022"
                                                        }else if año3 == 2023 {
                                                            añoRevista3 = "2023"
                                                        }else if año3 == 2024 {
                                                            añoRevista3 = "2024"
                                                        }else if año3 == 2025 {
                                                            añoRevista3 = "2025"
                                                        }else if año3 == 2026 {
                                                            añoRevista3 = "2026"
                                                        }else if año3 == 2027 {
                                                            añoRevista3 = "2027"
                                                        }else if año3 == 2028 {
                                                            añoRevista3 = "2028"
                                                        }else if año3 == 2029 {
                                                            añoRevista3 = "2029"
                                                        }else if año3 == 2030 {
                                                            añoRevista3 = "2030"
                                                        }
                                                        
                                                        let dates3 = "\(mesRevista3) \(añoRevista3)"
                                                        self.arrayF3.append(dates3)
                                                        fechaRevista3.text = "\(mesRevista3) \(añoRevista3)"
                                                        self.tercerScroll.addSubview(fechaRevista3)
                                                        self.tercerScroll.addSubview(tituloLabel3)
                                                        
                                                    }
                                                    
                                                    if subKey == "_id" {
                                                        
                                                        let idRevista3 : String = subVal as! String
                                                        
                                                        self.arrayIDs3.append(idRevista3)
                                                        
                                                    }
                                                    
                                                }
                                                
                                            }
                                            
                                        }
                                        
                                    }
                                    
                                    if titulo == "Autos" {
                                        
                                        if subKeys == "titulo" {
                                            
                                            let tituloLabel : String = subValues as! String
                                            self.cuartoLabel.text = "\(tituloLabel.uppercased())"
                                            
                                        }
                                        
                                        if subKeys == "elementos" {
                                            
                                            let arrayElementos : NSArray = subValues as! NSArray
                                            
                                            for element in arrayElementos {
                                                
                                                let dic3 : NSDictionary = element as! NSDictionary
                                                
                                                for i in dic3 {
                                                    
                                                    let subKey : String = i.key as! String
                                                    let subVal = i.value
                                                    
                                                    let mes4 : Double = dic3["mes"] as! Double
                                                    let año4 : Double = dic3["year"] as! Double
                                                    
                                                    if subKey == "_id" {
                                                        
                                                        let idRevista4 : String = subVal as! String
                                                        self.arrayIDs4.append(idRevista4)
                                                        
                                                    }
                                                    
                                                    if subKey == "imagen" {
                                                        
                                                        let urlImage4 : String = subVal as! String
                                                        self.urlImg4.append(urlImage4)
                                                        
                                                        let imagenRevista4 = UIImageView(frame: CGRect(x: self.ejeX4, y: 39, width: 175, height: 225))
                                                        let botonDetalles4 = UIButton(frame: CGRect(x: self.ejeX4, y: 39, width: 175, height: 225))
                                                        let imagenEsquina = UIImageView(frame: CGRect(x: self.ejexCorner4, y: 169, width: 71, height: 95))
                                                        imagenRevista4.backgroundColor = self.backGroundGray
                                                        imagenEsquina.contentMode = .scaleToFill
                                                        imagenEsquina.image = UIImage(named: "Hoja2")
                                                        self.ejexCorner4 = self.ejexCorner4 + self.z
                                                        botonDetalles4.tag = self.botonTag4
                                                        self.botonTag4 = self.botonTag4 + self.add4
                                                        self.imagenes4.append(imagenRevista4)
                                                        self.ejeX4 = self.ejeX4 + self.c
                                                        let scrollW4 = self.ejeX4
                                                        self.cuartoScroll.contentSize = CGSize(width: scrollW4, height: 272)
                                                        self.cuartoScroll.addSubview(imagenRevista4)
                                                        self.cuartoScroll.addSubview(imagenEsquina)
                                                        self.cuartoScroll.addSubview(botonDetalles4)
                                                        botonDetalles4.addTarget(self, action: #selector(self.ClickRevista4), for: .touchUpInside)
                                                        
                                                    }
                                                    
                                                    if subKey == "titulo" {
                                                        
                                                        var mesRevista4 = ""
                                                        var añoRevista4 = ""
                                                        let tit4 : String = subVal as! String
                                                        self.revistaTitulo4.append(tit4)
                                                        let tituloLabel4 = UILabel(frame: CGRect(x: self.ejeLabelX4, y: 15, width: 130, height: 15))
                                                        self.ejeLabelX4 = self.ejeLabelX4 + self.c
                                                        tituloLabel4.text = tit4
                                                        tituloLabel4.textColor = UIColor.white
                                                        tituloLabel4.font = UIFont.boldSystemFont(ofSize: 15)
                                                        tituloLabel4.adjustsFontSizeToFitWidth = true
                                                        
                                                        let fechaRevista4 = UILabel(frame: CGRect(x: self.ejeXFechaR4, y: 276, width: 130, height: 15))
                                                        
                                                        self.ejeXFechaR4 = self.ejeXFechaR4 + self.c
                                                        fechaRevista4.textColor = UIColor.white
                                                        fechaRevista4.font = UIFont.boldSystemFont(ofSize: 15)
                                                        fechaRevista4.adjustsFontSizeToFitWidth = true
                                                        
                                                        if mes4 == 1 {
                                                            mesRevista4 = "enero"
                                                        }else if mes4 == 2 {
                                                            mesRevista4 = "febrero"
                                                        }else if mes4 == 3 {
                                                            mesRevista4 = "marzo"
                                                        }else if mes4 == 4 {
                                                            mesRevista4 = "abril"
                                                        }else if mes4 == 5 {
                                                            mesRevista4 = "mayo"
                                                        }else if mes4 == 6 {
                                                            mesRevista4 = "junio"
                                                        }else if mes4 == 7 {
                                                            mesRevista4 = "julio"
                                                        }else if mes4 == 8 {
                                                            mesRevista4 = "agosto"
                                                        }else if mes4 == 9 {
                                                            mesRevista4 = "septiembre"
                                                        }else if mes4 == 10 {
                                                            mesRevista4 = "octubre"
                                                        }else if mes4 == 11 {
                                                            mesRevista4 = "noviembre"
                                                        }else if mes4 == 12 {
                                                            mesRevista4 = "diciembre"
                                                        }
                                                        
                                                        
                                                        if año4 == 2015 {
                                                            añoRevista4 = "2015"
                                                        }else if año4 == 2016 {
                                                            añoRevista4 = "2016"
                                                        }else if año4 == 2017 {
                                                            añoRevista4 = "2017"
                                                        }else if año4 == 2018 {
                                                            añoRevista4 = "2018"
                                                        }else if año4 == 2019 {
                                                            añoRevista4 = "2019"
                                                        }else if año4 == 2020 {
                                                            añoRevista4 = "2020"
                                                        }else if año4 == 2021 {
                                                            añoRevista4 = "2021"
                                                        }else if año4 == 2022 {
                                                            añoRevista4 = "2022"
                                                        }else if año4 == 2023 {
                                                            añoRevista4 = "2023"
                                                        }else if año4 == 2024 {
                                                            añoRevista4 = "2024"
                                                        }else if año4 == 2025 {
                                                            añoRevista4 = "2025"
                                                        }else if año4 == 2026 {
                                                            añoRevista4 = "2026"
                                                        }else if año4 == 2027 {
                                                            añoRevista4 = "2027"
                                                        }else if año4 == 2028 {
                                                            añoRevista4 = "2028"
                                                        }else if año4 == 2029 {
                                                            añoRevista4 = "2029"
                                                        }else if año4 == 2030 {
                                                            añoRevista4 = "2030"
                                                        }
                                                        
                                                        let dates4 = "\(mesRevista4) \(añoRevista4)"
                                                        self.arrayF4.append(dates4)
                                                        fechaRevista4.text = "\(mesRevista4) \(añoRevista4)"
                                                        self.cuartoScroll.addSubview(fechaRevista4)
                                                        self.cuartoScroll.addSubview(tituloLabel4)
                                                        
                                                    }
                                                    
                                                }
                                                
                                            }
                                            
                                        }
                                        
                                    }
                                    
                                    if titulo == "Deportes" {
                                        
                                        if subKeys == "titulo" {
                                            
                                            let tituloLabel : String = subValues as! String
                                            self.quintoLabel.text = "\(tituloLabel.uppercased())"
                                            
                                        }
                                        
                                        if subKeys == "elementos" {
                                            
                                            let arrayElementos : NSArray = subValues as! NSArray
                                            
                                            for element in arrayElementos {
                                                
                                                let dic3 : NSDictionary = element as! NSDictionary
                                                
                                                for i in dic3 {
                                                    
                                                    let subKey : String = i.key as! String
                                                    let subVal = i.value
                                                    
                                                    let mes5 : Double = dic3["mes"] as! Double
                                                    let año5 : Double = dic3["year"] as! Double
                                                    
                                                    if subKey == "_id" {
                                                        
                                                        let idRevista5 : String = subVal as! String
                                                        self.arrayIDs5.append(idRevista5)
                                                        
                                                    }
                                                    
                                                    if subKey == "imagen" {
                                                        
                                                        let urlImage5 : String = subVal as! String
                                                        self.urlImg5.append(urlImage5)
                                                        
                                                        let imagenRevista5 = UIImageView(frame: CGRect(x: self.ejeX5, y: 39, width: 175, height: 225))
                                                        let botonDetalles5 = UIButton(frame: CGRect(x: self.ejeX5, y: 39, width: 175, height: 225))
                                                        let imagenEsquina = UIImageView(frame: CGRect(x: self.ejexCorner5, y: 169, width: 71, height: 95))
                                                        imagenRevista5.backgroundColor = self.backGroundGray
                                                        imagenEsquina.contentMode = .scaleToFill
                                                        imagenEsquina.image = UIImage(named: "Hoja2")
                                                        self.ejexCorner5 = self.ejexCorner5 + self.z
                                                        botonDetalles5.tag = self.botonTag5
                                                        self.botonTag5 = self.botonTag5 + self.add5
                                                        self.imagenes5.append(imagenRevista5)
                                                        self.ejeX5 = self.ejeX5 + self.d
                                                        let scrollW5 = self.ejeX5
                                                        self.quintoScroll.contentSize = CGSize(width: scrollW5, height: 272)
                                                        self.quintoScroll.addSubview(imagenRevista5)
                                                        self.quintoScroll.addSubview(imagenEsquina)
                                                        self.quintoScroll.addSubview(botonDetalles5)
                                                        botonDetalles5.addTarget(self, action: #selector(self.ClickRevista5), for: .touchUpInside)
                                                    }
                                                    
                                                    if subKey == "titulo" {
                                                        
                                                        var mesRevista5 = ""
                                                        var añoRevista5 = ""
                                                        let tit5 : String = subVal as! String
                                                        self.revistaTitulo5.append(tit5)
                                                        let tituloLabel5 = UILabel(frame: CGRect(x: self.ejeLabelX5, y: 15, width: 130, height: 15))
                                                        self.ejeLabelX5 = self.ejeLabelX5 + self.d
                                                        tituloLabel5.text = tit5
                                                        tituloLabel5.textColor = UIColor.white
                                                        tituloLabel5.font = UIFont.boldSystemFont(ofSize: 15)
                                                        tituloLabel5.adjustsFontSizeToFitWidth = true
                                                        
                                                        let fechaRevista5 = UILabel(frame: CGRect(x: self.ejeXFechaR5, y: 276, width: 130, height: 15))
                                                        
                                                        self.ejeXFechaR5 = self.ejeXFechaR5 + self.d
                                                        fechaRevista5.textColor = UIColor.white
                                                        fechaRevista5.font = UIFont.boldSystemFont(ofSize: 15)
                                                        fechaRevista5.adjustsFontSizeToFitWidth = true
                                                        
                                                        if mes5 == 1 {
                                                            mesRevista5 = "enero"
                                                        }else if mes5 == 2 {
                                                            mesRevista5 = "febrero"
                                                        }else if mes5 == 3 {
                                                            mesRevista5 = "marzo"
                                                        }else if mes5 == 4 {
                                                            mesRevista5 = "abril"
                                                        }else if mes5 == 5 {
                                                            mesRevista5 = "mayo"
                                                        }else if mes5 == 6 {
                                                            mesRevista5 = "junio"
                                                        }else if mes5 == 7 {
                                                            mesRevista5 = "julio"
                                                        }else if mes5 == 8 {
                                                            mesRevista5 = "agosto"
                                                        }else if mes5 == 9 {
                                                            mesRevista5 = "septiembre"
                                                        }else if mes5 == 10 {
                                                            mesRevista5 = "octubre"
                                                        }else if mes5 == 11 {
                                                            mesRevista5 = "noviembre"
                                                        }else if mes5 == 12 {
                                                            mesRevista5 = "diciembre"
                                                        }
                                                        
                                                        
                                                        if año5 == 2015 {
                                                            añoRevista5 = "2015"
                                                        }else if año5 == 2016 {
                                                            añoRevista5 = "2016"
                                                        }else if año5 == 2017 {
                                                            añoRevista5 = "2017"
                                                        }else if año5 == 2018 {
                                                            añoRevista5 = "2018"
                                                        }else if año5 == 2019 {
                                                            añoRevista5 = "2019"
                                                        }else if año5 == 2020 {
                                                            añoRevista5 = "2020"
                                                        }else if año5 == 2021 {
                                                            añoRevista5 = "2021"
                                                        }else if año5 == 2022 {
                                                            añoRevista5 = "2022"
                                                        }else if año5 == 2023 {
                                                            añoRevista5 = "2023"
                                                        }else if año5 == 2024 {
                                                            añoRevista5 = "2024"
                                                        }else if año5 == 2025 {
                                                            añoRevista5 = "2025"
                                                        }else if año5 == 2026 {
                                                            añoRevista5 = "2026"
                                                        }else if año5 == 2027 {
                                                            añoRevista5 = "2027"
                                                        }else if año5 == 2028 {
                                                            añoRevista5 = "2028"
                                                        }else if año5 == 2029 {
                                                            añoRevista5 = "2029"
                                                        }else if año5 == 2030 {
                                                            añoRevista5 = "2030"
                                                        }
                                                        
                                                        fechaRevista5.text = "\(mesRevista5) \(añoRevista5)"
                                                        let dates5 = "\(mesRevista5) \(añoRevista5)"
                                                        self.arrayF5.append(dates5)
                                                        self.quintoScroll.addSubview(fechaRevista5)
                                                        self.quintoScroll.addSubview(tituloLabel5)
                                                        
                                                    }
                                                    
                                                }
                                                
                                            }
                                            
                                        }
                                        
                                    }
                                    
                                    if titulo == "Entretenimiento" {
                                        
                                        if subKeys == "titulo" {
                                            let tituloLabel : String = subValues as! String
                                            self.sextoLabel.text = "\(tituloLabel.uppercased())"
                                            
                                        }
                                        
                                        if subKeys == "elementos" {
                                            
                                            let arrayElementos : NSArray = subValues as! NSArray
                                            
                                            for element in arrayElementos {
                                                
                                                let dic3 : NSDictionary = element as! NSDictionary
                                                
                                                for i in dic3 {
                                                    
                                                    let subKey : String = i.key as! String
                                                    let subVal = i.value
                                                    
                                                    let mes6 : Double = dic3["mes"] as! Double
                                                    let año6 : Double = dic3["year"] as! Double
                                                    
                                                    if subKey == "_id" {
                                                        
                                                        let idRevista6 : String = subVal as! String
                                                        self.arrayIDs6.append(idRevista6)
                                                        
                                                    }
                                                    
                                                    if subKey == "imagen" {
                                                        
                                                        let urlImage6 : String = subVal as! String
                                                        self.urlImg6.append(urlImage6)
                                                        
                                                        let imagenRevista6 = UIImageView(frame: CGRect(x: self.ejeX6, y: 39, width: 175, height: 225))
                                                        let botonDetalles6 = UIButton(frame: CGRect(x: self.ejeX6, y: 39, width: 175, height: 225))
                                                        let imagenEsquina = UIImageView(frame: CGRect(x: self.ejexCorner6, y: 169, width: 71, height: 95))
                                                        imagenRevista6.backgroundColor = self.backGroundGray
                                                        imagenEsquina.contentMode = .scaleToFill
                                                        imagenEsquina.image = UIImage(named: "Hoja2")
                                                        self.ejexCorner6 = self.ejexCorner6 + self.z
                                                        botonDetalles6.tag = self.botonTag6
                                                        self.botonTag6 = self.botonTag6 + self.add6
                                                        self.imagenes6.append(imagenRevista6)
                                                        self.ejeX6 = self.ejeX6 + self.e
                                                        let scrollW6 = self.ejeX6
                                                        self.sextoScroll.contentSize = CGSize(width: scrollW6, height: 272)
                                                        self.sextoScroll.addSubview(imagenRevista6)
                                                        self.sextoScroll.addSubview(imagenEsquina)
                                                        self.sextoScroll.addSubview(botonDetalles6)
                                                        botonDetalles6.addTarget(self, action: #selector(self.ClickRevista6(sender:)), for: .touchUpInside)
                                                        
                                                    }
                                                    
                                                    if subKey == "titulo" {
                                                        
                                                        var mesRevista6 = ""
                                                        var añoRevista6 = ""
                                                        let tit6 : String = subVal as! String
                                                        self.revistaTitulo6.append(tit6)
                                                        let tituloLabel6 = UILabel(frame: CGRect(x: self.ejeLabelX6, y: 15, width: 130, height: 15))
                                                        self.ejeLabelX6 = self.ejeLabelX6 + self.e
                                                        tituloLabel6.text = tit6
                                                        tituloLabel6.textColor = UIColor.white
                                                        tituloLabel6.font = UIFont.boldSystemFont(ofSize: 15)
                                                        tituloLabel6.adjustsFontSizeToFitWidth = true
                                                        
                                                        let fechaRevista6 = UILabel(frame: CGRect(x: self.ejeXFechaR6, y: 276, width: 130, height: 15))
                                                        
                                                        self.ejeXFechaR6 = self.ejeXFechaR6 + self.d
                                                        fechaRevista6.textColor = UIColor.white
                                                        fechaRevista6.font = UIFont.boldSystemFont(ofSize: 15)
                                                        fechaRevista6.adjustsFontSizeToFitWidth = true
                                                        
                                                        if mes6 == 1 {
                                                            mesRevista6 = "enero"
                                                        }else if mes6 == 2 {
                                                            mesRevista6 = "febrero"
                                                        }else if mes6 == 3 {
                                                            mesRevista6 = "marzo"
                                                        }else if mes6 == 4 {
                                                            mesRevista6 = "abril"
                                                        }else if mes6 == 5 {
                                                            mesRevista6 = "mayo"
                                                        }else if mes6 == 6 {
                                                            mesRevista6 = "junio"
                                                        }else if mes6 == 7 {
                                                            mesRevista6 = "julio"
                                                        }else if mes6 == 8 {
                                                            mesRevista6 = "agosto"
                                                        }else if mes6 == 9 {
                                                            mesRevista6 = "septiembre"
                                                        }else if mes6 == 10 {
                                                            mesRevista6 = "octubre"
                                                        }else if mes6 == 11 {
                                                            mesRevista6 = "noviembre"
                                                        }else if mes6 == 12 {
                                                            mesRevista6 = "diciembre"
                                                        }
                                                        
                                                        
                                                        if año6 == 2015 {
                                                            añoRevista6 = "2015"
                                                        }else if año6 == 2016 {
                                                            añoRevista6 = "2016"
                                                        }else if año6 == 2017 {
                                                            añoRevista6 = "2017"
                                                        }else if año6 == 2018 {
                                                            añoRevista6 = "2018"
                                                        }else if año6 == 2019 {
                                                            añoRevista6 = "2019"
                                                        }else if año6 == 2020 {
                                                            añoRevista6 = "2020"
                                                        }else if año6 == 2021 {
                                                            añoRevista6 = "2021"
                                                        }else if año6 == 2022 {
                                                            añoRevista6 = "2022"
                                                        }else if año6 == 2023 {
                                                            añoRevista6 = "2023"
                                                        }else if año6 == 2024 {
                                                            añoRevista6 = "2024"
                                                        }else if año6 == 2025 {
                                                            añoRevista6 = "2025"
                                                        }else if año6 == 2026 {
                                                            añoRevista6 = "2026"
                                                        }else if año6 == 2027 {
                                                            añoRevista6 = "2027"
                                                        }else if año6 == 2028 {
                                                            añoRevista6 = "2028"
                                                        }else if año6 == 2029 {
                                                            añoRevista6 = "2029"
                                                        }else if año6 == 2030 {
                                                            añoRevista6 = "2030"
                                                        }
                                                        
                                                        fechaRevista6.text = "\(mesRevista6) \(añoRevista6)"
                                                        let dates6 = "\(mesRevista6) \(añoRevista6)"
                                                        self.arrayF6.append(dates6)
                                                        self.sextoScroll.addSubview(fechaRevista6)
                                                        self.sextoScroll.addSubview(tituloLabel6)
                                                        
                                                    }
                                                    
                                                }
                                                
                                            }
                                            
                                        }
                                    }
                                    
                                    if titulo == "Hombres" {
                                        
                                        if subKeys == "titulo" {
                                            
                                            let tituloLabel : String = subValues as! String
                                            self.septimoLabel.text = "\(tituloLabel.uppercased())"
                                            
                                        }
                                        
                                        if subKeys == "elementos" {
                                            
                                            let arrayElementos : NSArray = subValues as! NSArray
                                            
                                            for element in arrayElementos {
                                                
                                                let dic3 : NSDictionary = element as! NSDictionary
                                                
                                                for i in dic3 {
                                                    
                                                    let subKey : String = i.key as! String
                                                    let subVal = i.value
                                                    
                                                    let mes7 : Double = dic3["mes"] as! Double
                                                    let año7 : Double = dic3["year"] as! Double
                                                    
                                                    if subKey == "_id" {
                                                        
                                                        let idRevista7 : String = subVal as! String
                                                        
                                                        self.arrayIDs7.append(idRevista7)
                                                        
                                                    }
                                                    
                                                    if subKey == "imagen" {
                                                        
                                                        let urlImage7 : String = subVal as! String
                                                        self.urlImg7.append(urlImage7)
                                                        
                                                        let imagenRevista7 = UIImageView(frame: CGRect(x: self.ejeX7, y: 39, width: 175, height: 225))
                                                        let botonDetalles7 = UIButton(frame: CGRect(x: self.ejeX7, y: 39, width: 175, height: 225))
                                                        let imagenEsquina = UIImageView(frame: CGRect(x: self.ejexCorner7, y: 169, width: 71, height: 95))
                                                        imagenRevista7.backgroundColor = self.backGroundGray
                                                        imagenEsquina.contentMode = .scaleToFill
                                                        imagenEsquina.image = UIImage(named: "Hoja2")
                                                        self.ejexCorner7 = self.ejexCorner7 + self.z
                                                        botonDetalles7.tag = self.botonTag7
                                                        self.botonTag7 = self.botonTag7 + self.add7
                                                        self.imagenes7.append(imagenRevista7)
                                                        self.ejeX7 = self.ejeX7 + self.f
                                                        let scrollW7 = self.ejeX7
                                                        self.septimoScroll.contentSize = CGSize(width: scrollW7, height: 272)
                                                        self.septimoScroll.addSubview(imagenRevista7)
                                                        self.septimoScroll.addSubview(imagenEsquina)
                                                        self.septimoScroll.addSubview(botonDetalles7)
                                                        botonDetalles7.addTarget(self, action: #selector(self.ClickRevista7), for: .touchUpInside)
                                                        
                                                    }
                                                    
                                                    if subKey == "titulo" {
                                                        
                                                        var mesRevista7 = ""
                                                        var añoRevista7 = ""
                                                        let tit7 : String = subVal as! String
                                                        self.revistaTitulo7.append(tit7)
                                                        let tituloLabel7 = UILabel(frame: CGRect(x: self.ejeLabelX7, y: 15, width: 130, height: 15))
                                                        self.ejeLabelX7 = self.ejeLabelX7 + self.f
                                                        tituloLabel7.text = tit7
                                                        tituloLabel7.textColor = UIColor.white
                                                        tituloLabel7.font = UIFont.boldSystemFont(ofSize: 15)
                                                        tituloLabel7.adjustsFontSizeToFitWidth = true
                                                        
                                                        let fechaRevista7 = UILabel(frame: CGRect(x: self.ejeXFechaR7, y: 276, width: 130, height: 15))
                                                        
                                                        self.ejeXFechaR7 = self.ejeXFechaR7 + self.f
                                                        fechaRevista7.textColor = UIColor.white
                                                        fechaRevista7.font = UIFont.boldSystemFont(ofSize: 15)
                                                        fechaRevista7.adjustsFontSizeToFitWidth = true
                                                        
                                                        if mes7 == 1 {
                                                            mesRevista7 = "enero"
                                                        }else if mes7 == 2 {
                                                            mesRevista7 = "febrero"
                                                        }else if mes7 == 3 {
                                                            mesRevista7 = "marzo"
                                                        }else if mes7 == 4 {
                                                            mesRevista7 = "abril"
                                                        }else if mes7 == 5 {
                                                            mesRevista7 = "mayo"
                                                        }else if mes7 == 6 {
                                                            mesRevista7 = "junio"
                                                        }else if mes7 == 7 {
                                                            mesRevista7 = "julio"
                                                        }else if mes7 == 8 {
                                                            mesRevista7 = "agosto"
                                                        }else if mes7 == 9 {
                                                            mesRevista7 = "septiembre"
                                                        }else if mes7 == 10 {
                                                            mesRevista7 = "octubre"
                                                        }else if mes7 == 11 {
                                                            mesRevista7 = "noviembre"
                                                        }else if mes7 == 12 {
                                                            mesRevista7 = "diciembre"
                                                        }
                                                        
                                                        
                                                        if año7 == 2015 {
                                                            añoRevista7 = "2015"
                                                        }else if año7 == 2016 {
                                                            añoRevista7 = "2016"
                                                        }else if año7 == 2017 {
                                                            añoRevista7 = "2017"
                                                        }else if año7 == 2018 {
                                                            añoRevista7 = "2018"
                                                        }else if año7 == 2019 {
                                                            añoRevista7 = "2019"
                                                        }else if año7 == 2020 {
                                                            añoRevista7 = "2020"
                                                        }else if año7 == 2021 {
                                                            añoRevista7 = "2021"
                                                        }else if año7 == 2022 {
                                                            añoRevista7 = "2022"
                                                        }else if año7 == 2023 {
                                                            añoRevista7 = "2023"
                                                        }else if año7 == 2024 {
                                                            añoRevista7 = "2024"
                                                        }else if año7 == 2025 {
                                                            añoRevista7 = "2025"
                                                        }else if año7 == 2026 {
                                                            añoRevista7 = "2026"
                                                        }else if año7 == 2027 {
                                                            añoRevista7 = "2027"
                                                        }else if año7 == 2028 {
                                                            añoRevista7 = "2028"
                                                        }else if año7 == 2029 {
                                                            añoRevista7 = "2029"
                                                        }else if año7 == 2030 {
                                                            añoRevista7 = "2030"
                                                        }
                                                        
                                                        fechaRevista7.text = "\(mesRevista7) \(añoRevista7)"
                                                        let dates7 = "\(mesRevista7) \(añoRevista7)"
                                                        self.arrayF7.append(dates7)
                                                        self.septimoScroll.addSubview(fechaRevista7)
                                                        self.septimoScroll.addSubview(tituloLabel7)
                                                        
                                                    }
                                                    
                                                }
                                                
                                            }
                                            
                                        }
                                        
                                    }
                                    
                                    if titulo == "Lujo" {
                                        
                                        if subKeys == "titulo" {
                                            
                                            let tituloLabel : String = subValues as! String
                                            self.octavoLabel.text = "\(tituloLabel.uppercased())"
                                            
                                        }
                                        
                                        if subKeys == "elementos" {
                                            
                                            let arrayElementos : NSArray = subValues as! NSArray
                                            
                                            for element in arrayElementos {
                                                
                                                let dic3 : NSDictionary = element as! NSDictionary
                                                
                                                for i in dic3 {
                                                    
                                                    let subKey : String = i.key as! String
                                                    let subVal = i.value
                                                    
                                                    let mes8 : Double = dic3["mes"] as! Double
                                                    let año8 : Double = dic3["year"] as! Double
                                                    
                                                    if subKey == "_id" {
                                                        
                                                        let idRevista8 : String = subVal as! String
                                                        
                                                        self.arrayIDs8.append(idRevista8)
                                                        
                                                    }
                                                    
                                                    if subKey == "imagen" {
                                                        
                                                        let urlImage8 : String = subVal as! String
                                                        self.urlImg8.append(urlImage8)
                                                        
                                                        let imagenRevista8 = UIImageView(frame: CGRect(x: self.ejeX8, y: 39, width: 175, height: 225))
                                                        let botonDetalles8 = UIButton(frame: CGRect(x: self.ejeX8, y: 39, width: 175, height: 225))
                                                        let imagenEsquina = UIImageView(frame: CGRect(x: self.ejexCorner8, y: 169, width: 71, height: 95))
                                                        imagenRevista8.backgroundColor = self.backGroundGray
                                                        imagenEsquina.contentMode = .scaleToFill
                                                        imagenEsquina.image = UIImage(named: "Hoja2")
                                                        self.ejexCorner8 = self.ejexCorner8 + self.z
                                                        botonDetalles8.tag = self.botonTag8
                                                        self.botonTag8 = self.botonTag8 + self.add8
                                                        self.imagenes8.append(imagenRevista8)
                                                        self.ejeX8 = self.ejeX8 + self.g
                                                        let scrollW8 = self.ejeX8
                                                        self.octavoScroll.contentSize = CGSize(width: scrollW8, height: 272)
                                                        self.octavoScroll.addSubview(imagenRevista8)
                                                        self.octavoScroll.addSubview(imagenEsquina)
                                                        self.octavoScroll.addSubview(botonDetalles8)
                                                        botonDetalles8.addTarget(self, action: #selector(self.ClickRevista8), for: .touchUpInside)
                                                        
                                                    }
                                                    
                                                    if subKey == "titulo" {
                                                        
                                                        var mesRevista8 = ""
                                                        var añoRevista8 = ""
                                                        let tit8 : String = subVal as! String
                                                        self.revistaTitulo8.append(tit8)
                                                        let tituloLabel8 = UILabel(frame: CGRect(x: self.ejeLabelX8, y: 15, width: 130, height: 15))
                                                        self.ejeLabelX8 = self.ejeLabelX8 + self.g
                                                        tituloLabel8.text = tit8
                                                        tituloLabel8.textColor = UIColor.white
                                                        tituloLabel8.font = UIFont.boldSystemFont(ofSize: 15)
                                                        tituloLabel8.adjustsFontSizeToFitWidth = true
                                                        
                                                        let fechaRevista8 = UILabel(frame: CGRect(x: self.ejeXFechaR8, y: 276, width: 130, height: 15))
                                                        
                                                        self.ejeXFechaR8 = self.ejeXFechaR8 + self.g
                                                        fechaRevista8.textColor = UIColor.white
                                                        fechaRevista8.font = UIFont.boldSystemFont(ofSize: 15)
                                                        fechaRevista8.adjustsFontSizeToFitWidth = true
                                                        
                                                        if mes8 == 1 {
                                                            mesRevista8 = "enero"
                                                        }else if mes8 == 2 {
                                                            mesRevista8 = "febrero"
                                                        }else if mes8 == 3 {
                                                            mesRevista8 = "marzo"
                                                        }else if mes8 == 4 {
                                                            mesRevista8 = "abril"
                                                        }else if mes8 == 5 {
                                                            mesRevista8 = "mayo"
                                                        }else if mes8 == 6 {
                                                            mesRevista8 = "junio"
                                                        }else if mes8 == 7 {
                                                            mesRevista8 = "julio"
                                                        }else if mes8 == 8 {
                                                            mesRevista8 = "agosto"
                                                        }else if mes8 == 9 {
                                                            mesRevista8 = "septiembre"
                                                        }else if mes8 == 10 {
                                                            mesRevista8 = "octubre"
                                                        }else if mes8 == 11 {
                                                            mesRevista8 = "noviembre"
                                                        }else if mes8 == 12 {
                                                            mesRevista8 = "diciembre"
                                                        }
                                                        
                                                        
                                                        if año8 == 2015 {
                                                            añoRevista8 = "2015"
                                                        }else if año8 == 2016 {
                                                            añoRevista8 = "2016"
                                                        }else if año8 == 2017 {
                                                            añoRevista8 = "2017"
                                                        }else if año8 == 2018 {
                                                            añoRevista8 = "2018"
                                                        }else if año8 == 2019 {
                                                            añoRevista8 = "2019"
                                                        }else if año8 == 2020 {
                                                            añoRevista8 = "2020"
                                                        }else if año8 == 2021 {
                                                            añoRevista8 = "2021"
                                                        }else if año8 == 2022 {
                                                            añoRevista8 = "2022"
                                                        }else if año8 == 2023 {
                                                            añoRevista8 = "2023"
                                                        }else if año8 == 2024 {
                                                            añoRevista8 = "2024"
                                                        }else if año8 == 2025 {
                                                            añoRevista8 = "2025"
                                                        }else if año8 == 2026 {
                                                            añoRevista8 = "2026"
                                                        }else if año8 == 2027 {
                                                            añoRevista8 = "2027"
                                                        }else if año8 == 2028 {
                                                            añoRevista8 = "2028"
                                                        }else if año8 == 2029 {
                                                            añoRevista8 = "2029"
                                                        }else if año8 == 2030 {
                                                            añoRevista8 = "2030"
                                                        }
                                                        
                                                        fechaRevista8.text = "\(mesRevista8) \(añoRevista8)"
                                                        let dates8 = "\(mesRevista8) \(añoRevista8)"
                                                        self.arrayF8.append(dates8)
                                                        self.octavoScroll.addSubview(fechaRevista8)
                                                        self.octavoScroll.addSubview(tituloLabel8)
                                                        
                                                    }
                                                    
                                                }
                                                
                                            }
                                            
                                        }
                                        
                                    }
                                    
                                    if titulo == "Mujer" {
                                        
                                        if subKeys == "titulo" {
                                            
                                            let tituloLabel : String = subValues as! String
                                            self.novenoLabel.text = "\(tituloLabel.uppercased())"
                                            
                                        }
                                        
                                        if subKeys == "elementos" {
                                            
                                            let arrayElementos : NSArray = subValues as! NSArray
                                            
                                            for element in arrayElementos {
                                                
                                                let dic3 : NSDictionary = element as! NSDictionary
                                                
                                                for i in dic3 {
                                                    
                                                    let subKey : String = i.key as! String
                                                    let subVal = i.value
                                                    
                                                    let mes9 : Double = dic3["mes"] as! Double
                                                    let año9 : Double = dic3["year"] as! Double
                                                    
                                                    if subKey == "_id" {
                                                        
                                                        let idRevista9 : String = subVal as! String
                                                        
                                                        self.arrayIDs9.append(idRevista9)
                                                        
                                                    }
                                                    
                                                    if subKey == "imagen" {
                                                        
                                                        let urlImage9 : String = subVal as! String
                                                        self.urlImg9.append(urlImage9)
                                                        
                                                        let imagenRevista9 = UIImageView(frame: CGRect(x: self.ejeX9, y: 39, width: 175, height: 225))
                                                        let botonDetalles9 = UIButton(frame: CGRect(x: self.ejeX9, y: 39, width: 175, height: 225))
                                                        let imagenEsquina = UIImageView(frame: CGRect(x: self.ejexCorner9, y: 169, width: 71, height: 95))
                                                        imagenRevista9.backgroundColor = self.backGroundGray
                                                        imagenEsquina.contentMode = .scaleToFill
                                                        imagenEsquina.image = UIImage(named: "Hoja2")
                                                        self.ejexCorner9 = self.ejexCorner9 + self.z
                                                        botonDetalles9.tag = self.botonTag9
                                                        self.botonTag9 = self.botonTag9 + self.add9
                                                        self.imagenes9.append(imagenRevista9)
                                                        self.ejeX9 = self.ejeX9 + self.h
                                                        let scrollW9 = self.ejeX9
                                                        self.novenoScroll.contentSize = CGSize(width: scrollW9, height: 272)
                                                        self.novenoScroll.addSubview(imagenRevista9)
                                                        self.novenoScroll.addSubview(imagenEsquina)
                                                        self.novenoScroll.addSubview(botonDetalles9)
                                                        botonDetalles9.addTarget(self, action: #selector(self.ClickRevista9), for: .touchUpInside)
                                                        
                                                    }
                                                    
                                                    if subKey == "titulo" {
                                                        
                                                        var mesRevista9 = ""
                                                        var añoRevista9 = ""
                                                        let tit9 : String = subVal as! String
                                                        self.revistaTitulo9.append(tit9)
                                                        let tituloLabel9 = UILabel(frame: CGRect(x: self.ejeLabelX9, y: 15, width: 130, height: 15))
                                                        self.ejeLabelX9 = self.ejeLabelX9 + self.h
                                                        tituloLabel9.text = tit9
                                                        tituloLabel9.textColor = UIColor.white
                                                        tituloLabel9.font = UIFont.boldSystemFont(ofSize: 15)
                                                        tituloLabel9.adjustsFontSizeToFitWidth = true
                                                        
                                                        let fechaRevista9 = UILabel(frame: CGRect(x: self.ejeXFechaR9, y: 276, width: 130, height: 15))
                                                        
                                                        self.ejeXFechaR9 = self.ejeXFechaR9 + self.h
                                                        fechaRevista9.textColor = UIColor.white
                                                        fechaRevista9.font = UIFont.boldSystemFont(ofSize: 15)
                                                        fechaRevista9.adjustsFontSizeToFitWidth = true
                                                        
                                                        if mes9 == 1 {
                                                            mesRevista9 = "enero"
                                                        }else if mes9 == 2 {
                                                            mesRevista9 = "febrero"
                                                        }else if mes9 == 3 {
                                                            mesRevista9 = "marzo"
                                                        }else if mes9 == 4 {
                                                            mesRevista9 = "abril"
                                                        }else if mes9 == 5 {
                                                            mesRevista9 = "mayo"
                                                        }else if mes9 == 6 {
                                                            mesRevista9 = "junio"
                                                        }else if mes9 == 7 {
                                                            mesRevista9 = "julio"
                                                        }else if mes9 == 8 {
                                                            mesRevista9 = "agosto"
                                                        }else if mes9 == 9 {
                                                            mesRevista9 = "septiembre"
                                                        }else if mes9 == 10 {
                                                            mesRevista9 = "octubre"
                                                        }else if mes9 == 11 {
                                                            mesRevista9 = "noviembre"
                                                        }else if mes9 == 12 {
                                                            mesRevista9 = "diciembre"
                                                        }
                                                        
                                                        
                                                        if año9 == 2015 {
                                                            añoRevista9 = "2015"
                                                        }else if año9 == 2016 {
                                                            añoRevista9 = "2016"
                                                        }else if año9 == 2017 {
                                                            añoRevista9 = "2017"
                                                        }else if año9 == 2018 {
                                                            añoRevista9 = "2018"
                                                        }else if año9 == 2019 {
                                                            añoRevista9 = "2019"
                                                        }else if año9 == 2020 {
                                                            añoRevista9 = "2020"
                                                        }else if año9 == 2021 {
                                                            añoRevista9 = "2021"
                                                        }else if año9 == 2022 {
                                                            añoRevista9 = "2022"
                                                        }else if año9 == 2023 {
                                                            añoRevista9 = "2023"
                                                        }else if año9 == 2024 {
                                                            añoRevista9 = "2024"
                                                        }else if año9 == 2025 {
                                                            añoRevista9 = "2025"
                                                        }else if año9 == 2026 {
                                                            añoRevista9 = "2026"
                                                        }else if año9 == 2027 {
                                                            añoRevista9 = "2027"
                                                        }else if año9 == 2028 {
                                                            añoRevista9 = "2028"
                                                        }else if año9 == 2029 {
                                                            añoRevista9 = "2029"
                                                        }else if año9 == 2030 {
                                                            añoRevista9 = "2030"
                                                        }
                                                        
                                                        fechaRevista9.text = "\(mesRevista9) \(añoRevista9)"
                                                        let dates9 = "\(mesRevista9) \(añoRevista9)"
                                                        self.arrayF9.append(dates9)
                                                        self.novenoScroll.addSubview(fechaRevista9)
                                                        self.novenoScroll.addSubview(tituloLabel9)
                                                        
                                                    }
                                                    
                                                }
                                                
                                            }
                                            
                                        }
                                        
                                    }
                                    
                                    if titulo == "Musica" {
                                        
                                        if subKeys == "titulo" {
                                            
                                            let tituloLabel : String = subValues as! String
                                            self.decimoLabel.text = "\(tituloLabel.uppercased())"
                                            
                                        }
                                        
                                        if subKeys == "elementos" {
                                            
                                            let arrayElementos : NSArray = subValues as! NSArray
                                            
                                            for element in arrayElementos {
                                                
                                                let dic3 : NSDictionary = element as! NSDictionary
                                                
                                                for i in dic3 {
                                                    
                                                    let subKey : String = i.key as! String
                                                    let subVal = i.value
                                                    
                                                    let mes10 : Double = dic3["mes"] as! Double
                                                    let año10 : Double = dic3["year"] as! Double
                                                    
                                                    if subKey == "_id" {
                                                        
                                                        let idRevista10 : String = subVal as! String
                                                        
                                                        self.arrayIDs10.append(idRevista10)
                                                        
                                                    }
                                                    
                                                    if subKey == "imagen" {
                                                        
                                                        let urlImage10 : String = subVal as! String
                                                        self.urlImg10.append(urlImage10)
                                                        
                                                        let imagenRevista10 = UIImageView(frame: CGRect(x: self.ejeX10, y: 39, width: 175, height: 225))
                                                        let botonDetalles10 = UIButton(frame: CGRect(x: self.ejeX10, y: 39, width: 175, height: 225))
                                                        let imagenEsquina = UIImageView(frame: CGRect(x: self.ejexCorner10, y: 169, width: 71, height: 95))
                                                        imagenRevista10.backgroundColor = self.backGroundGray
                                                        imagenEsquina.contentMode = .scaleToFill
                                                        imagenEsquina.image = UIImage(named: "Hoja2")
                                                        self.ejexCorner10 = self.ejexCorner10 + self.z
                                                        botonDetalles10.tag = self.botonTag10
                                                        self.botonTag10 = self.botonTag10 + self.add10
                                                        self.imagenes10.append(imagenRevista10)
                                                        self.ejeX10 = self.ejeX10 + self.i
                                                        let scrollW10 = self.ejeX10
                                                        self.decimoScroll.contentSize = CGSize(width: scrollW10, height: 272)
                                                        self.decimoScroll.addSubview(imagenRevista10)
                                                        self.decimoScroll.addSubview(imagenEsquina)
                                                        self.decimoScroll.addSubview(botonDetalles10)
                                                        botonDetalles10.addTarget(self, action: #selector(self.ClickRevista10(sender:)), for: .touchUpInside)
                                                        
                                                    }
                                                    
                                                    if subKey == "titulo" {
                                                        
                                                        var mesRevista10 = ""
                                                        var añoRevista10 = ""
                                                        let tit10 : String = subVal as! String
                                                        self.revistaTitulo10.append(tit10)
                                                        let tituloLabel10 = UILabel(frame: CGRect(x: self.ejeLabelX10, y: 15, width: 130, height: 15))
                                                        self.ejeLabelX10 = self.ejeLabelX10 + self.i
                                                        tituloLabel10.text = tit10
                                                        tituloLabel10.textColor = UIColor.white
                                                        tituloLabel10.font = UIFont.boldSystemFont(ofSize: 15)
                                                        tituloLabel10.adjustsFontSizeToFitWidth = true
                                                        
                                                        let fechaRevista10 = UILabel(frame: CGRect(x: self.ejeXFechaR10, y: 276, width: 130, height: 15))
                                                        
                                                        self.ejeXFechaR10 = self.ejeXFechaR10 + self.i
                                                        fechaRevista10.textColor = UIColor.white
                                                        fechaRevista10.font = UIFont.boldSystemFont(ofSize: 15)
                                                        fechaRevista10.adjustsFontSizeToFitWidth = true
                                                        
                                                        if mes10 == 1 {
                                                            mesRevista10 = "enero"
                                                        }else if mes10 == 2 {
                                                            mesRevista10 = "febrero"
                                                        }else if mes10 == 3 {
                                                            mesRevista10 = "marzo"
                                                        }else if mes10 == 4 {
                                                            mesRevista10 = "abril"
                                                        }else if mes10 == 5 {
                                                            mesRevista10 = "mayo"
                                                        }else if mes10 == 6 {
                                                            mesRevista10 = "junio"
                                                        }else if mes10 == 7 {
                                                            mesRevista10 = "julio"
                                                        }else if mes10 == 8 {
                                                            mesRevista10 = "agosto"
                                                        }else if mes10 == 9 {
                                                            mesRevista10 = "septiembre"
                                                        }else if mes10 == 10 {
                                                            mesRevista10 = "octubre"
                                                        }else if mes10 == 11 {
                                                            mesRevista10 = "noviembre"
                                                        }else if mes10 == 12 {
                                                            mesRevista10 = "diciembre"
                                                        }
                                                        
                                                        
                                                        if año10 == 2015 {
                                                            añoRevista10 = "2015"
                                                        }else if año10 == 2016 {
                                                            añoRevista10 = "2016"
                                                        }else if año10 == 2017 {
                                                            añoRevista10 = "2017"
                                                        }else if año10 == 2018 {
                                                            añoRevista10 = "2018"
                                                        }else if año10 == 2019 {
                                                            añoRevista10 = "2019"
                                                        }else if año10 == 2020 {
                                                            añoRevista10 = "2020"
                                                        }else if año10 == 2021 {
                                                            añoRevista10 = "2021"
                                                        }else if año10 == 2022 {
                                                            añoRevista10 = "2022"
                                                        }else if año10 == 2023 {
                                                            añoRevista10 = "2023"
                                                        }else if año10 == 2024 {
                                                            añoRevista10 = "2024"
                                                        }else if año10 == 2025 {
                                                            añoRevista10 = "2025"
                                                        }else if año10 == 2026 {
                                                            añoRevista10 = "2026"
                                                        }else if año10 == 2027 {
                                                            añoRevista10 = "2027"
                                                        }else if año10 == 2028 {
                                                            añoRevista10 = "2028"
                                                        }else if año10 == 2029 {
                                                            añoRevista10 = "2029"
                                                        }else if año10 == 2030 {
                                                            añoRevista10 = "2030"
                                                        }
                                                        
                                                        fechaRevista10.text = "\(mesRevista10) \(añoRevista10)"
                                                        let dates10 = "\(mesRevista10) \(añoRevista10)"
                                                        self.arrayF10.append(dates10)
                                                        self.decimoScroll.addSubview(fechaRevista10)
                                                        self.decimoScroll.addSubview(tituloLabel10)
                                                        
                                                    }
                                                    
                                                }
                                                
                                            }
                                            
                                        }
                                    }
                                    
                                    if titulo == "Negocios" {
                                        
                                        if subKeys == "titulo" {
                                            
                                            let tituloLabel : String = subValues as! String
                                            self.onceavo.text = "\(tituloLabel.uppercased())"
                                            
                                        }
                                        
                                        if subKeys == "elementos" {
                                            
                                            let arrayElementos : NSArray = subValues as! NSArray
                                            
                                            for element in arrayElementos {
                                                
                                                let dic3 : NSDictionary = element as! NSDictionary
                                                
                                                for i in dic3 {
                                                    
                                                    let subKey : String = i.key as! String
                                                    let subVal = i.value
                                                    
                                                    let mes11 : Double = dic3["mes"] as! Double
                                                    let año11 : Double = dic3["year"] as! Double
                                                    
                                                    if subKey == "_id" {
                                                        
                                                        let idRevista11 : String = subVal as! String
                                                        
                                                        self.arrayIDs11.append(idRevista11)
                                                        
                                                    }
                                                    
                                                    if subKey == "imagen" {
                                                        
                                                        let urlImage11 : String = subVal as! String
                                                        self.urlImg11.append(urlImage11)
                                                        
                                                        let imagenRevista11 = UIImageView(frame: CGRect(x: self.ejeX11, y: 39, width: 175, height: 225))
                                                        let botonDetalles11 = UIButton(frame: CGRect(x: self.ejeX11, y: 39, width: 175, height: 225))
                                                        let imagenEsquina = UIImageView(frame: CGRect(x: self.ejexCorner11, y: 169, width: 71, height: 95))
                                                        imagenRevista11.backgroundColor = self.backGroundGray
                                                        imagenEsquina.contentMode = .scaleToFill
                                                        imagenEsquina.image = UIImage(named: "Hoja2")
                                                        self.ejexCorner11 = self.ejexCorner11 + self.z
                                                        botonDetalles11.tag = self.botonTag11
                                                        self.botonTag11 = self.botonTag11 + self.add11
                                                        self.imagenes11.append(imagenRevista11)
                                                        self.ejeX11 = self.ejeX11 + self.j
                                                        let scrollW11 = self.ejeX11
                                                        self.onceavoScroll.contentSize = CGSize(width: scrollW11, height: 272)
                                                        self.onceavoScroll.addSubview(imagenRevista11)
                                                        self.onceavoScroll.addSubview(imagenEsquina)
                                                        self.onceavoScroll.addSubview(botonDetalles11)
                                                        botonDetalles11.addTarget(self, action: #selector(self.ClickRevista11), for: .touchUpInside)
                                                        
                                                    }
                                                    
                                                    if subKey == "titulo" {
                                                        
                                                        var mesRevista11 = ""
                                                        var añoRevista11 = ""
                                                        let tit11 : String = subVal as! String
                                                        self.revistaTitulo11.append(tit11)
                                                        let tituloLabel11 = UILabel(frame: CGRect(x: self.ejeLabelX11, y: 15, width: 130, height: 15))
                                                        self.ejeLabelX11 = self.ejeLabelX11 + self.j
                                                        tituloLabel11.text = tit11
                                                        tituloLabel11.textColor = UIColor.white
                                                        tituloLabel11.font = UIFont.boldSystemFont(ofSize: 15)
                                                        tituloLabel11.adjustsFontSizeToFitWidth = true
                                                        
                                                        let fechaRevista11 = UILabel(frame: CGRect(x: self.ejeXFechaR11, y: 276, width: 130, height: 15))
                                                        
                                                        self.ejeXFechaR11 = self.ejeXFechaR11 + self.j
                                                        fechaRevista11.textColor = UIColor.white
                                                        fechaRevista11.font = UIFont.boldSystemFont(ofSize: 15)
                                                        fechaRevista11.adjustsFontSizeToFitWidth = true
                                                        
                                                        if mes11 == 1 {
                                                            mesRevista11 = "enero"
                                                        }else if mes11 == 2 {
                                                            mesRevista11 = "febrero"
                                                        }else if mes11 == 3 {
                                                            mesRevista11 = "marzo"
                                                        }else if mes11 == 4 {
                                                            mesRevista11 = "abril"
                                                        }else if mes11 == 5 {
                                                            mesRevista11 = "mayo"
                                                        }else if mes11 == 6 {
                                                            mesRevista11 = "junio"
                                                        }else if mes11 == 7 {
                                                            mesRevista11 = "julio"
                                                        }else if mes11 == 8 {
                                                            mesRevista11 = "agosto"
                                                        }else if mes11 == 9 {
                                                            mesRevista11 = "septiembre"
                                                        }else if mes11 == 10 {
                                                            mesRevista11 = "octubre"
                                                        }else if mes11 == 11 {
                                                            mesRevista11 = "noviembre"
                                                        }else if mes11 == 12 {
                                                            mesRevista11 = "diciembre"
                                                        }
                                                        
                                                        
                                                        if año11 == 2015 {
                                                            añoRevista11 = "2015"
                                                        }else if año11 == 2016 {
                                                            añoRevista11 = "2016"
                                                        }else if año11 == 2017 {
                                                            añoRevista11 = "2017"
                                                        }else if año11 == 2018 {
                                                            añoRevista11 = "2018"
                                                        }else if año11 == 2019 {
                                                            añoRevista11 = "2019"
                                                        }else if año11 == 2020 {
                                                            añoRevista11 = "2020"
                                                        }else if año11 == 2021 {
                                                            añoRevista11 = "2021"
                                                        }else if año11 == 2022 {
                                                            añoRevista11 = "2022"
                                                        }else if año11 == 2023 {
                                                            añoRevista11 = "2023"
                                                        }else if año11 == 2024 {
                                                            añoRevista11 = "2024"
                                                        }else if año11 == 2025 {
                                                            añoRevista11 = "2025"
                                                        }else if año11 == 2026 {
                                                            añoRevista11 = "2026"
                                                        }else if año11 == 2027 {
                                                            añoRevista11 = "2027"
                                                        }else if año11 == 2028 {
                                                            añoRevista11 = "2028"
                                                        }else if año11 == 2029 {
                                                            añoRevista11 = "2029"
                                                        }else if año11 == 2030 {
                                                            añoRevista11 = "2030"
                                                        }
                                                        
                                                        fechaRevista11.text = "\(mesRevista11) \(añoRevista11)"
                                                        let dates11 = "\(mesRevista11) \(añoRevista11)"
                                                        self.arrayF11.append(dates11)
                                                        self.onceavoScroll.addSubview(fechaRevista11)
                                                        self.onceavoScroll.addSubview(tituloLabel11)
                                                        
                                                    }
                                                    
                                                }
                                                
                                            }
                                            
                                        }
                                    }
                                    
                                    if titulo == "Noticias" {
                                        
                                        if subKeys == "titulo" {
                                            
                                            let tituloLabel : String = subValues as! String
                                            self.doceavoLabel.text = "\(tituloLabel.uppercased())"
                                            
                                        }
                                        
                                        if subKeys == "elementos" {
                                            
                                            let arrayElementos : NSArray = subValues as! NSArray
                                            
                                            for element in arrayElementos {
                                                
                                                let dic3 : NSDictionary = element as! NSDictionary
                                                
                                                for i in dic3 {
                                                    
                                                    let subKey : String = i.key as! String
                                                    let subVal = i.value
                                                    
                                                    let mes12 : Double = dic3["mes"] as! Double
                                                    let año12 : Double = dic3["year"] as! Double
                                                    
                                                    if subKey == "_id" {
                                                        
                                                        let idRevista12 : String = subVal as! String
                                                        
                                                        self.arrayIDs12.append(idRevista12)
                                                        
                                                    }
                                                    
                                                    if subKey == "imagen" {
                                                        
                                                        let urlImage12 : String = subVal as! String
                                                        self.urlImg12.append(urlImage12)
                                                        
                                                        let imagenRevista12 = UIImageView(frame: CGRect(x: self.ejeX12, y: 39, width: 175, height: 225))
                                                        
                                                        let botonDetalles12 = UIButton(frame: CGRect(x: self.ejeX12, y: 39, width: 175, height: 225))
                                                        let imagenEsquina = UIImageView(frame: CGRect(x: self.ejexCorner12, y: 169, width: 71, height: 95))
                                                        imagenRevista12.backgroundColor = self.backGroundGray
                                                        imagenEsquina.contentMode = .scaleToFill
                                                        imagenEsquina.image = UIImage(named: "Hoja2")
                                                        self.ejexCorner12 = self.ejexCorner12 + self.z
                                                        botonDetalles12.tag = self.botonTag12
                                                        self.botonTag12 = self.botonTag12 + self.add12
                                                        self.imagenes12.append(imagenRevista12)
                                                        self.ejeX12 = self.ejeX12 + self.k
                                                        let scrollW12 = self.ejeX12
                                                        self.doceavoScroll.contentSize = CGSize(width: scrollW12, height: 272)
                                                        self.doceavoScroll.addSubview(imagenRevista12)
                                                        self.doceavoScroll.addSubview(imagenEsquina)
                                                        self.doceavoScroll.addSubview(botonDetalles12)
                                                        botonDetalles12.addTarget(self, action: #selector(self.ClickRevista12), for: .touchUpInside)
                                                        
                                                    }
                                                    
                                                    if subKey == "titulo" {
                                                        
                                                        var mesRevista12 = ""
                                                        var añoRevista12 = ""
                                                        let tit12 : String = subVal as! String
                                                        self.revistaTitulo12.append(tit12)
                                                        let tituloLabel12 = UILabel(frame: CGRect(x: self.ejeLabelX12, y: 15, width: 130, height: 15))
                                                        self.ejeLabelX12 = self.ejeLabelX12 + self.k
                                                        tituloLabel12.text = tit12
                                                        tituloLabel12.textColor = UIColor.white
                                                        tituloLabel12.font = UIFont.boldSystemFont(ofSize: 15)
                                                        tituloLabel12.adjustsFontSizeToFitWidth = true
                                                        
                                                        let fechaRevista12 = UILabel(frame: CGRect(x: self.ejeXFechaR12, y: 276, width: 130, height: 15))
                                                        
                                                        self.ejeXFechaR12 = self.ejeXFechaR12 + self.k
                                                        fechaRevista12.textColor = UIColor.white
                                                        fechaRevista12.font = UIFont.boldSystemFont(ofSize: 15)
                                                        fechaRevista12.adjustsFontSizeToFitWidth = true
                                                        
                                                        if mes12 == 1 {
                                                            mesRevista12 = "enero"
                                                        }else if mes12 == 2 {
                                                            mesRevista12 = "febrero"
                                                        }else if mes12 == 3 {
                                                            mesRevista12 = "marzo"
                                                        }else if mes12 == 4 {
                                                            mesRevista12 = "abril"
                                                        }else if mes12 == 5 {
                                                            mesRevista12 = "mayo"
                                                        }else if mes12 == 6 {
                                                            mesRevista12 = "junio"
                                                        }else if mes12 == 7 {
                                                            mesRevista12 = "julio"
                                                        }else if mes12 == 8 {
                                                            mesRevista12 = "agosto"
                                                        }else if mes12 == 9 {
                                                            mesRevista12 = "septiembre"
                                                        }else if mes12 == 10 {
                                                            mesRevista12 = "octubre"
                                                        }else if mes12 == 11 {
                                                            mesRevista12 = "noviembre"
                                                        }else if mes12 == 12 {
                                                            mesRevista12 = "diciembre"
                                                        }
                                                        
                                                        
                                                        if año12 == 2015 {
                                                            añoRevista12 = "2015"
                                                        }else if año12 == 2016 {
                                                            añoRevista12 = "2016"
                                                        }else if año12 == 2017 {
                                                            añoRevista12 = "2017"
                                                        }else if año12 == 2018 {
                                                            añoRevista12 = "2018"
                                                        }else if año12 == 2019 {
                                                            añoRevista12 = "2019"
                                                        }else if año12 == 2020 {
                                                            añoRevista12 = "2020"
                                                        }else if año12 == 2021 {
                                                            añoRevista12 = "2021"
                                                        }else if año12 == 2022 {
                                                            añoRevista12 = "2022"
                                                        }else if año12 == 2023 {
                                                            añoRevista12 = "2023"
                                                        }else if año12 == 2024 {
                                                            añoRevista12 = "2024"
                                                        }else if año12 == 2025 {
                                                            añoRevista12 = "2025"
                                                        }else if año12 == 2026 {
                                                            añoRevista12 = "2026"
                                                        }else if año12 == 2027 {
                                                            añoRevista12 = "2027"
                                                        }else if año12 == 2028 {
                                                            añoRevista12 = "2028"
                                                        }else if año12 == 2029 {
                                                            añoRevista12 = "2029"
                                                        }else if año12 == 2030 {
                                                            añoRevista12 = "2030"
                                                        }
                                                        
                                                        fechaRevista12.text = "\(mesRevista12) \(añoRevista12)"
                                                        let dates12 = "\(mesRevista12) \(añoRevista12)"
                                                        self.arrayF12.append(dates12)
                                                        self.doceavoScroll.addSubview(fechaRevista12)
                                                        self.doceavoScroll.addSubview(tituloLabel12)
                                                        
                                                    }
                                                    
                                                }
                                                
                                            }
                                            
                                        }
                                        
                                    }
                                    
                                    if titulo == "Salud" {
                                        
                                        if subKeys == "titulo" {
                                            
                                            let tituloLabel : String = subValues as! String
                                            self.treceavoLabel.text = "\(tituloLabel.uppercased())"
                                            
                                        }
                                        
                                        if subKeys == "elementos" {
                                            
                                            let arrayElementos : NSArray = subValues as! NSArray
                                            
                                            for element in arrayElementos {
                                                
                                                let dic3 : NSDictionary = element as! NSDictionary
                                                
                                                for i in dic3 {
                                                    
                                                    let subKey : String = i.key as! String
                                                    let subVal = i.value
                                                    
                                                    let mes13 : Double = dic3["mes"] as! Double
                                                    let año13 : Double = dic3["year"] as! Double
                                                    
                                                    if subKey == "_id" {
                                                        
                                                        let idRevista13 : String = subVal as! String
                                                        
                                                        self.arrayIDs13.append(idRevista13)
                                                        
                                                    }
                                                    
                                                    if subKey == "imagen" {
                                                        
                                                        let urlImage13 : String = subVal as! String
                                                        self.urlImg13.append(urlImage13)
                                                        
                                                        let imagenRevista13 = UIImageView(frame: CGRect(x: self.ejeX13, y: 39, width: 175, height: 225))
                                                        let botonDetalles13 = UIButton(frame: CGRect(x: self.ejeX13, y: 39, width: 175, height: 225))
                                                        let imagenEsquina = UIImageView(frame: CGRect(x: self.ejexCorner13, y: 169, width: 71, height: 95))
                                                        imagenRevista13.backgroundColor = self.backGroundGray
                                                        imagenEsquina.contentMode = .scaleToFill
                                                        imagenEsquina.image = UIImage(named: "Hoja2")
                                                        self.ejexCorner13 = self.ejexCorner13 + self.z
                                                        botonDetalles13.tag = self.botonTag13
                                                        self.botonTag13 = self.botonTag13 + self.add13
                                                        self.imagenes13.append(imagenRevista13)
                                                        self.ejeX13 = self.ejeX13 + self.l
                                                        let scrollW13 = self.ejeX13
                                                        self.treceavoScroll.contentSize = CGSize(width: scrollW13, height: 272)
                                                        self.treceavoScroll.addSubview(imagenRevista13)
                                                        self.treceavoScroll.addSubview(imagenEsquina)
                                                        self.treceavoScroll.addSubview(botonDetalles13)
                                                        botonDetalles13.addTarget(self, action: #selector(self.ClickRevista13), for: .touchUpInside)
                                                        
                                                    }
                                                    
                                                    if subKey == "titulo" {
                                                        
                                                        var mesRevista13 = ""
                                                        var añoRevista13 = ""
                                                        let tit13 : String = subVal as! String
                                                        self.revistaTitulo13.append(tit13)
                                                        let tituloLabel13 = UILabel(frame: CGRect(x: self.ejeLabelX13, y: 15, width: 130, height: 15))
                                                        self.ejeLabelX13 = self.ejeLabelX13 + self.l
                                                        tituloLabel13.text = tit13
                                                        tituloLabel13.textColor = UIColor.white
                                                        tituloLabel13.font = UIFont.boldSystemFont(ofSize: 15)
                                                        tituloLabel13.adjustsFontSizeToFitWidth = true
                                                        
                                                        let fechaRevista13 = UILabel(frame: CGRect(x: self.ejeXFechaR13, y: 276, width: 130, height: 15))
                                                        
                                                        self.ejeXFechaR13 = self.ejeXFechaR13 + self.l
                                                        fechaRevista13.textColor = UIColor.white
                                                        fechaRevista13.font = UIFont.boldSystemFont(ofSize: 15)
                                                        fechaRevista13.adjustsFontSizeToFitWidth = true
                                                        
                                                        if mes13 == 1 {
                                                            mesRevista13 = "enero"
                                                        }else if mes13 == 2 {
                                                            mesRevista13 = "febrero"
                                                        }else if mes13 == 3 {
                                                            mesRevista13 = "marzo"
                                                        }else if mes13 == 4 {
                                                            mesRevista13 = "abril"
                                                        }else if mes13 == 5 {
                                                            mesRevista13 = "mayo"
                                                        }else if mes13 == 6 {
                                                            mesRevista13 = "junio"
                                                        }else if mes13 == 7 {
                                                            mesRevista13 = "julio"
                                                        }else if mes13 == 8 {
                                                            mesRevista13 = "agosto"
                                                        }else if mes13 == 9 {
                                                            mesRevista13 = "septiembre"
                                                        }else if mes13 == 10 {
                                                            mesRevista13 = "octubre"
                                                        }else if mes13 == 11 {
                                                            mesRevista13 = "noviembre"
                                                        }else if mes13 == 12 {
                                                            mesRevista13 = "diciembre"
                                                        }
                                                        
                                                        
                                                        if año13 == 2015 {
                                                            añoRevista13 = "2015"
                                                        }else if año13 == 2016 {
                                                            añoRevista13 = "2016"
                                                        }else if año13 == 2017 {
                                                            añoRevista13 = "2017"
                                                        }else if año13 == 2018 {
                                                            añoRevista13 = "2018"
                                                        }else if año13 == 2019 {
                                                            añoRevista13 = "2019"
                                                        }else if año13 == 2020 {
                                                            añoRevista13 = "2020"
                                                        }else if año13 == 2021 {
                                                            añoRevista13 = "2021"
                                                        }else if año13 == 2022 {
                                                            añoRevista13 = "2022"
                                                        }else if año13 == 2023 {
                                                            añoRevista13 = "2023"
                                                        }else if año13 == 2024 {
                                                            añoRevista13 = "2024"
                                                        }else if año13 == 2025 {
                                                            añoRevista13 = "2025"
                                                        }else if año13 == 2026 {
                                                            añoRevista13 = "2026"
                                                        }else if año13 == 2027 {
                                                            añoRevista13 = "2027"
                                                        }else if año13 == 2028 {
                                                            añoRevista13 = "2028"
                                                        }else if año13 == 2029 {
                                                            añoRevista13 = "2029"
                                                        }else if año13 == 2030 {
                                                            añoRevista13 = "2030"
                                                        }
                                                        
                                                        fechaRevista13.text = "\(mesRevista13) \(añoRevista13)"
                                                        let dates13 = "\(mesRevista13) \(añoRevista13)"
                                                        self.arrayF13.append(dates13)
                                                        self.treceavoScroll.addSubview(fechaRevista13)
                                                        self.treceavoScroll.addSubview(tituloLabel13)
                                                        
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        
                        if keys == "Categorias" {
                            
                            
                        }
                        
                    }
                    
                    Timer.scheduledTimer(withTimeInterval: 3 , repeats: false){ (timer) in
                        self.DescargaImg()
                    }
                    
                }
                
            }catch{
                
                print("Error")
                
            }
            
        }.resume()
        
    }
        
        
        // MARK: - LO MAS NUEVO BOTONES

        @objc func ClickRevista(sender:UIButton) {
            
            self.datosID = sender.tag
            self.catID = 1
            self.performSegue(withIdentifier: "TOINFOREVISTA", sender: nil)
            
            
        }
        
        @objc func ClickRevista2(sender:UIButton) {
            
            self.datosID2 = sender.tag
            self.catID = 2
            self.performSegue(withIdentifier: "TOINFOREVISTA2", sender: nil)
        }
        
        @objc func ClickRevista3(sender:UIButton) {
               
               self.datosID3 = sender.tag
               self.performSegue(withIdentifier: "TOINFOREVISTA3", sender: nil)
           }
        
        
        @objc func ClickRevista4(sender:UIButton) {
               
               self.datosID4 = sender.tag
               self.performSegue(withIdentifier: "TOINFOREVISTA4", sender: nil)
           }
        
        @objc func ClickRevista5(sender:UIButton) {
               
               self.datosID5 = sender.tag
               self.performSegue(withIdentifier: "TOINFOREVISTA5", sender: nil)
           }
        
        
        @objc func ClickRevista6(sender:UIButton) {
               
               self.datosID6 = sender.tag
               self.performSegue(withIdentifier: "TOINFOREVISTA6", sender: nil)
           }
        @objc func ClickRevista7(sender:UIButton) {
                  
                  self.datosID7 = sender.tag
                  self.performSegue(withIdentifier: "TOINFOREVISTA7", sender: nil)
              }
        @objc func ClickRevista8(sender:UIButton) {
                  
                  self.datosID8 = sender.tag
                  self.performSegue(withIdentifier: "TOINFOREVISTA8", sender: nil)
              }
        @objc func ClickRevista9(sender:UIButton) {
                  
                  self.datosID9 = sender.tag
                  self.performSegue(withIdentifier: "TOINFOREVISTA9", sender: nil)
              }
        @objc func ClickRevista10(sender:UIButton) {
                  
                  self.datosID10 = sender.tag
                  self.performSegue(withIdentifier: "TOINFOREVISTA10", sender: nil)
              }
        @objc func ClickRevista11(sender:UIButton) {
                     
                     self.datosID11 = sender.tag
                     self.performSegue(withIdentifier: "TOINFOREVISTA11", sender: nil)
                 }
        @objc func ClickRevista12(sender:UIButton) {
                     
                     self.datosID12 = sender.tag
                     self.performSegue(withIdentifier: "TOINFOREVISTA12", sender: nil)
                 }
        @objc func ClickRevista13(sender:UIButton) {
                     
                     self.datosID13 = sender.tag
                     self.performSegue(withIdentifier: "TOINFOREVISTA13", sender: nil)
                 }
        
        @IBAction func PErfi(_ sender: Any) {
            
            self.performSegue(withIdentifier: "TOPERFIL", sender: nil)
            
        }
        
        // MARK: - FUNCIONES DE DISEÑO
        
        func sizeScreen() {
             if UIDevice().userInterfaceIdiom == .phone {
                 switch UIScreen.main.nativeBounds.height {
                 case 1136:
                     print("IPhone 5 o SE")
                 case 1334:
                     print("IPhone 6, 6s, 7 u 8")
                    
                 case 1920:
                     print("IPhone plus") // incorrecto
                 case 2436:
                     print("IPhone X, XS y 11 Pro")

                 case 1792:
                     print("IPhone XR y 11")
                 case 2688:
                     print("IPhone XS Max y 11 Pro Max")
                
                 default:
                     print("Cualquier otro tamaño")

                 }
             }
         }
        
       
    // MARK: - GESTOS
    
    @objc func tapOn() {
        
    }
    
}

