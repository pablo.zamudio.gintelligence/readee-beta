//
//  FavoritosController.swift
//  Readee.Beta
//
//  Created by Pablo Luis Velazquez Zamudio on 23/05/20.
//  Copyright © 2020 P.Z.Scorpion. All rights reserved.
//

import UIKit
import CoreData
import Network

class FavoritosController: UIViewController, UIScrollViewDelegate {

    var valorArray : Int = 0
    var ejeXView : Double = 15
    var ejey : Double = 20
    var tag : Int = 0
    var favoriteID : String!
    var tags : Int!
    var arrayView : [UIView] = Array()
    var arrayImage : [UIImageView] = Array()
    var arrayTitle : [UILabel] = Array()
    var arrayDate : [UILabel] = Array()
    var arrayUrlImg : [String] = Array()
    var arrayTitulos : [String] = Array()
    var arrayFechas : [String] = Array()
    var arrayIds : [String] = Array()
    var arraySlugs : [String] = Array()
    var elementos : NSArray!
    var queryPath : String!
    var scrollTop = false
    var timerTest : Timer?
    var iniciOrMagazine : Bool!
    var firstAppear = 0
    var viewAppearID = true
    var deleteID : String!
    
//    lazy var refreshControl: UIRefreshControl = {
//    let refresControl = UIRefreshControl()
//        refresControl.addTarget(self, action: #selector(ActualizarDatos), for: .valueChanged)
//        refresControl.tintColor = UIColor.white
//
//
//        return refresControl
//
//    }()
    
    @IBOutlet weak var principalView: UIView!
    @IBOutlet weak var principalScroll: UIScrollView!
    @IBOutlet weak var loaderView: UIView!
    @IBOutlet weak var gifLoader: UIImageView!
    @IBOutlet weak var wifiAlertView: UIView!
    
    
    func conexion()-> NSManagedObjectContext {
            let delegate = UIApplication.shared.delegate as! AppDelegate
            return delegate.persistentContainer.viewContext
        }
    
     let networkMonitor = NWPathMonitor()
        
    // MARK: - FUNCIONES OVERRIDE
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        networkMonitor.pathUpdateHandler = { path in
            if path.status == .satisfied {
                print("Estás conectado a la red")
                DispatchQueue.main.async {
                    UIView.animate(withDuration: 0.4, animations: {
                        self.wifiAlertView.alpha = 0
                    })
                    if self.firstAppear == 0 {
                        if self.iniciOrMagazine != true {
                            self.GetFavoritos()
                        }
                        self.gifLoader.loadGif(name: "loader_readee")
                    }
                   
                }
            } else {
                print("No estás conectado a la red")
                DispatchQueue.main.async {
                    
                    UIView.animate(withDuration: 0.4, animations: {
                        self.wifiAlertView.alpha = 1
                        self.loaderView.alpha = 0
                    })
                }
                
            }
        }
        
        let queue = DispatchQueue(label: "Network connectivity")
        networkMonitor.start(queue: queue)
        
      // DISEÑO
      //  principalScroll?.addSubview(refreshControl)
        principalScroll?.delegate = self
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewDidLoad()
        
        networkMonitor.pathUpdateHandler = { path in
            if path.status == .satisfied {
                print("Estás conectado a la red")
                DispatchQueue.main.async {
                    UIView.animate(withDuration: 0.4, animations: {
                        self.wifiAlertView.alpha = 0
                    })
                    if self.iniciOrMagazine == true {
                         self.ejeXView = 15
                         self.ejey = 20
                         
                         self.loaderView.alpha = 1
                         self.arrayView.forEach{ (view) in
                             view.removeFromSuperview()
                         }
                         self.arrayImage.forEach{ (imagen) in
                             imagen.removeFromSuperview()
                         }
                         self.arrayTitle.forEach{ (label) in
                             label.removeFromSuperview()
                         }
                         self.arrayImage.removeAll()
                         self.arrayView.removeAll()
                         self.arrayTitle.removeAll()
                         
                        self.FromRevistaView()
                         
                         
                     }
                     
                     if self.iniciOrMagazine != true {
                         if self.firstAppear != 0 {
                             self.loaderView.alpha = 1
                             self.gifLoader.loadGif(name: "loader_readee")
                             
                             UIView.animate(withDuration: 0.3, animations: {
                                 self.arrayView.forEach{ (view) in
                                     view.alpha = 9.5
                                 }
                             })
                            self.FromInicio()
                            self.StartTimer()
                             
                         }
                     }
                    }
                
            } else {
                print("No estás conectado a la red")
                DispatchQueue.main.async {
                    UIView.animate(withDuration: 0.4, animations: {
                        self.wifiAlertView.alpha = 1
                        self.loaderView.alpha = 0
                    })
                }
                
            }
        }
        
        let queue = DispatchQueue(label: "Network connectivity")
        networkMonitor.start(queue: queue)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        AppUtility.lockOrientation(.portrait)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewDidLoad()
        
        //  let revista = RevistaInfoController.singleton
        if self.firstAppear == 0 {
            self.firstAppear = 1
        }
        
        if self.iniciOrMagazine == true {
            self.iniciOrMagazine = false
        }
        
        AppUtility.lockOrientation(.all)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "FROMFAVORITOS" {
            let destino = segue.destination as! RevistaInfoController
            destino.fromID = "From favorites"
            destino.queryParamID = self.arrayIds[self.tags]
            
        }
        
    }
    
    // MARK: - FUNCIONES TIMER 
    
    func FromRevistaView() {
        let revista = RevistaInfoController.singleton
        self.gifLoader?.loadGif(name: "loader_readee")
        UIView.animate(withDuration: 0.3, animations: {
            self.loaderView?.alpha = 9.5
        })
        
        Timer.scheduledTimer(withTimeInterval: 3, repeats: false) { (timer) in
            
            if revista.errorAlert == "error" {
                self.AlertErrorFavorite()
            }else{
                self.GetFavoritos()
            }
        }
    }
    
    func FromInicio() {
        
        self.ejeXView = 15
        self.ejey = 20
        
        self.arrayView.forEach{ (view) in
            view.removeFromSuperview()
        }
        self.arrayImage.forEach{ (imagen) in
            imagen.removeFromSuperview()
        }
        self.arrayTitle.forEach{ (label) in
            label.removeFromSuperview()
        }
        self.arrayImage.removeAll()
        self.arrayView.removeAll()
        self.arrayTitle.removeAll()
        self.GetFavoritos()
        
    }
    func StartTimer() {
        
        guard timerTest == nil else {return}
        
        timerTest = Timer.scheduledTimer(withTimeInterval: 1, repeats: true, block: { (timer) in
            self.arrayView.last?.bounce()
        })
        
    }
    
    func StopTimer() {
        
        timerTest?.invalidate()
        timerTest = nil
        
    }
        
    // MARK: - FUNCIONES GENERALES
    
    func alert () {
        let revista = RevistaInfoController.singleton
        let alerta = UIAlertController(title: "", message: "\(revista.tituloForAlert ?? "ouch") se ha añadido a tus favoritos.", preferredStyle: .alert)
        let accion = UIAlertAction(title: "Entendido", style: .default) { (_) in
            print("Click en el boton")
            self.navigationController?.popViewController(animated: true)
        }
        
        alerta.addAction(accion)
        present(alerta, animated: true, completion: nil)
        
    }
    
    func AlertDeleteFavortie() {
        let revista = RevistaInfoController.singleton
        let alerta = UIAlertController(title: "", message: "Hemos quitado \(revista.tituloForAlert ?? "ouch") de tus favoritos.", preferredStyle: .alert)
        let accion = UIAlertAction(title: "Entendido", style: .default) { (_) in
            print("Click en el boton")
            self.navigationController?.popViewController(animated: true)
        }
        
        alerta.addAction(accion)
        
        present(alerta, animated: true, completion: nil)
        
    }
    
    func AlertErrorFavorite() {
        let alerta = UIAlertController(title: "", message: "Tuvimos un problema. Intenta de nuevo por favor.", preferredStyle: .alert)
        let accion = UIAlertAction(title: "Volver a intentar", style: .default) { (_) in
            print("Click en el boton")
            self.navigationController?.popViewController(animated: true)
        }
        
        alerta.addAction(accion)
        present(alerta, animated: true, completion: nil)
    }
    
    // MARK: - SERVICIOS REST API
    
    func GetFavoritos() {
        let revista = RevistaInfoController.singleton
        self.tag = 0
        guard let url = URL(string: "https://testapi.ginmag.ga/v1/favoritos/")
            else { return }
        var request = URLRequest(url: url)
        let contexto = conexion()
        let fetchRequest : NSFetchRequest<Usuarios> = Usuarios.fetchRequest()
        do {
            let resultados = try contexto.fetch(fetchRequest)
            // print("Numero de tokens: \(resultados.count)")
            for res in resultados as [NSManagedObject] {
                let token = res.value(forKey: "accessToken")
                
                request.httpMethod = "GET"
                request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                request.setValue("Bearer \(token ?? "ouch")", forHTTPHeaderField: "Authorization")
                
                
            }
        }catch let error as NSError {
            print("Error al mostrar token", error.localizedDescription)
            
        }
        
        URLSession.shared.dataTask(with: request) { (data, response, _) in
            
            do {
                
                let json2 = try JSONSerialization.jsonObject(with: data!, options: .allowFragments)
                // print("AQUI ESTAN TUS REVISTAS FAVORITAS: \(json2)")
                DispatchQueue.main.async {
                    
                    let principalDictionary : NSDictionary = json2 as! NSDictionary
                    
                    for item in principalDictionary {
                        
                        let key : String = item.key as! String
                        let value = item.value
                        
                        if key == "message" {
                            
                            print("jwt expired")
                            self.RenovarToken3()
                            
                        }
                        
                        if key == "elementos" {
                            let elementos : NSArray = principalDictionary["elementos"] as! NSArray
                            self.elementos = elementos
                            
                        }
                    }
                    
                    if self.elementos != NSArray() {
                        self.principalView.alpha = 1
                        for item in self.elementos {
                            
                            let revistaFavorita : NSDictionary = item as! NSDictionary
                            let imagen : String = revistaFavorita["imagen"] as! String
                            let titulo : String = revistaFavorita["titulo"] as! String
                            let idElemento : String = revistaFavorita["elemento_id"] as! String
                            let slug : String = revistaFavorita["slug"] as! String
                            let mes : Int = revistaFavorita["mes"] as! Int
                            let año : Int = revistaFavorita["year"] as! Int
                            
                            var mesRevista = ""
                            var añoRevista = ""
                            if mes == 1 {
                                mesRevista = "enero"
                            }else if mes == 2 {
                                mesRevista = "febrero"
                            }else if mes == 3 {
                                mesRevista = "marzo"
                            }else if mes == 4 {
                                mesRevista = "abril"
                            }else if mes == 5 {
                                mesRevista = "mayo"
                            }else if mes == 6 {
                                mesRevista = "junio"
                            }else if mes == 7 {
                                mesRevista = "julio"
                            }else if mes == 8 {
                                mesRevista = "agosto"
                            }else if mes == 9 {
                                mesRevista = "septiembre"
                            }else if mes == 10 {
                                mesRevista = "octubre"
                            }else if mes == 11 {
                                mesRevista = "noviembre"
                            }else if mes == 12 {
                                mesRevista = "diciembre"
                            }
                            
                            if año == 2015 {
                                añoRevista = "2015"
                            }else if año == 2016 {
                                añoRevista = "2016"
                            }else if año == 2017 {
                                añoRevista = "2017"
                            }else if año == 2018 {
                                añoRevista = "2018"
                            }else if año == 2019 {
                                añoRevista = "2019"
                            }else if año == 2020 {
                                añoRevista = "2020"
                            }else if año == 2021 {
                                añoRevista = "2021"
                            }else if año == 2022 {
                                añoRevista = "2022"
                            }else if año == 2023 {
                                añoRevista = "2023"
                            }else if año == 2024 {
                                añoRevista = "2024"
                            }else if año == 2025 {
                                añoRevista = "2025"
                            }else if año == 2026 {
                                añoRevista = "2026"
                            }else if año == 2027 {
                                añoRevista = "2027"
                            }else if año == 2028 {
                                añoRevista = "2028"
                            }else if año == 2029 {
                                añoRevista = "2029"
                            }else if año == 2030 {
                                añoRevista = "2030"
                            }
                            let fecha = "\(mesRevista) \(añoRevista)"
                            
                            if self.arrayUrlImg.count <= self.arrayImage.count {
                                self.arrayUrlImg.append(imagen)
                                self.arrayTitulos.append(titulo)
                                self.arrayFechas.append(fecha)
                                self.arrayIds.append(idElemento)
                                self.arraySlugs.append(slug)
                            }
                            let dateLabel = UILabel(frame: CGRect(x: 8, y: 261, width: 140, height: 20))
                            let titleLabel = UILabel(frame: CGRect(x: 8, y: 10, width: 140, height: 20))
                            let favoriteView = UIView(frame: CGRect(x: self.ejeXView, y: self.ejey, width: 160, height: 290))
                            let favoriteImage = UIImageView(frame: CGRect(x: 0, y: 37, width: 160, height: 219))
                            let favoriteButton = UIButton(frame: CGRect(x: 0, y: 37, width: 160, height: 219))
                            favoriteButton.addTarget(self, action: #selector(self.ClickFavorite), for: .touchUpInside)
                            self.arrayView.append(favoriteView)
                            self.arrayImage.append(favoriteImage)
                            self.arrayTitle.append(titleLabel)
                            self.arrayDate.append(dateLabel)
                            favoriteView.backgroundColor = UIColor.black
                            favoriteView.layer.cornerRadius = 17
                            favoriteImage.contentMode = .scaleToFill
                            titleLabel.text = titulo
                            titleLabel.textColor = UIColor.white
                            titleLabel.numberOfLines = 2
                            titleLabel.font = UIFont.boldSystemFont(ofSize: 15)
                            titleLabel.adjustsFontSizeToFitWidth = true
                            dateLabel.text = fecha
                            dateLabel.textColor = UIColor.white
                            dateLabel.font = UIFont.boldSystemFont(ofSize: 15)
                            dateLabel.adjustsFontSizeToFitWidth = true
                            self.downloadFavoriteImage(imagen, inView: favoriteImage)
                            favoriteView.addSubview(favoriteImage)
                            favoriteView.addSubview(titleLabel)
                            favoriteView.addSubview(dateLabel)
                            favoriteView.addSubview(favoriteButton)
                            self.principalScroll.addSubview(favoriteView)
                            let newEjeX = self.view.frame.width - 175
                            print("TAMAÑO VIEW: \(newEjeX)")
                            if self.ejeXView == Double(newEjeX) {
                                
                                self.ejeXView = 15
                                self.ejey = self.ejey + 308
                                
                            }else {
                                self.ejeXView = self.ejeXView + 224
                            }
                            
                            favoriteButton.tag = self.tag
                            if self.tag < self.arrayUrlImg.count {
                                self.tag = self.tag + 1
                                
                            }
                            
                            self.principalScroll.contentSize = CGSize(width: 375, height: favoriteView.frame.maxY + 25)
                            
                        } // FIN DEL FOR ELEMENTOS
                        
                        if revista.isInFavorites == true {
                            revista.isInFavorites = nil
                            self.StartTimer()
                            self.alert()
                            let bottomOffset = CGPoint(x: 0, y: self.principalScroll.contentSize.height - self.principalScroll.bounds.size.height + 25)
                            self.principalScroll.setContentOffset(bottomOffset, animated: true)
                            
                            
                        }else
                            if revista.isInFavorites == false{
                                revista.isInFavorites = nil
                                self.StopTimer()
                                self.AlertDeleteFavortie()
                                let bottomOffset = CGPoint(x: 0, y: self.principalScroll.contentSize.height - self.principalScroll.bounds.size.height + 25)
                                self.principalScroll.setContentOffset(bottomOffset, animated: true)
                                
                        }
                        
                    }else{
                        self.principalView.alpha = 0
                        
                    } // FIN DE ELSE CONDICIONAL DA ARRAY != DE VACIO()
                    
                    UIView.animate(withDuration: 0.3, animations: {
                        self.loaderView.alpha = 0
                    })
                    
                    if self.deleteID != nil {
                        if let index = self.arrayIds.firstIndex(of: self.deleteID) {
                            self.arrayIds.remove(at: index)
                        }
                        
                    }
                    
                    if self.arrayIds.count > self.arrayView.count {
                        
                        self.arrayIds.remove(at: self.tags)
                        
                    }
                    print("ELEMENTOS EN EL ARRAY ID's: \(self.arrayIds.count)")
                    
                } // FIN DEL DISPATCH
                
            }catch let error as NSError{
                
                DispatchQueue.main.async {
                    
                    print(error)
                    
                } // FINAL DEL DISPATCH DE ERROR
                
            } // FINAL DEL CATCH
            
        }.resume() // FINAL DEL TASK
        
    } // FIN DE LA FUNCION GET FAVORITOS
    
    func RenovarToken3() {
        
        let fetchRequest1 : NSFetchRequest<Usuarios> = Usuarios.fetchRequest()
        guard let datos = URL(string: "https://testapi.ginmag.ga/v1/usuarios/renew") else { return }
        var request = URLRequest(url: datos)
        
        let contexto = conexion()
        let fetchRequest : NSFetchRequest<Usuarios> = Usuarios.fetchRequest()
        do {
            let resultados = try contexto.fetch(fetchRequest)
            // print("Numero de tokens: \(resultados.count)")
            for res in resultados as [NSManagedObject] {
                let refreshToken = res.value(forKey: "refToken")
                let parametros: [String : String] = [
                    
                    "refreshToken" : "\(refreshToken ?? "ouch")"
                    
                ]
                
                let body = try! JSONSerialization.data(withJSONObject: parametros)
                request.httpMethod = "POST"
                request.httpBody = body
                request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                
            }
        }catch let error as NSError {
            print("Error al mostrar token", error.localizedDescription)
            
        }
        
        URLSession.shared.dataTask(with: request) { (data, response, _) in
            
            do {
                
                let result = try contexto.fetch(fetchRequest1)
                let json = try JSONSerialization.jsonObject(with: data!, options: .allowFragments)
                // print(json)
                print(type(of: json))
                
                DispatchQueue.main.async {
                    
                    let dictionaryJSON : NSDictionary = json as! NSDictionary
                    let newAccessToken  = dictionaryJSON["token"]
                    let resul = result[0] as NSManagedObject
                    resul.setValue(newAccessToken, forKey: "accessToken")
                    
                    do {
                        try contexto.save()
                        self.ejeXView = 15
                        self.ejey = 20
                        self.arrayView.forEach{ (view) in
                            view.removeFromSuperview()
                        }
                        self.arrayImage.forEach{ (imagen) in
                            imagen.removeFromSuperview()
                        }
                        self.arrayTitle.forEach{ (label) in
                            label.removeFromSuperview()
                        }
                        self.arrayImage.removeAll()
                        self.arrayView.removeAll()
                        self.arrayTitle.removeAll()
                        self.GetFavoritos()
                        print("Exito al sobre escribir new accessToken")
                        
                    }catch let error as NSError{
                        print("Error al guardar token", error.localizedDescription)
                    }
                    
                }
                
            }catch{
                
                print("Error")
                
            }
            
        }.resume()
    }
    
    func downloadFavoriteImage(_ uri : String, inView: UIImageView) {
        let url = URL(string: uri)
        
        let task = URLSession.shared.dataTask(with: url!) {responseData,response,error in
            if error == nil {
                if let data = responseData {
                    
                    DispatchQueue.main.async {
                        
                        inView.image = UIImage(data: data)
                        
                    }
                    
                }else {
                    print("no data")
                }
            }else{
                print("error")
            }
        }
        task.resume()
        
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        scrollingFinish()
    }
        
    func scrollingFinish() -> Void {
        if principalScroll.isAtTop {
            if self.scrollTop == true {
            }
            self.scrollTop = true
        }
        
        if principalScroll.isAtBottom {
            self.scrollTop = false
        }
    }
    
    @objc func ClickFavorite(sender: UIButton) {
        
        print("TAG: \(sender.tag)")
        self.tags = sender.tag
        self.performSegue(withIdentifier: "FROMFAVORITOS", sender: nil)
        StopTimer()
        
    }
//
//        @objc func ActualizarDatos(_ refresControl: UIRefreshControl) {
//
//            self.scrollTop = false
//            UIView.animate(withDuration: 0.3, animations: {
//                self.arrayView.forEach{ (view) in
//                    view.alpha = 0
//                }
//            })
//            self.ejeXView = 15
//            self.ejey = 20
//
//            Timer.scheduledTimer(withTimeInterval: 5, repeats: false) { (timer) in
//
//                self.arrayView.forEach{ (view) in
//                    view.removeFromSuperview()
//                }
//                self.arrayImage.forEach{ (imagen) in
//                    imagen.removeFromSuperview()
//                }
//                self.arrayTitle.forEach{ (label) in
//                    label.removeFromSuperview()
//                }
//                self.arrayImage.removeAll()
//                self.arrayView.removeAll()
//                self.arrayTitle.removeAll()
//                self.GetFavoritos()
//                self.scrollTop = false
//                self.refreshControl.endRefreshing()
//            }
//
//        }

}
