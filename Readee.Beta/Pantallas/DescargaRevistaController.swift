//
//  DescargaRevistaController.swift
//  Readee.Beta
//
//  Created by Pablo Luis Velazquez Zamudio on 26/05/20.
//  Copyright © 2020 P.Z.Scorpion. All rights reserved.
//

import UIKit


class DescargaRevistaController: UIViewController {
    
    @IBOutlet weak var text: UITextView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        let template = URL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent("playboy.pdf") as NSURL
//
//        // Fill buffer with a C string representing the local file system path.
//        var buffer = [Int8](repeating: 0, count: Int(PATH_MAX))
//        template.getFileSystemRepresentation(&buffer, maxLength: buffer.count)
//
//        // Create unique file name (and open file):
//        let fd = mkstemp(&buffer)
//        if fd != -1 {
//
//            // Create URL from file system string:
//            let url = URL(fileURLWithFileSystemRepresentation: buffer, isDirectory: false, relativeTo: nil)
//            print("PATH: \(url.path)")
//
//        } else {
//            print("Error: " + String(cString: strerror(errno)))
//        }
//
//
//        let sourcePath = URL(fileURLWithPath: "/private/var/mobile/Containers/Data/Application/tmp", isDirectory: true)
//    //    let destinationPath = URL(fileURLWithPath: "/Users/josuevhn/Documents", isDirectory: true)
//
//        let someFile: URL = URL(fileURLWithPath: "test2.txt", relativeTo: sourcePath)
//
//        let stringLine: String = "Datos de ejemplo a añadir al archivo!"
//
//        let dataToFile: Data = stringLine.data(using: String.Encoding.utf8)!
//
//        let fileManager = FileManager.default
//
//        fileManager.createFile(atPath: someFile.path, contents: dataToFile, attributes: nil)
//
//        if fileManager.fileExists(atPath: someFile.path) {
//
//            print("El archivo existe")
//
//        } else {
//
//            print("Archivo no encontrado")
//
//        }
//
        
//     let str = "Super long string here"
//     let filename = getDocumentsDirectory().appendingPathComponent("prueba1.txt")
//
//     do {
//         try str.write(to: filename, atomically: true, encoding: String.Encoding.utf8)
//     } catch {
//         // failed to write file – bad permissions, bad filename, missing permissions, or more likely it can't be converted to the encoding
//     }
        
    let edgePan = UIScreenEdgePanGestureRecognizer(target: self, action: #selector(screenEdgeSwiped))
    edgePan.edges = .left
    view.addGestureRecognizer(edgePan)
        
    }
    
    
    
   @objc func screenEdgeSwiped(_ recognizer: UIScreenEdgePanGestureRecognizer) {
          navigationController?.popViewController(animated: true)
     }
    
    func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask)
        print(paths)
        return paths[0]
        
    }
    
    @IBAction func loadPDf(_ sender: Any) {
        
        let fileURL = Bundle.main.url(forResource: "playboy", withExtension: "pdf")
        //Convertimos el archivo a NSData
        let dataAsNs = NSData.init(contentsOf: fileURL!)
        //Lo convertimos a un texto legible.
        let stringOfDataNS : String = dataAsNs!.base64EncodedString(options: NSData.Base64EncodingOptions.init(rawValue: 0))
    }
    
    @IBAction func VerBytes(_ sender: Any) {
        
       let fileURL = Bundle.main.url(forResource: "playboy", withExtension: "pdf")
       //Convertimos el archivo a NSData
       let dataAsNs = NSData.init(contentsOf: fileURL!)
       //Lo convertimos a un texto legible.
       let stringOfDataNS : String = dataAsNs!.base64EncodedString(options: NSData.Base64EncodingOptions.init(rawValue: 0))
       
        // Save data to file
           let fileName = "Test2"
           let DocumentDirURL = try! FileManager.default.url(for: .cachesDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
           
           let fileURLcache = DocumentDirURL.appendingPathComponent(fileName).appendingPathExtension("txt")
           print("FilePath: \(fileURLcache.path)")
           
           let writeString = stringOfDataNS
           do {
               // Write to the file
               try writeString.write(to: fileURLcache, atomically: true, encoding: String.Encoding.utf8)
           } catch let error as NSError {
               print("Failed writing to URL: \(fileURLcache), Error: " + error.localizedDescription)
           }
        
           var readString = "" // Used to store the file contents
           do {
               // Read the file contents
               readString = try String(contentsOf: fileURLcache)
           } catch let error as NSError {
               print("Failed reading from URL: \(fileURLcache), Error: " + error.localizedDescription)
           }
           //print("File Text: \(readString)")
           text.text = readString
        
    }
    
}
