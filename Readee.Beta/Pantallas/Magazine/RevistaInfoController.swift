//
//  RevistaInfoController.swift
//  Readee.Beta
//
//  Created by Pablo Luis Velazquez Zamudio on 23/05/20.
//  Copyright © 2020 P.Z.Scorpion. All rights reserved.
//

import UIKit
import CoreData

class RevistaInfoController: UIViewController {
    
    // MARK: - FUNCION CRUD CORE DATA
       
       func conexion()-> NSManagedObjectContext {
              let delegate = UIApplication.shared.delegate as! AppDelegate
              return delegate.persistentContainer.viewContext
          }

    // MARK: - SINGLETON VARIABLE
    static let singleton = RevistaInfoController()
    
    // MARK: - VARIABLES GLOBALES
    
    var queryParamID : String!
    var descripcionR : String!
    var imagenR : String!
    var fechaR : String!
    var tituloR : String!
    var revistaR : NSDictionary!
    var tagsR : NSArray!
    var favoritoID : String!
    var idR : String!
    var activoR : Int!
    var costoR : String!
    var createdR : String!
    var estatusR : String!
    var tipoR : String!
    var viewsR : Int!
    var tituloForAlert : String!
    var errorAlert : String!
    var fromID : String!
    var tagID : Int!

    var descripciones2 : [String] = Array()
    var arrayEtiquet : [String] = Array()
    var newArrayEtiquetas : [String] = Array()
    var arrayImagenes : [UIImageView] = Array()
    var titulos2 : [String] = Array()
    var arrayLabelTit : [UILabel] = Array()
    var arrayFechas : [String] = Array()
    var arrayIDsRevista2 : [String] = Array()
    var urlImage2 : [String] = Array()
    var arrayEtiqueta2 : [String] = Array()
    var arrayLikeID : [String] = Array()
    
    var ejexCorner : Double = 123.9
    var ejex : Double = 20
    var ejeXtitulo : Double = 28
    var ejeXfecha : Double = 20
    let a : Double = 210
    var botonTag = 0
    let add = 1
    var tag2 : Int!
    
    var state: Bool = false
    var IDfavoritos = false
    var idViewDelegate : Bool!
    var isInFavorites : Bool!

    @IBOutlet weak var scrollNumerosAnteriores: UIScrollView!
    @IBOutlet weak var principalScroll: UIScrollView!
    @IBOutlet weak var descripicionLabel: UILabel!
    @IBOutlet weak var bottomContatnt: NSLayoutConstraint!
    @IBOutlet weak var principalView: UIView!
    @IBOutlet weak var imagenMuestra: UIImageView!
    @IBOutlet weak var tituloLabel: UILabel!
    @IBOutlet weak var fechaLabel: UILabel!
    @IBOutlet weak var verMasButton: UIButton!
    @IBOutlet var etiquetasCollection: [UIButton]!
    @IBOutlet weak var categoriaLabel: UILabel!
    @IBOutlet weak var publicacionLabel: UILabel!
    @IBOutlet weak var verRevista: UIButton!
    @IBOutlet weak var verDescargarRevista: UIButton!
    var arrayLabelFecha : [UILabel] = Array()
    @IBOutlet weak var tituloAnteriores: UILabel!
    @IBOutlet weak var loaderView: UIView!
    @IBOutlet weak var gifLoader: UIImageView!
    @IBOutlet weak var emptyHeart: UIImageView!
    @IBOutlet weak var favorites: UIButton!
    
    
    // MARK: - FUNCIONES OVERRIDE
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        GetNumerosAnteriores()
        GetFavoritosInicio()
        descripicionLabel.text = "We all love Autolayout"
        principalScroll.contentSize = CGSize(width: 414, height: 1000)
        etiquetasCollection.forEach { (button) in
            button.layer.cornerRadius = 7
            button.addTarget(self, action: #selector(ClickEtiqueta), for: .touchUpInside)
        }
        verRevista.layer.cornerRadius = 25
        verDescargarRevista.layer.cornerRadius = 25
        self.gifLoader.loadGif(name: "loader_readee")
        
        let edgePan = UIScreenEdgePanGestureRecognizer(target: self, action: #selector(screenEdgeSwiped))
        edgePan.edges = .left
        view.addGestureRecognizer(edgePan)

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        AppUtility.lockOrientation(.portrait)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
       AppUtility.lockOrientation(.all)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        let revista = RevistaInfoController.singleton
        if segue.identifier == "ADDFAVORITE" {
            let destino = segue.destination as! FavoritosController
            destino.iniciOrMagazine = revista.idViewDelegate
            destino.deleteID = self.idR
            
        }
    }
    
    
    // MARK: - FUNCIONES GENERALES 
    
    @objc func screenEdgeSwiped(_ recognizer: UIScreenEdgePanGestureRecognizer) {
        navigationController?.popViewController(animated: true)
   }
    
    @IBAction func vermas(_ sender: UIButton) {
           state = !state
           if state {
               descripicionLabel.text = self.descripcionR
                   descripicionLabel.numberOfLines = 0
               let maximumLabelSize: CGSize = CGSize(width: 280, height: 9999)
               let expectedLabelSize: CGSize = descripicionLabel.sizeThatFits(maximumLabelSize)
    
                   var newFrame: CGRect = descripicionLabel.frame
                   newFrame.size.height = expectedLabelSize.height
                   descripicionLabel.frame = newFrame
            bottomContatnt.constant = bottomContatnt.constant - descripicionLabel.frame.height + 200
            print("TAMAÑO DEL LABEL2: \(self.tituloAnteriores.frame.maxY)")
            verMasButton.setTitle("Ver menos", for: .normal)
           
           }else{
        
            verMasButton.setTitle("Ver mas...", for: .normal)
               descripicionLabel.numberOfLines = 4
               let maximumLabelSize: CGSize = CGSize(width: 280, height: 100)
               let expectedLabelSize: CGSize = descripicionLabel.sizeThatFits(maximumLabelSize)
               var newFrame: CGRect = descripicionLabel.frame
               newFrame.size.height = expectedLabelSize.height
               descripicionLabel.frame = newFrame
            bottomContatnt.constant = 1226
            
           }
               
           
       }
    
    func isInFavoritesF() {
        
        if self.arrayLikeID.contains(self.queryParamID) {
            self.IDfavoritos = true
            self.emptyHeart.image = UIImage(named: "favorite_full")
            
        }else{
            
            self.IDfavoritos = false 
            self.emptyHeart.image = UIImage(named: "favorite_empty")
        }
        
    }
    
     func FuncionDatosGenrales() {
            
        downloadImage(self.imagenR, inView: imagenMuestra)
        tituloLabel.text = self.tituloR
        fechaLabel.text = self.fechaR
        descripicionLabel.text = self.descripcionR
        
            for item in self.tagsR {
                let singelton = RevistaInfoController.singleton
                let diccionario : NSDictionary = item as! NSDictionary

                for dic in diccionario {
                    let key : String = dic.key as! String
                    let value = dic.value

                    if key == "nombre" {
                    let nombre : String = value as! String
                    self.arrayEtiquet.append(nombre)
                    singelton.newArrayEtiquetas = self.arrayEtiquet

                    }
                  }
                }
             self.DatosEtiquetas()

                for it in self.revistaR {

                    let key : String = it.key as! String
                    let value = it.value

                    if key == "categorias" {
                        let dicc : NSArray = value as! NSArray
                        for i in dicc {

                            let subDiccionario: NSDictionary = i as! NSDictionary
                            for sub in subDiccionario {

                                let subkey : String = sub.key as! String
                                let subvalue : String = sub.value as! String
                                if subkey == "nombre"{
                                self.categoriaLabel.text = "Categoría: \(subvalue)"
                                }

                            }
                        }

                    }
                    if key == "periodicidad" {
                        let periodicidad : String = value as! String
                        self.publicacionLabel.text = "Publicación: \(periodicidad)"
                    }
                }

            UIView.animate(withDuration: 0.3, animations: {
            //  self.viewLoader.alpha = 0
                                 })
        }
    
    
    func DatosEtiquetas() {
        
        let arrayCount = self.arrayEtiquet.count
        if arrayCount == 0 {
            
            self.etiquetasCollection.forEach { (button) in
                button.isHidden = true 
            }
            //            heightEtiquetas.constant = heightEtiquetas.constant - 75
            //            botonFavoritoH.constant = botonFavoritoH.constant - 75
            //            fullHeight.constant = fullHeight.constant - 75
            //            emptyHeight.constant = emptyHeight.constant - 75
            //            heightScrollView.constant = heightScrollView.constant + 75
        }else if arrayCount == 1 {
            self.etiquetasCollection[0].setTitle(self.arrayEtiquet[0], for: .normal)
            let collection = self.etiquetasCollection[1...14]
            collection.forEach { (button) in
                button.isHidden = true
            }
            //            heightEtiquetas.constant = heightEtiquetas.constant - 60
            //            botonFavoritoH.constant = botonFavoritoH.constant - 60
            //            fullHeight.constant = fullHeight.constant - 60
            //            emptyHeight.constant = emptyHeight.constant - 60
            //            heightScrollView.constant = heightScrollView.constant + 60
            //
        }else if arrayCount == 2 {
            let newCollection = self.etiquetasCollection[0...1]
            let newEtiqueta = self.arrayEtiquet[0...1]
            var i = 0
            
            if i < 2 {
                newCollection.forEach { (button) in
                    button.setTitle(newEtiqueta[i], for: .normal)
                    i = i + 1
                }
            }
            let collection = self.etiquetasCollection[2...14]
            collection.forEach { (button) in
                button.isHidden = true
            }
            //            heightEtiquetas.constant = heightEtiquetas.constant - 60
            //            botonFavoritoH.constant = botonFavoritoH.constant - 60
            //            fullHeight.constant = fullHeight.constant - 60
            //            emptyHeight.constant = emptyHeight.constant - 60
            //            heightScrollView.constant = heightScrollView.constant + 60
        }else if arrayCount == 3 {
            let newCollection = self.etiquetasCollection[0...2]
            let newEtiqueta = self.arrayEtiquet[0...2]
            var i = 0
            
            if i < 3 {
                newCollection.forEach { (button) in
                    button.setTitle(newEtiqueta[i], for: .normal)
                    i = i + 1
                }
            }
            let collection = self.etiquetasCollection[3...14]
            collection.forEach { (button) in
                button.isHidden = true
            }
            //            heightEtiquetas.constant = heightEtiquetas.constant - 60
            //            botonFavoritoH.constant = botonFavoritoH.constant - 60
            //            fullHeight.constant = fullHeight.constant - 60
            //            emptyHeight.constant = emptyHeight.constant - 60
            //            heightScrollView.constant = heightScrollView.constant + 60
            //
        }else if arrayCount == 4 {
            let newCollection = self.etiquetasCollection[0...3]
            let newEtiqueta = self.arrayEtiquet[0...3]
            var i = 0
            
            if i < 4 {
                newCollection.forEach { (button) in
                    button.setTitle(newEtiqueta[i], for: .normal)
                    i = i + 1
                }
            }
            let collection = self.etiquetasCollection[4...14]
            collection.forEach { (button) in
                button.isHidden = true
            }
            //            heightEtiquetas.constant = heightEtiquetas.constant - 60
            //            botonFavoritoH.constant = botonFavoritoH.constant - 60
            //            fullHeight.constant = fullHeight.constant - 60
            //            emptyHeight.constant = emptyHeight.constant - 60
            //            heightScrollView.constant = heightScrollView.constant + 60
            //
        }else if arrayCount == 5 {
            let newCollection = self.etiquetasCollection[0...4]
            let newEtiqueta = self.arrayEtiquet[0...4]
            var i = 0
            
            if i < 5 {
                newCollection.forEach { (button) in
                    button.setTitle(newEtiqueta[i], for: .normal)
                    i = i + 1
                }
            }
            let collection = self.etiquetasCollection[5...14]
            collection.forEach { (button) in
                button.isHidden = true
            }
            //            heightEtiquetas.constant = heightEtiquetas.constant - 50
            //            botonFavoritoH.constant = botonFavoritoH.constant - 50
            //            fullHeight.constant = fullHeight.constant - 50
            //            emptyHeight.constant = emptyHeight.constant - 50
            //            heightScrollView.constant = heightScrollView.constant + 50
            //
        }else if arrayCount == 6 {
            let newCollection = self.etiquetasCollection[0...5]
            let newEtiqueta = self.arrayEtiquet[0...5]
            var i = 0
            
            if i < 6 {
                newCollection.forEach { (button) in
                    button.setTitle(newEtiqueta[i], for: .normal)
                    i = i + 1
                }
            }
            //            heightEtiquetas.constant = heightEtiquetas.constant - 50
            //            botonFavoritoH.constant = botonFavoritoH.constant - 50
            //            fullHeight.constant = fullHeight.constant - 50
            //            emptyHeight.constant = emptyHeight.constant - 50
            //            heightScrollView.constant = heightScrollView.constant + 50
            //
            let collection = self.etiquetasCollection[6...14]
            collection.forEach { (button) in
                button.isHidden = true
            }
            
        }else if arrayCount == 7 {
            let newCollection = self.etiquetasCollection[0...6]
            let newEtiqueta = self.arrayEtiquet[0...6]
            var i = 0
            
            if i < 7 {
                newCollection.forEach { (button) in
                    button.setTitle(newEtiqueta[i], for: .normal)
                    i = i + 1
                }
            }
            let collection = self.etiquetasCollection[7...14]
            collection.forEach { (button) in
                button.isHidden = true
            }
            //            heightEtiquetas.constant = heightEtiquetas.constant - 50
            //            botonFavoritoH.constant = botonFavoritoH.constant - 50
            //            fullHeight.constant = fullHeight.constant - 50
            //            emptyHeight.constant = emptyHeight.constant - 50
            //            heightScrollView.constant = heightScrollView.constant + 50
            
        }else if arrayCount == 8 {
            let newCollection = self.etiquetasCollection[0...7]
            let newEtiqueta = self.arrayEtiquet[0...7]
            var i = 0
            
            if i < 8 {
                newCollection.forEach { (button) in
                    button.setTitle(newEtiqueta[i], for: .normal)
                    i = i + 1
                }
            }
            let collection = self.etiquetasCollection[8...14]
            collection.forEach { (button) in
                button.isHidden = true
            }
            //            heightEtiquetas.constant = heightEtiquetas.constant - 50
            //            botonFavoritoH.constant = botonFavoritoH.constant - 50
            //            fullHeight.constant = fullHeight.constant - 50
            //            emptyHeight.constant = emptyHeight.constant - 50
            //            heightScrollView.constant = heightScrollView.constant + 50
            
        }else if arrayCount == 9 {
            let newCollection = self.etiquetasCollection[0...8]
            let newEtiqueta = self.arrayEtiquet[0...8]
            var i = 0
            
            if i < 9 {
                newCollection.forEach { (button) in
                    button.setTitle(newEtiqueta[i], for: .normal)
                    i = i + 1
                }
            }
            let collection = self.etiquetasCollection[9...14]
            collection.forEach { (button) in
                button.isHidden = true
            }
            //            heightEtiquetas.constant = heightEtiquetas.constant - 30
            //            botonFavoritoH.constant = botonFavoritoH.constant - 30
            //            fullHeight.constant = fullHeight.constant - 30
            //            emptyHeight.constant = emptyHeight.constant - 30
            //            heightScrollView.constant = heightScrollView.constant + 30
            
        }else if arrayCount == 10 {
            let newCollection = self.etiquetasCollection[0...9]
            let newEtiqueta = self.arrayEtiquet[0...9]
            var i = 0
            
            if i < 10 {
                newCollection.forEach { (button) in
                    button.setTitle(newEtiqueta[i], for: .normal)
                    i = i + 1
                }
            }
            
            let collection = self.etiquetasCollection[10...14]
            collection.forEach { (button) in
                button.isHidden = true
            }
            //            heightEtiquetas.constant = heightEtiquetas.constant - 30
            //            botonFavoritoH.constant = botonFavoritoH.constant - 30
            //            fullHeight.constant = fullHeight.constant - 30
            //            emptyHeight.constant = emptyHeight.constant - 30
            //            heightScrollView.constant = heightScrollView.constant + 30
            
        }else if arrayCount == 11 {
            let newCollection = self.etiquetasCollection[0...10]
            let newEtiqueta = self.arrayEtiquet[0...10]
            var i = 0
            
            if i < 11 {
                newCollection.forEach { (button) in
                    button.setTitle(newEtiqueta[i], for: .normal)
                    i = i + 1
                }
            }
            let collection = self.etiquetasCollection[11...14]
            collection.forEach { (button) in
                button.isHidden = true
            }
            //            heightEtiquetas.constant = heightEtiquetas.constant - 30
            //            botonFavoritoH.constant = botonFavoritoH.constant - 30
            //            fullHeight.constant = fullHeight.constant - 30
            //            emptyHeight.constant = emptyHeight.constant - 30
            //            heightScrollView.constant = heightScrollView.constant + 30
            
        }else if arrayCount == 12 {
            let newCollection = self.etiquetasCollection[0...11]
            let newEtiqueta = self.arrayEtiquet[0...11]
            var i = 0
            
            if i < 12 {
                newCollection.forEach { (button) in
                    button.setTitle(newEtiqueta[i], for: .normal)
                    i = i + 1
                }
            }
            let collection = self.etiquetasCollection[12...14]
            collection.forEach { (button) in
                button.isHidden = true
            }
            
            //            heightEtiquetas.constant = heightEtiquetas.constant - 30
            //            botonFavoritoH.constant = botonFavoritoH.constant - 30
            //            fullHeight.constant = fullHeight.constant - 30
            //            emptyHeight.constant = emptyHeight.constant - 30
            //            heightScrollView.constant = heightScrollView.constant + 30
            //
        }else if arrayCount == 13 {
            let newCollection = self.etiquetasCollection[0...12]
            let newEtiqueta = self.arrayEtiquet[0...12]
            var i = 0
            
            if i < 13 {
                newCollection.forEach { (button) in
                    button.setTitle(newEtiqueta[i], for: .normal)
                    i = i + 1
                }
            }
            let collection = self.etiquetasCollection[13...14]
            collection.forEach { (button) in
                button.isHidden = true
            }
            
        }else if arrayCount == 14 {
            let newCollection = self.etiquetasCollection[0...13]
            let newEtiqueta = self.arrayEtiquet[0...13]
            var i = 0
            
            if i < 14 {
                newCollection.forEach { (button) in
                    button.setTitle(newEtiqueta[i], for: .normal)
                    i = i + 1
                }
            }
            let collection = self.etiquetasCollection[14...14]
            collection.forEach { (button) in
                button.isHidden = true
            }
            
        }else if arrayCount == 15 {
            
            let newCollection = self.etiquetasCollection[0...14]
            let newEtiqueta = self.arrayEtiquet[0...14]
            var i = 0
            
            if i < 15 {
                newCollection.forEach { (button) in
                    button.setTitle(newEtiqueta[i], for: .normal)
                    i = i + 1
                }
            }
            self.etiquetasCollection[15].isHidden = true
        }else if arrayCount >= 16 {
                        
            let newCollection = self.etiquetasCollection[0...14]
            let newEtiqueta = self.arrayEtiquet[0...14]
            var i = 0
            
            if i < 15 {
                newCollection.forEach { (button) in
                    button.setTitle(newEtiqueta[i], for: .normal)
                    i = i + 1
                }
            }
        }
        
        UIView.animate(withDuration: 0.3, animations: {
            self.loaderView.alpha = 0
        })
        
    }
        
    // MARK: - SERVICIOS API REST
    
    func GetNumerosAnteriores() {
        let revista2 = RevistaInfoController.singleton
        guard let url = URL(string: "https://testapi.ginmag.ga/v1/numeros/\(self.queryParamID ?? "ouch")")
        else { return }

    var request = URLRequest(url: url)
    request.httpMethod = "GET"
    request.setValue("application/json", forHTTPHeaderField: "Content-Type")

    URLSession.shared.dataTask(with: request) { (data, response, _) in

        do {

           let json2 = try JSONSerialization.jsonObject(with: data!, options: .allowFragments)
           // print(json2)

            DispatchQueue.main.async {

                let diccionarioNumerosAnteriores : NSDictionary = json2 as! NSDictionary
                let numerosAnteriores : NSArray = diccionarioNumerosAnteriores["numerosAnteriores"] as! NSArray
                let id : String = diccionarioNumerosAnteriores["_id"] as! String
                let activo : Int = diccionarioNumerosAnteriores["activo"] as! Int
                let costo : String = diccionarioNumerosAnteriores["costo"] as! String
                let created : String = diccionarioNumerosAnteriores["created_at"] as! String
                let descDic : String = diccionarioNumerosAnteriores["descripcion"] as! String
                let tituloDic : String = diccionarioNumerosAnteriores["titulo"] as! String
                let estatus : String = diccionarioNumerosAnteriores["estatus"] as! String
                let imagen : String = diccionarioNumerosAnteriores["imagen"] as! String
                let tgs : NSArray = diccionarioNumerosAnteriores["tags"] as! NSArray
                let revista : NSDictionary = diccionarioNumerosAnteriores["revista"] as! NSDictionary
                let tipo : String = diccionarioNumerosAnteriores["tipo"] as! String
                let mes : Int = diccionarioNumerosAnteriores["mes"] as! Int
                let año : Int = diccionarioNumerosAnteriores["year"] as! Int
               // let views : Int = diccionarioNumerosAnteriores["views"] as! Int
                var mesRev = ""
                var añoRev = ""
                
                if mes == 1 {
                mesRev = "enero"
                }else if mes == 2 {
                mesRev = "febrero"
                }else if mes == 3 {
                mesRev = "marzo"
                }else if mes == 4 {
                mesRev = "abril"
                }else if mes == 5 {
                mesRev = "mayo"
                }else if mes == 6 {
                mesRev = "junio"
                }else if mes == 7 {
                mesRev = "julio"
                }else if mes == 8 {
                mesRev = "agosto"
                }else if mes == 9 {
                mesRev = "septiembre"
                }else if mes == 10 {
                mesRev = "octubre"
                }else if mes == 11 {
                mesRev = "noviembre"
                }else if mes == 12 {
                mesRev = "diciembre"
                }

                if año == 2015 {
                añoRev = "2015"
                }else if año == 2016 {
                añoRev = "2016"
                }else if año == 2017 {
                añoRev = "2017"
                }else if año == 2018 {
                añoRev = "2018"
                }else if año == 2019 {
                añoRev = "2019"
                }else if año == 2020 {
                añoRev = "2020"
                }else if año == 2021 {
                añoRev = "2021"
                }else if año == 2022 {
                añoRev = "2022"
                }else if año == 2023 {
                añoRev = "2023"
                }else if año == 2024 {
                añoRev = "2024"
                }else if año == 2025 {
                añoRev = "2025"
                }else if año == 2026 {
                añoRev = "2026"
                }else if año == 2027 {
                añoRev = "2027"
                }else if año == 2028 {
                añoRev = "2028"
                }else if año == 2029 {
                añoRev = "2029"
                }else if año == 2030 {
                añoRev = "2030"
                }
                
                let fechaRevista2 = "\(mesRev) \(añoRev)"
                print("ID: \(id)")
                print("ACTIVO: \(activo)")
                print("COSTO: \(costo)")
                print("CREATED: \(created)")
                print("DESCRIPCION: \(descDic)")
                print("TITULO: \(tituloDic)")
                print("ESTATUS: \(estatus)")
                print("IMAGEN: \(imagen)")
                print("TAGS: \(tgs)")
                print("REVISTA: \(revista)")
                print("TIPO: \(tipo)")
               // print("VIEWS: \(views)")
                print("FECHA: \(fechaRevista2)")
                 
                self.idR = id
                self.activoR = activo
                self.costoR = costo
                self.createdR = created
                self.descripcionR = descDic
                self.tituloR = tituloDic
                revista2.tituloForAlert = tituloDic
                self.estatusR = estatus
                self.imagenR = imagen
                self.tagsR = tgs
                self.revistaR = revista
                self.tipoR = tipo
               // self.viewsR = views
                self.fechaR = fechaRevista2

                self.botonTag = 0

                for item in numerosAnteriores {

                    let diccionario : NSDictionary = item as! NSDictionary
                    let mes : Double = diccionario["mes"] as! Double
                    let año : Double = diccionario["year"] as! Double

                    for dic in diccionario {

                        let keys : String = dic.key as! String
                        let values = dic.value
                        if keys == "imagen" {

                            let imgURL : String = values as! String
                            self.urlImage2.append(imgURL)

                let imagenNumeroAnterior = UIImageView(frame: CGRect(x: self.ejex, y: 39, width: 175, height: 225))
                let imagenEsquina = UIImageView(frame: CGRect(x: self.ejexCorner, y: 169, width: 71, height: 95))
                imagenEsquina.contentMode = .scaleToFill
                imagenEsquina.image = UIImage(named: "Hoja2")
                self.ejexCorner = self.ejexCorner + self.a
                self.downloadImageNumerosAnteriores(imgURL, inView:imagenNumeroAnterior)
                let botonDetalles = UIButton(frame: CGRect(x: self.ejex, y: 39, width: 175, height: 225))
                botonDetalles.tag = self.botonTag
                self.botonTag = self.botonTag + self.add
                self.ejex = self.ejex + self.a
                let scrollW = self.ejex
                self.arrayImagenes.append(imagenNumeroAnterior)
                self.scrollNumerosAnteriores.contentSize = CGSize(width: scrollW, height: 324)
                self.scrollNumerosAnteriores.addSubview(imagenNumeroAnterior)
                self.scrollNumerosAnteriores.addSubview(botonDetalles)
                self.scrollNumerosAnteriores.addSubview(imagenEsquina)
                botonDetalles.addTarget(self, action: #selector(self.ClickRevista2), for: .touchUpInside)

                        }

                        if keys == "titulo"{

                        var mesRevista = ""
                        var añoRevista = ""

                        let titleLabel : String = values as! String
                        self.titulos2.append(titleLabel)
                        let tituloLabel = UILabel(frame: CGRect(x: self.ejeXtitulo, y: 15, width: 130, height: 15))
                        self.ejeXtitulo = self.ejeXtitulo + self.a
                        tituloLabel.text = titleLabel
                        tituloLabel.textColor = UIColor.white
                        tituloLabel.font = UIFont.boldSystemFont(ofSize: 15)
                        tituloLabel.adjustsFontSizeToFitWidth = true
                        self.arrayLabelTit.append(tituloLabel)
                        self.scrollNumerosAnteriores.addSubview(tituloLabel)

                        let fechaRevista = UILabel(frame: CGRect(x: self.ejeXfecha, y: 276, width: 130, height: 15))
                        self.ejeXfecha = self.ejeXfecha + self.a
                        fechaRevista.textColor = UIColor.white
                        fechaRevista.font = UIFont.boldSystemFont(ofSize: 15)
                        fechaRevista.adjustsFontSizeToFitWidth = true

                        if mes == 1 {
                        mesRevista = "enero"
                        }else if mes == 2 {
                        mesRevista = "febrero"
                        }else if mes == 3 {
                        mesRevista = "marzo"
                        }else if mes == 4 {
                        mesRevista = "abril"
                        }else if mes == 5 {
                        mesRevista = "mayo"
                        }else if mes == 6 {
                        mesRevista = "junio"
                        }else if mes == 7 {
                        mesRevista = "julio"
                        }else if mes == 8 {
                        mesRevista = "agosto"
                        }else if mes == 9 {
                        mesRevista = "septiembre"
                        }else if mes == 10 {
                        mesRevista = "octubre"
                        }else if mes == 11 {
                        mesRevista = "noviembre"
                        }else if mes == 12 {
                        mesRevista = "diciembre"
                        }

                        if año == 2015 {
                        añoRevista = "2015"
                        }else if año == 2016 {
                        añoRevista = "2016"
                        }else if año == 2017 {
                        añoRevista = "2017"
                        }else if año == 2018 {
                        añoRevista = "2018"
                        }else if año == 2019 {
                        añoRevista = "2019"
                        }else if año == 2020 {
                        añoRevista = "2020"
                        }else if año == 2021 {
                        añoRevista = "2021"
                        }else if año == 2022 {
                        añoRevista = "2022"
                        }else if año == 2023 {
                        añoRevista = "2023"
                        }else if año == 2024 {
                        añoRevista = "2024"
                        }else if año == 2025 {
                        añoRevista = "2025"
                        }else if año == 2026 {
                        añoRevista = "2026"
                        }else if año == 2027 {
                        añoRevista = "2027"
                        }else if año == 2028 {
                        añoRevista = "2028"
                        }else if año == 2029 {
                        añoRevista = "2029"
                        }else if año == 2030 {
                        añoRevista = "2030"
                        }

                        fechaRevista.text = "\(mesRevista) \(añoRevista)"
                        let nuevaFecha = fechaRevista.text
                        self.arrayFechas.append(nuevaFecha!)
                        self.arrayLabelFecha.append(fechaRevista)
                        self.scrollNumerosAnteriores.addSubview(fechaRevista)

                        }

                         if keys == "descripcion"{

                            let descripcion2 : String = values as! String
                            self.descripciones2.append(descripcion2)

                        }

                        if keys == "_id" {

                            let idRevista : String = values as! String
                            self.arrayIDsRevista2.append(idRevista)
                        }

                    }

                }
            
                Timer.scheduledTimer(withTimeInterval: 3 , repeats: false){ (timer) in
                    self.FuncionDatosGenrales()
                    }
                }
            
        }catch let error as NSError{

            DispatchQueue.main.async {
                print(error)
            }
        }
      }.resume()
    }
    
    
    func GetFavoritosInicio() {
        
        guard let url = URL(string: "https://testapi.ginmag.ga/v1/favoritos/")
            else { return }
        var request = URLRequest(url: url)
        let contexto = conexion()
        let fetchRequest : NSFetchRequest<Usuarios> = Usuarios.fetchRequest()
        do {
            let resultados = try contexto.fetch(fetchRequest)
            // print("Numero de tokens: \(resultados.count)")
            for res in resultados as [NSManagedObject] {
                let token = res.value(forKey: "accessToken")
                
                request.httpMethod = "GET"
                request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                request.setValue("Bearer \(token ?? "ouch")", forHTTPHeaderField: "Authorization")
            
            }
        }catch let error as NSError {
            print("Error al mostrar token", error.localizedDescription)
            
        }
        
        URLSession.shared.dataTask(with: request) { (data, response, _) in
            
            do {
                
                let json2 = try JSONSerialization.jsonObject(with: data!, options: .allowFragments)
                // print("AQUI ESTAN TUS REVISTAS FAVORITAS: \(json2)")
                DispatchQueue.main.async {
                    
                    let principalDictionary : NSDictionary = json2 as! NSDictionary
                    
                    for item in principalDictionary {
                        
                        let key : String = item.key as! String
                        let value = item.value
                        
                        if key == "message" {
                            
                            print("jwt expired")
                            self.favoritoID = "normal"
                            self.RenovarToken2()
                            
                        }
                        
                        if key == "elementos" {
                            let elementos : NSArray = principalDictionary["elementos"] as! NSArray
                            //  print("AQUI ESTAN TUS REVISTAS FAVORITAS: \(elementos)")
                            
                            for item in elementos {
                                
                                let revistaFavorita : NSDictionary = item as! NSDictionary
                                let revistaID : String = revistaFavorita["elemento_id"] as! String
                                self.arrayLikeID.append(revistaID)
                                
                                
                            }
                        }
                    }
                    
                    
                    Timer.scheduledTimer(withTimeInterval: 3, repeats: false) { (timer) in
                        self.isInFavoritesF()
                    }
                }
            }catch let error as NSError{
                
                DispatchQueue.main.async {
                    
                    print(error)
                    
                }
                
            }
            
        }.resume()
        
    }
   
    func RenovarToken2() {
        
        let fetchRequest1 : NSFetchRequest<Usuarios> = Usuarios.fetchRequest()
        guard let datos = URL(string: "https://testapi.ginmag.ga/v1/usuarios/renew") else { return }
        var request = URLRequest(url: datos)
        
        let contexto = conexion()
        let fetchRequest : NSFetchRequest<Usuarios> = Usuarios.fetchRequest()
        do {
            let resultados = try contexto.fetch(fetchRequest)
            // print("Numero de tokens: \(resultados.count)")
            for res in resultados as [NSManagedObject] {
                let refreshToken = res.value(forKey: "refToken")
                let parametros: [String : String] = [
                    
                    "refreshToken" : "\(refreshToken ?? "ouch")"
                    
                ]
                
                let body = try! JSONSerialization.data(withJSONObject: parametros)
                request.httpMethod = "POST"
                request.httpBody = body
                request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                
            }
        }catch let error as NSError {
            print("Error al mostrar token", error.localizedDescription)
            
        }
        
        URLSession.shared.dataTask(with: request) { (data, response, _) in
            
            do {
                
                let result = try contexto.fetch(fetchRequest1)
                let json = try JSONSerialization.jsonObject(with: data!, options: .allowFragments)
                
                DispatchQueue.main.async {
                    
                    let dictionaryJSON : NSDictionary = json as! NSDictionary
                    let newAccessToken  = dictionaryJSON["token"]
                    let resul = result[0] as NSManagedObject
                    resul.setValue(newAccessToken, forKey: "accessToken")
                    
                    do {
                        try contexto.save()
                        print("Exito al sobre escribir new accessToken")
                        
                        if self.favoritoID == "normal" {
                            self.GetFavoritosInicio()
                        }
                    }catch let error as NSError{
                        print("Error al guardar token", error.localizedDescription)
                    }
                    
                }
                
            }catch{
                
                print("Error")
                
            }
            
        }.resume()
    }
    
    
    func Favoritos() {
        
        let revista = RevistaInfoController.singleton
        guard let url = URL(string: "https://testapi.ginmag.ga/v1/favoritos/")
            else { return }
        var request = URLRequest(url: url)
        let contexto = conexion()
        let fetchRequest : NSFetchRequest<Usuarios> = Usuarios.fetchRequest()
        do {
            let resultados = try contexto.fetch(fetchRequest)
            // print("Numero de tokens: \(resultados.count)")
            for res in resultados as [NSManagedObject] {
                let token = res.value(forKey: "accessToken")
                
                let parametros : [String : String] = ["tipo" : self.tipoR, "id" : self.queryParamID!]
                
                let body = try! JSONSerialization.data(withJSONObject: parametros)
                request.httpMethod = "POST"
                request.httpBody = body
                request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                request.setValue("Bearer \(token ?? "ouch")", forHTTPHeaderField: "Authorization")
                
                
            }
        }catch let error as NSError {
            print("Error al mostrar token", error.localizedDescription)
            
        }
        
        URLSession.shared.dataTask(with: request) { (data, response, _) in
            
            do {
                
                let json = try JSONSerialization.jsonObject(with: data!, options: .allowFragments)
                print("FAVORITOS: \(json)")
                DispatchQueue.main.async {
                    
                    let dictionaryMessage : NSDictionary = json as! NSDictionary
                    for dic in dictionaryMessage {
                        
                        let key : String = dic.key as! String
                        let value : String = dic.value as! String
                        
                        if value == "jwt expired" {
                            
                            self.favoritoID = "FAVS"
                            UIView.animate(withDuration: 0.2, animations: {
                                self.loaderView.alpha = 0
                            })
                            // self.AlertErrorFavorite()
                            revista.errorAlert = "error"
                            self.IDfavoritos = false
                            self.emptyHeart.image = UIImage(named: "favorite_empty")
                            self.RenovarToken2()
                            
                        }else{
                            DispatchQueue.main.async {
                                
                                UIView.animate(withDuration: 0.2, animations: {
                                    self.loaderView.alpha = 0
                                })
                                revista.errorAlert = "no error"
                            }
                            
                            
                        }
                        
                    }
                    
                }
                
            }catch let error as NSError{
                
                DispatchQueue.main.async {
                    
                    
                }
                
                
            }
            
        }.resume()
        
    }
    
    
    func DeleteFavoritos() {
        
        let revista = RevistaInfoController.singleton
        guard let url = URL(string: "https://testapi.ginmag.ga/v1/favoritos/\(self.queryParamID!)")
            else { return }
        var request = URLRequest(url: url)
        let contexto = conexion()
        let fetchRequest : NSFetchRequest<Usuarios> = Usuarios.fetchRequest()
        do {
            let resultados = try contexto.fetch(fetchRequest)
            // print("Numero de tokens: \(resultados.count)")
            for res in resultados as [NSManagedObject] {
                let token = res.value(forKey: "accessToken")
                
                request.httpMethod = "DELETE"
                request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                request.setValue("Bearer \(token ?? "ouch")", forHTTPHeaderField: "Authorization")
                
                
            }
        }catch let error as NSError {
            print("Error al mostrar token", error.localizedDescription)
            
        }
        
        URLSession.shared.dataTask(with: request) { (data, response, _) in
            
            do {
                
                let json = try JSONSerialization.jsonObject(with: data!, options: .allowFragments)
                
                DispatchQueue.main.async {
                    
                    let dictionaryMessage : NSDictionary = json as! NSDictionary
                    
                    for dic in dictionaryMessage {
                        
                        let key : String = dic.key as! String
                        
                        if key == "description" {
                            
                            self.favoritoID = "NO FAVS"
                            UIView.animate(withDuration: 0.2, animations: {
                                self.loaderView.alpha = 0
                            })
                            //  self.AlertErrorFavorite()
                            revista.errorAlert = "error"
                            self.IDfavoritos = true
                            self.emptyHeart.image = UIImage(named: "favorite_full")
                            self.RenovarToken2()
                            print("Error al quitar de favoritos")
                            
                        }else{
                            
                            UIView.animate(withDuration: 0.2, animations: {
                                self.loaderView.alpha = 0
                            })
                            revista.errorAlert = "no error"
                            print("Exito al quitar de favoritos")
                            
                        }
                        
                    }
                    
                }
                
            }catch let error as NSError{
                
                DispatchQueue.main.async {
                    print(error)
                    
                }
                
                
            }
            
        }.resume()
        
    }
    
    func downloadImage(_ uri : String, inView: UIImageView) {
           let url = URL(string: uri)
           
           let task = URLSession.shared.dataTask(with: url!) {responseData,response,error in
               if error == nil {
                   if let data = responseData {
                       
                       DispatchQueue.main.async {
                           inView.image = UIImage(data: data)
                           
                           print("Fin del hilo imagen muestra")
                       }
                       
                   }else {
                       print("no data")
                   }
               }else{
                   print("error")
               }
           }
           task.resume()
       }
    
    
    func downloadImageNumerosAnteriores(_ uri : String, inView: UIImageView) {
        let url = URL(string: uri)
        
        let task = URLSession.shared.dataTask(with: url!) {responseData,response,error in
            if error == nil {
                if let data = responseData {
                    DispatchQueue.main.async {
                        inView.image = UIImage(data: data)
                    }
                }else {
                    print("no data")
                }
            }else{
                print("error")
            }
        }
        task.resume()
    }
    
    @IBAction func FavoritosAction(_ sender: Any) {
        let revista = RevistaInfoController.singleton
        revista.idViewDelegate = false
        if IDfavoritos == false {
            revista.idViewDelegate = true
            revista.isInFavorites = true
            UIView.animate(withDuration: 0.2, animations: {
                self.loaderView.alpha = 1
            })
            IDfavoritos = true
            self.emptyHeart.image = UIImage(named: "favorite_full")
            Favoritos()
            self.performSegue(withIdentifier: "ADDFAVORITE", sender: nil)
            
        }else{
            
            revista.idViewDelegate = true
            revista.isInFavorites = false
            UIView.animate(withDuration: 0.2, animations: {
                self.loaderView.alpha = 1
            })
            IDfavoritos = false
            self.emptyHeart.image = UIImage(named: "favorite_empty")
            DeleteFavoritos()
            self.performSegue(withIdentifier: "ADDFAVORITE", sender: nil)
            
        }
        
    }
    
    @objc func ClickEtiqueta(sender: UIButton) {
              let singeton = RevistaInfoController.singleton
              singeton.tagID = sender.tag
              print(singeton.tagID!)
             
           }
    
    @objc func ClickRevista2(sender:UIButton) {
        
        UIView.animate(withDuration: 0.2, animations: {
            self.loaderView.alpha = 1
        })
        self.gifLoader.loadGif(name: "loader_readee")
        let tagA = sender.tag
        self.tag2 = tagA
        
        self.arrayImagenes.forEach{ (imagen) in
            imagen.removeFromSuperview()
        }
        self.arrayLabelTit.forEach{ (label) in
            label.removeFromSuperview()
        }
        self.arrayLabelFecha.forEach{ (label2) in
            label2.removeFromSuperview()
        }
        self.ejex = 20
        self.ejeXtitulo = 28
        self.ejeXfecha = 20
        self.arrayEtiquet.removeAll()
        
        self.queryParamID = arrayIDsRevista2[tagA]
        self.urlImage2.removeAll()
        self.arrayFechas.removeAll()
        self.titulos2.removeAll()
        self.arrayEtiqueta2.removeAll()
        GetNumerosAnteriores()
        GetFavoritosInicio()
        principalScroll.setContentOffset(.zero, animated: true)

    }
    
    @IBAction func VeRevista(_ sender: Any) {
        self.performSegue(withIdentifier: "TOVIEWER", sender: nil)
    }
    
    @IBAction func VerYDescargar(_ sender: Any) {
        self.performSegue(withIdentifier: "TODESCARGAREVISTA", sender: nil)
    }
    
}
