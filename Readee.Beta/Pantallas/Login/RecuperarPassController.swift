//
//  RecuperarPassController.swift
//  Readee.Beta
//
//  Created by Pablo Luis Velazquez Zamudio on 22/05/20.
//  Copyright © 2020 P.Z.Scorpion. All rights reserved.
//

import UIKit

class RecuperarPassController: UIViewController {

    @IBOutlet weak var fondo: UIImageView!
    @IBOutlet weak var shadow: UIView!
    @IBOutlet weak var titulo: UILabel!
    @IBOutlet weak var correoField: UITextField!
    @IBOutlet weak var alertLabel: UILabel!
    @IBOutlet weak var recuperarPass: UIButton!
    @IBOutlet weak var contentView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        recuperarPass.layer.cornerRadius = 18
        contentView.layer.cornerRadius = 13
        
        let edgePan = UIScreenEdgePanGestureRecognizer(target: self, action: #selector(screenEdgeSwiped))
        edgePan.edges = .left
        view.addGestureRecognizer(edgePan)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        AppUtility.lockOrientation(.portrait)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
       AppUtility.lockOrientation(.all)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    
    
    func RecuperarContraseña(email: String) {
        
        guard let url = URL(string: "https://testapi.ginmag.ga/v1/usuarios/forgotpassword")
            else { return }
        let parametros : [String : String] = ["email" : email]
        let body = try! JSONSerialization.data(withJSONObject: parametros)
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = body
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        URLSession.shared.dataTask(with: request) { (data, response, _) in
            
            do {
                
                let json = try JSONSerialization.jsonObject(with: data!, options: .allowFragments)
                print(json)
                
                DispatchQueue.main.async {
                    
                    
                }
                
            }catch let error as NSError{
                
                DispatchQueue.main.async {
                    
                    self.alert()
                    
                }
                
                
            }
            
        }.resume()
        
    }
    
    func alert () {
        let alerta = UIAlertController(title: "", message: "Usuario incorrecto... por favor revise su información.", preferredStyle: .alert)
        let accion = UIAlertAction(title: "Ok", style: .default) { (_) in
            print("Click en el boton")
        }
        
        alerta.addAction(accion)
        present(alerta, animated: true, completion: nil)
    }
       
       
    @objc func screenEdgeSwiped(_ recognizer: UIScreenEdgePanGestureRecognizer) {
        if recognizer.state == .recognized {
            
            navigationController?.popViewController(animated: true)
            
        }
        
    }
    
    // MARK: - ACCIONES BOTONES
    
    @IBAction func RecuperarContraseña(_ sender: Any) {
        
        if correoField.text != "" {
            
            alertLabel.isHidden = true
            RecuperarContraseña(email: correoField.text!)
            
        }else{
            
            alertLabel.isHidden = false
            
        }
    }
    
    
} // AQUI TERMINA LA CLASE 
