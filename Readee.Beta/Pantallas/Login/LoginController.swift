//
//  LoginController.swift
//  Readee.Beta
//
//  Created by Pablo Luis Velazquez Zamudio on 22/05/20.
//  Copyright © 2020 P.Z.Scorpion. All rights reserved.
//

import UIKit
import CoreData
import SwiftGifOrigin
import Network
// MARK: - ESTRUCTURAS

struct Login : Codable {
    
    var token : String
    var refreshToken : String
    
}

class LoginController: UIViewController, NSFetchedResultsControllerDelegate, UITextFieldDelegate {

    // MARK: - ELEMENTOS GENERALES
    @IBOutlet weak var fondoImage: UIImageView!
    @IBOutlet weak var shadowView: UIView!
    
    //  MARK: - ELEMENTOS (CONTENT VIEW)
    @IBOutlet weak var gifLoader: UIImageView!
    @IBOutlet weak var logoImage: UIImageView!
    @IBOutlet weak var loaderView: UIView!
    @IBOutlet weak var contentLoaderView: UIView!

    // MARK: - ELEMNTOS (SUBCONTENTVIEW)
    @IBOutlet weak var subContentView: UIView!
    @IBOutlet weak var entradaLabel: UILabel!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var forgotButton: UIButton!
    @IBOutlet weak var entryButton: UIButton!
    @IBOutlet weak var registrarseButton: UIButton!
    @IBOutlet weak var ingreseUsuarioAlert: UILabel!
    @IBOutlet weak var ingresePassAlert: UILabel!
    @IBOutlet weak var segundoLabel: UILabel!
    
 
    // MARK: - VARIABLE SINGLETON
    
    static let singletonLogin = LoginController()
    
    // MARK: - VARIABLES GLOBALES
        
    var name : String!
    
    // MARK: - VARIABLES ID
    
    var IDsegue = 0
    
    // MARK: - FUNCION CORE DATA
    
    func conexion()-> NSManagedObjectContext {
        let delegate = UIApplication.shared.delegate as! AppDelegate
        return delegate.persistentContainer.viewContext
    }
    
    let networkMonitor = NWPathMonitor()
    
    // MARK: - FUNCIONES OVERRIDE
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        gifLoader.loadGif(name: "loader_readee")
        self.navigationController?.isNavigationBarHidden = true
        subContentView.layer.cornerRadius = 13
        entryButton.layer.cornerRadius = 25
        registrarseButton.layer.cornerRadius = 25
        self.loaderView.alpha = 0.85
        self.contentLoaderView.alpha = 1
        
        networkMonitor.pathUpdateHandler = { path in
            if path.status == .satisfied {
                print("Estás conectado a la red")
                DispatchQueue.main.async {
                    self.caducidadToken()
                }
            } else {
                print("No estás conectado a la red")
                DispatchQueue.main.async {
                    self.WifiAlert()
                    UIView.animate(withDuration: 0.4, animations: {
                        self.loaderView.alpha = 0
                        self.contentLoaderView.alpha = 0
                    })
                }
                
            }
        }
        
        let queue = DispatchQueue(label: "Network connectivity")
        networkMonitor.start(queue: queue)
        
        emailTextField.attributedPlaceholder = NSAttributedString(string: "Email o nombre de ususario", attributes: [NSAttributedString.Key.foregroundColor: UIColor.init(red: 121/255, green: 121/255, blue: 121/255, alpha: 1)])
        
        passwordTextField.attributedPlaceholder = NSAttributedString(string: "Contraseña", attributes: [NSAttributedString.Key.foregroundColor: UIColor.init(red: 121/255, green: 121/255, blue: 121/255, alpha: 1)])
        
        passwordTextField.delegate = self
        passwordTextField.returnKeyType = .done

//        UIView.animate(withDuration: 0.3, animations: {
//            self.loaderView.alpha = 0
//            self.contentLoaderView.alpha = 0
//        })

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        AppUtility.lockOrientation(.portrait)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
       AppUtility.lockOrientation(.all)
    }
    
    // MARK: - FUNCIONES GENERALES
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
          self.view.endEditing(true)
      }
    
    func emptyAlert () {
        let alerta = UIAlertController(title: "Alerta", message: "Por favor ingrese usuario y contraseña", preferredStyle: .alert)
        let accion = UIAlertAction(title: "Ok", style: .default) { (_) in
            print("Click en el boton")
        }
        
        alerta.addAction(accion)
        present(alerta, animated: true, completion: nil)
        
    }
    
    func alert () {
        let alerta = UIAlertController(title: "", message: "Usuario o contraseña incorrectos... por favor revise su información.", preferredStyle: .alert)
        let accion = UIAlertAction(title: "Ok", style: .default) { (_) in
            print("Click en el boton")
        }
        
        alerta.addAction(accion)
        present(alerta, animated: true, completion: nil)
    }
    
    func WifiAlert () {
        let alerta = UIAlertController(title: "¡Ups!", message: "No tienes conexion a internet, por favor conectate a una red.", preferredStyle: .alert)
        let accion = UIAlertAction(title: "Ok", style: .default) { (_) in
            print("Click en el boton")
        }
        
        alerta.addAction(accion)
        present(alerta, animated: true, completion: nil)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
      
        entrarBoton(self)
        
        return true
        
    }
    
    
    // MARK: - FUNCIONES LOGIN (TOKEN)
    
// Esta funcion es para borrar elmentos en la base de datos
    func autoDelete() {
        let contexto = conexion()
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Usuarios")
        let borrar = NSBatchDeleteRequest(fetchRequest: fetchRequest)
        
        do {
            try contexto.execute(borrar)
          //  print("Exito al borrar token")
        } catch let error as NSError{
            print("Error al eliminar tokens", error.localizedDescription)
        }
    }
    
    // Esta función me regresa el string que le pido
    func decode(jwtToken jwt: String) -> [String: Any] {
        let segments = jwt.components(separatedBy: ".")
        return decodeJWTPart(segments[1]) ?? [:]
    }
    // Esta funcion decodifica de base 64 a string
    func base64UrlDecode(_ value: String) -> Data? {
        var base64 = value
            .replacingOccurrences(of: "-", with: "+")
            .replacingOccurrences(of: "_", with: "/")
        let length = Double(base64.lengthOfBytes(using: String.Encoding.utf8))
        let requiredLength = 4 * ceil(length / 4.0)
        let paddingLength = requiredLength - length
        if paddingLength > 0 {
            let padding = "".padding(toLength: Int(paddingLength), withPad: "=", startingAt: 0)
            base64 = base64 + padding
        }
        return Data(base64Encoded: base64, options: .ignoreUnknownCharacters)
    }
    // Esta funcion convierte un string en un JSON
    func decodeJWTPart(_ value: String) -> [String: Any]? {
        guard let bodyData = base64UrlDecode(value),
            let json = try? JSONSerialization.jsonObject(with: bodyData, options: []), let payload = json as? [String: Any] else {
                return nil
        }
        return payload
    }
    
    func login(email: String, password: String) {
        
        let singleton = LoginController.singletonLogin
      //  let singletonRegistro = RegistroController.singleton
        guard let url = URL(string: "https://testapi.ginmag.ga/v1/usuarios/login")
            else { return }
        let parametros : [String : String] = ["email" : email, "password" : password]
        let body = try! JSONSerialization.data(withJSONObject: parametros)
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = body
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        URLSession.shared.dataTask(with: request) { (data, response, _) in
          
            do {
                
                let datos = try JSONDecoder().decode (Login.self, from: data!)
                
                DispatchQueue.main.async {
                    
                    if datos.token.isEmpty {
                        print("No hay token")
                    }else{
                        self.autoDelete()
                        print("Exito al sobre escribir token")
                    }
                    
                    let contexto = self.conexion()
                    let entidadUsuarios = NSEntityDescription.insertNewObject(forEntityName: "Usuarios", into: contexto) as! Usuarios
                    entidadUsuarios.accessToken = datos.token
                    entidadUsuarios.refToken = datos .refreshToken
                    do {
                        try contexto.save()
                        print("Exito al guardar token")
                    }catch let error as NSError{
                        print("Error al guardar token", error.localizedDescription)
                    }
                    
                    let decodeToken = self.decode(jwtToken: datos.token)
                    print("DETALLES DEL TOKEN: \(decodeToken)")
                    
                    for detail in decodeToken {
                        
                        let keys = detail.key
                        let values = detail.value
                        
                        if keys == "name" {
                            
                            let nombre : String = values as! String
                            singleton.name = nombre
        
                        }
                        
                    }
                
                  //  singletonRegistro.idLogin = true
                    
                    if singleton.IDsegue == 1 {
                    
                    UIView.animate(withDuration: 0.3, animations: {
                        self.loaderView.alpha = 0
                        self.contentLoaderView.alpha = 0
                    })
                    
                    self.performSegue(withIdentifier:"TOINICIO", sender:nil)
                    
                    }
                    
                }
                
            }catch let error as NSError{
                
                DispatchQueue.main.async {
                    print("Error", error.localizedDescription)
                    UIView.animate(withDuration: 0.3, animations: {
                        self.loaderView.alpha = 0
                        self.contentLoaderView.alpha = 0
                    })
                    self.alert()
                    
                }
                
                
            }
            
        }.resume()
        
    }
    
    func caducidadToken() {
        
       // let inicio = InicioController()
        
        UIView.animate(withDuration: 0.3, animations: {
            self.loaderView.alpha = 0.85
            self.contentLoaderView.alpha = 1
        })
        
        let singleton = LoginController.singletonLogin
         let contexto = self.conexion()
                    let fetchRequest : NSFetchRequest<Usuarios> = Usuarios.fetchRequest()
                    do {
                        let resultados = try contexto.fetch(fetchRequest)
                        
                        print("Numero de tokens: \(resultados.count)")
                        if resultados.count == 0 {
                            UIView.animate(withDuration: 0.3, animations: {
                                self.loaderView.alpha = 0
                                self.contentLoaderView.alpha = 0
                            })
                        }
                        for res in resultados as [NSManagedObject] {
                            let token = res.value(forKey: "accessToken")
                            let refreshToken = res.value(forKey: "refToken")
                            let dataToken = self.decode(jwtToken: "\(String(describing: token))")
                            let dataRefresh = self.decode(jwtToken: "\(String(describing: refreshToken))")
                            let tokenDictionary : NSDictionary = dataToken as NSDictionary
                            let refreshDictionary : NSDictionary = dataRefresh as NSDictionary
                            print("DATOS ACCESSS TOKEN: \(tokenDictionary)")
                            print("DATOS REFRESH TOKEN: \(refreshDictionary)")
                            let caducidad : Double = tokenDictionary["exp"] as! Double
                            let nombre : String = tokenDictionary["name"] as! String
                            let caducidadRefresh : Double = refreshDictionary["exp"] as! Double
                         //   print("TIPO DE VARIABLE: \(type(of: caducidad))")
                            
                            let timesStamp = NSDate().timeIntervalSince1970
                            
                        
                Timer.scheduledTimer(withTimeInterval: 0 , repeats: false){ (timer) in
                                        
                           if timesStamp < caducidad {
                            
                            UIView.animate(withDuration: 0.3, animations: {
                                self.loaderView.alpha = 0
                                self.contentLoaderView.alpha = 0
                            })
                            singleton.name = nombre
                            self.performSegue(withIdentifier: "TOINICIO", sender: nil)
                            
                           }else{
                            
                             print("El token expiro")
                            if timesStamp < caducidadRefresh {
                                
                            print("El refreshToken no ha expirado")
                            self.RenovarToken()
                                
                            }else{
                            
                                print("El refreshToken también murio.😵")
                                UIView.animate(withDuration: 0.3, animations: {
                                    self.loaderView.alpha = 0
                                    self.contentLoaderView.alpha = 0
                                })
                                
                                }
                            }
                        }
                        }
                    }catch let error as NSError {
                        print("Error al mostrar token", error.localizedDescription)
                        
                    }
        
    }
    
    func RenovarToken() {
        
        let singleton = LoginController.singletonLogin
        let fetchRequest1 : NSFetchRequest<Usuarios> = Usuarios.fetchRequest()
        guard let datos = URL(string: "https://testapi.ginmag.ga/v1/usuarios/renew") else { return }
            var request = URLRequest(url: datos)
            
            let contexto = conexion()
            let fetchRequest : NSFetchRequest<Usuarios> = Usuarios.fetchRequest()
        
            do {
                let resultados = try contexto.fetch(fetchRequest)
                // print("Numero de tokens: \(resultados.count)")
                for res in resultados as [NSManagedObject] {
                    let refreshToken = res.value(forKey: "refToken")
                    let parametros: [String : String] = [
                                      
                        "refreshToken" : "\(refreshToken ?? "ouch")"
                                      
                                  ]
    
                    let body = try! JSONSerialization.data(withJSONObject: parametros)
                    request.httpMethod = "POST"
                    request.httpBody = body
                    request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                    
                }
            }catch let error as NSError {
                print("Error al mostrar token", error.localizedDescription)
                
            }
            
            URLSession.shared.dataTask(with: request) { (data, response, _) in
        
              
                do {
                    
                    let result = try contexto.fetch(fetchRequest1)
                
                    let json = try JSONSerialization.jsonObject(with: data!, options: .allowFragments)
                    
                    DispatchQueue.main.async {
                        
                        let dictionaryJSON : NSDictionary = json as! NSDictionary
                        let newAccessToken  = dictionaryJSON["token"]
                        let decodeToken = self.decode(jwtToken: newAccessToken as! String)
                       // print("DETALLES DEL NUEVO ACCESS TOKEN: \(decodeToken)")
                        
                        for detail in decodeToken {
                            
                            let keys = detail.key
                            let values = detail.value
                            
                            if keys == "name" {
                                
                                let nombre : String = values as! String
                                singleton.name = nombre
                                
                            }
                            
                        }
                        
                        let resul = result[0] as NSManagedObject
                        resul.setValue(newAccessToken, forKey: "accessToken")
                        do {
                            try contexto.save()
                            print("Exito al guardar new accessToken")
                            self.performSegue(withIdentifier: "TOINICIO", sender: nil)
                            UIView.animate(withDuration: 0.3, animations: {
                                self.loaderView.alpha = 0
                                self.contentLoaderView.alpha = 0
                            })
                        }catch let error as NSError{
                            print("Error al guardar token", error.localizedDescription)
                        }
                        
                    }
                    
                }catch{
                    
                    print("Error")
                    
                }
                
            }.resume()
    }
    
    // MARK: - BOTONES GENERALES
    
    @IBAction func entrarBoton(_ sender: Any) {
        
        let singleton = LoginController.singletonLogin
        if emailTextField.text == "" {
            
            ingreseUsuarioAlert.isHidden = false
            
        }else{
            
            ingreseUsuarioAlert.isHidden = true
            
        }
        
        if passwordTextField.text == "" {
            
            ingresePassAlert.isHidden = false
            
        }else{
            
            singleton.IDsegue = 1
            networkMonitor.pathUpdateHandler = { path in
                if path.status == .satisfied {
                    print("Estás conectado a la red")
                    DispatchQueue.main.async {
                        self.login(email: self.emailTextField.text!, password: self.passwordTextField.text!)
                        self.ingreseUsuarioAlert.isHidden = true
                        self.ingresePassAlert.isHidden = true
                    UIView.animate(withDuration: 0.3, animations: {
                        self.loaderView.alpha = 0.8
                        self.contentLoaderView.alpha = 0.8
                    })
                        self.gifLoader.loadGif(name: "loader_readee")
                    }
                } else {
                    print("No estás conectado a la red")
                    DispatchQueue.main.async {
                        self.WifiAlert()
                        UIView.animate(withDuration: 0.4, animations: {
                            self.loaderView.alpha = 0
                            self.contentLoaderView.alpha = 0
                        })
                    }
                    
                }
            }
            
            let queue = DispatchQueue(label: "Network connectivity")
            networkMonitor.start(queue: queue)
            
        }
        
    }
    
    @IBAction func registroBoton(_ sender: Any) {
        
        self.performSegue(withIdentifier:"TOREGISTRO", sender:nil)
       
        
    }
    
    @IBAction func RecuperarContraseña(_ sender: Any) {
        
        self.performSegue(withIdentifier:"TOPASS", sender:nil)
        
        
    }
    
}

