//
//  PlanesController.swift
//  Readee.Beta
//
//  Created by Pablo Luis Velazquez Zamudio on 23/05/20.
//  Copyright © 2020 P.Z.Scorpion. All rights reserved.
//

import UIKit
import CoreData
import SwiftGifOrigin

class PlanesController: UIViewController {

    @IBOutlet weak var fondo: UIImageView!
    @IBOutlet weak var scrollPlan: UIScrollView!
    @IBOutlet weak var alphaView: UIView!
    @IBOutlet weak var subContentView: UIView!
    
    // MARK: - ELEMENTOS SUB CONTENT VIEW
    
    @IBOutlet weak var seleccionPlan: UILabel!
    @IBOutlet weak var creaPlan: UILabel!
    @IBOutlet weak var seleccionaPlanB: UILabel!
    @IBOutlet weak var eligeElPlan: UILabel!
    @IBOutlet weak var mensual: UIButton!
    @IBOutlet weak var anual: UIButton!
    @IBOutlet weak var obten2meses: UILabel!
    @IBOutlet weak var premium: UIButton!
    @IBOutlet weak var business: UIButton!
    @IBOutlet weak var unlimited: UIButton!
    @IBOutlet weak var dosMesesGratis: UILabel!
    @IBOutlet var marksCollection: [UIImageView]!
    @IBOutlet weak var precioMensual: UILabel!
    @IBOutlet weak var primerPrecio: UILabel!
    @IBOutlet weak var segundoPrecio: UILabel!
    @IBOutlet weak var tercerPrecio: UILabel!
    @IBOutlet weak var accesoRevistasA: UILabel!
    @IBOutlet weak var accesoRevistasB: UILabel!
    @IBOutlet weak var accesoPlayboy: UILabel!
    @IBOutlet weak var verDispositivos1: UILabel!
    @IBOutlet weak var verDispositivos2: UILabel!
    @IBOutlet weak var primerSlot: UILabel!
    @IBOutlet weak var segundoSlot: UILabel!
    @IBOutlet weak var tercerSlot: UILabel!
    @IBOutlet weak var iniciarMembresia: UIButton!
    @IBOutlet weak var loaderView: UIView!
    @IBOutlet weak var gifLoader: UIImageView!
    
     // MARK: - FUNCION CRUD CORE DATA
      
      func conexion()-> NSManagedObjectContext {
             let delegate = UIApplication.shared.delegate as! AppDelegate
             return delegate.persistentContainer.viewContext
         }
      
      // MARK: - SINGLETON VARIABLE
      
      static let singleton = PlanesController()
      
      // MARK: - VARIABLES GLOBALES
      
      var arrayPreciosAnual : [String] = Array()
      var arrayPrecioMensual : [String] = Array()
      var arrayIdSuscripcion : [String] = Array()
      var arraySlot : [String] = Array()
      var suscripcion : String!
      var total : String!
      var tipo : String!
      
      
      // MARK: - VARIABLES DE DISEÑO
      
      let pulsarBoton = UIColor.init(red: 60/255, green: 199/255, blue: 247/247, alpha: 1)
      
      override func viewDidLoad() {
          super.viewDidLoad()

          // MARK: - ACTIVAR FUNCIONALIDADES DE DISEÑO
          
          subContentView.layer.cornerRadius = 13
          mensual.layer.cornerRadius = 17
          anual.layer.cornerRadius = 17
          unlimited.layer.cornerRadius = 17
          business.layer.cornerRadius = 17
          premium.layer.cornerRadius = 17
          iniciarMembresia.layer.cornerRadius = 17
          mensual.backgroundColor = pulsarBoton
          seleccionPlan.adjustsFontSizeToFitWidth = true
          scrollPlan.contentSize = CGSize(width: 374, height: 1300)
         
          // MARK: - ACTIVAR SERVICOS API REST
          
          GetSuscripciones()
          
      }
      
      // MARK: - SERVICIOS API REST
      
      func GetSuscripciones() {
          
          guard let datos = URL(string: "https://testapi.ginmag.ga/v1/suscripciones") else { return }
              var request = URLRequest(url: datos)
      
                      request.setValue("application/json", forHTTPHeaderField: "Content-Type")

              
              URLSession.shared.dataTask(with: request) { (data, response, _) in
          
                  do {
                      
                      let json = try JSONSerialization.jsonObject(with: data!, options: .allowFragments)
                      print(json)
                      
                      DispatchQueue.main.async {
                          
                          let jso : NSArray = json as! NSArray
                          let plan = PlanesController.singleton
                          
                          var arrrayPreciosM : [String] = Array()
                          var arrayPreciosA : [String] = Array()
                          var arrayID : [String] = Array()
                          var arraySlots : [String] = Array()
                          
                          for item in jso {
                              
                              let dictionary : NSDictionary = item as! NSDictionary
                              
                              for dic in dictionary {
                                  
                                  let keys : String = dic.key as! String
                                  let values = dic.value
                                  
                                  if keys == "costo_mensual" {
                                      
                                      arrrayPreciosM.append("\(values)")
                                      plan.arrayPrecioMensual = arrrayPreciosM
                                      
                                      
                                  }
                                  
                                  if keys == "costo_anual" {
                                      
                                      arrayPreciosA.append("\(values)")
                                      plan.arrayPreciosAnual = arrayPreciosA
                                      // print(type(of: i))
                                      
                                  }
                                  
                                  if keys == "_id" {
                                      
                                      arrayID.append("\(values)")
                                      plan.arrayIdSuscripcion = arrayID
                                  }
                                  
                                  if keys == "slots" {
                                      
                                      arraySlots.append("\(values)")
                                      plan.arraySlot = arraySlots
                                      
                              
                                  }
                                  
                              }
                              
                          }
                         
                          self.primerSlot.text = arraySlots[2]
                          self.segundoSlot.text = arraySlots[1]
                          self.tercerSlot.text = arraySlots[0]
                          
                      }
                      
                  }catch{
                      
                      print("Error")
                      
                  }
                  
              }.resume()
          
      }
      
      
      func VentasTrial() {
      
          let reg = RegistroController.singleton
          let plan = PlanesController.singleton
          let generoA = reg.genero
          
          guard let datos = URL(string: "https://testapi.ginmag.ga/v1/ventas/trial") else { return }
          var request = URLRequest(url: datos)
          
          let contexto = conexion()
          let fetchRequest : NSFetchRequest<Usuarios> = Usuarios.fetchRequest()
          do {
              let resultados = try contexto.fetch(fetchRequest)
              for res in resultados as [NSManagedObject] {
                  let token = res.value(forKey: "accesToken")
                  
                  let parametros: [String : String] = [
                      
                      "suscripcion" : plan.suscripcion,
                      "total" : plan.total,
                      "tipo" : plan.tipo
                      
                  ]
                  
                  let body = try! JSONSerialization.data(withJSONObject: parametros)
                  request.httpMethod = "POST"
                  request.httpBody = body
                  request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                  request.setValue("Bearer \(token ?? "ouch")", forHTTPHeaderField: "Authorization")
                  
                  
              }
          }catch let error as NSError {
              print("Error al mostrar token", error.localizedDescription)
              
          }
          
          URLSession.shared.dataTask(with: request) { (data, response, _) in
      
              do {
                  
                  let json = try JSONSerialization.jsonObject(with: data!, options: .allowFragments)
                  print(json)
                  
                  DispatchQueue.main.async {
                      
                      UIView.animate(withDuration: 0.2, animations: {
                          self.loaderView.alpha = 0
                          
                      })
                      self.performSegue(withIdentifier: "TOHOME", sender: nil)
                      
                  }
                  
              }catch{
                  
                  print("Error")
                  
              }
              
          }.resume()
      }
      
      // MARK: - FUNCIONES CORE DATA
      
      func autoDelete() {
          
          let contexto = conexion()
          let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Usuarios")
          let borrar = NSBatchDeleteRequest(fetchRequest: fetchRequest)
          
          do {
              try contexto.execute(borrar)
          } catch let error as NSError{
              print("Error al eliminar tokens", error.localizedDescription)
          }
      }
      
      // MARK: - ACCIONES BOTONES
      
      @IBAction func PlanMensual(_ sender: Any) {
          
          let plan = PlanesController.singleton
          mensual.backgroundColor = pulsarBoton
          anual.backgroundColor = UIColor.white
          self.precioMensual.text = "Precio mensual"
          self.primerPrecio.text = "$\(plan.arrayPrecioMensual[2]).00"
          self.segundoPrecio.text = "$\(plan.arrayPrecioMensual[1]).00"
          self.tercerPrecio.text = "$\(plan.arrayPrecioMensual[0]).00"
          plan.tipo = "mesnual"
          
          
      }
      
      @IBAction func PlanAnual(_ sender: Any) {
          
          let plan = PlanesController.singleton
          mensual.backgroundColor = UIColor.white
          anual.backgroundColor = pulsarBoton
          self.precioMensual.text = "Precio anual"
          self.primerPrecio.text = "$\(plan.arrayPreciosAnual[2]).00"
          self.segundoPrecio.text = "$\(plan.arrayPreciosAnual[1]).00"
          self.tercerPrecio.text = "$\(plan.arrayPreciosAnual[0]).00"
          plan.tipo = "anual"
      }
      
      @IBAction func Premium(_ sender: Any) {
          
          let plan = PlanesController.singleton
          premium.backgroundColor = pulsarBoton
          business.backgroundColor = UIColor.white
          unlimited.backgroundColor = UIColor.white
          
          marksCollection[3].image = UIImage(named: "icon_check_active")
          marksCollection[12].image = UIImage(named: "icon_check_active")
          
          marksCollection[4].image = UIImage(named: "icon_check")
          marksCollection[7].image = UIImage(named: "icon_check")
          marksCollection[13].image = UIImage(named: "icon_check")
          marksCollection[5].image = UIImage(named: "icon_check")
          marksCollection[8].image = UIImage(named: "icon_check")
          marksCollection[11].image = UIImage(named: "icon_check")
          marksCollection[14].image = UIImage(named: "icon_check")
          
          let premium = plan.arrayIdSuscripcion[2]
          plan.suscripcion = premium
          plan.total = primerPrecio.text

      }
      
      @IBAction func Business(_ sender: Any) {
          
          let plan = PlanesController.singleton
          premium.backgroundColor = UIColor.white
          business.backgroundColor = pulsarBoton
          unlimited.backgroundColor = UIColor.white
          
          marksCollection[4].image = UIImage(named: "icon_check_active")
          marksCollection[7].image = UIImage(named: "icon_check_active")
          marksCollection[13].image = UIImage(named: "icon_check_active")
          
          marksCollection[5].image = UIImage(named: "icon_check")
          marksCollection[8].image = UIImage(named: "icon_check")
          marksCollection[11].image = UIImage(named: "icon_check")
          marksCollection[14].image = UIImage(named: "icon_check")
          marksCollection[3].image = UIImage(named: "icon_check")
          marksCollection[12].image = UIImage(named: "icon_check")
          
          let business = plan.arrayIdSuscripcion[1]
          plan.suscripcion = business
          plan.total = segundoPrecio.text
          
          
      }
      
      @IBAction func Unlimited(_ sender: Any) {
          
          let plan = PlanesController.singleton
          premium.backgroundColor = UIColor.white
          business.backgroundColor = UIColor.white
          unlimited.backgroundColor = pulsarBoton
          
          marksCollection[4].image = UIImage(named: "icon_check")
          marksCollection[7].image = UIImage(named: "icon_check")
          marksCollection[13].image = UIImage(named: "icon_check")
          marksCollection[3].image = UIImage(named: "icon_check")
          marksCollection[12].image = UIImage(named: "icon_check")
          
          marksCollection[5].image = UIImage(named: "icon_check_active")
          marksCollection[8].image = UIImage(named: "icon_check_active")
          marksCollection[11].image = UIImage(named: "icon_check_active")
          marksCollection[14].image = UIImage(named: "icon_check_active")
          
          let unlimited = plan.arrayIdSuscripcion[0]
          plan.suscripcion = unlimited
          plan.total = tercerPrecio.text
          
      }
      
      @IBAction func IniciarMembresia(_ sender: Any) {
          
          VentasTrial()
          UIView.animate(withDuration: 0.3, animations: {
              self.loaderView.alpha = 0.85
              
          })
          gifLoader.loadGif(name: "loader_readee")
      }

    
    
}
