//
//  PickerController.swift
//  Readee.Beta
//
//  Created by Pablo Luis Velazquez Zamudio on 22/05/20.
//  Copyright © 2020 P.Z.Scorpion. All rights reserved.
//

import UIKit

class PickerController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    
    static let singleton = PickerController()
    
    var pickerData3 : [String] = Array()
    var mesS : String!
    var diaS : String!
    var añoS : String!
    
    @IBOutlet weak var popWindow: UIView!
    @IBOutlet weak var datePicker: UIPickerView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var listo: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        popWindow.layer.cornerRadius = 10
        listo.layer.cornerRadius = 20
        datePicker.delegate = self
        datePicker.dataSource = self

    }
    
     func numberOfComponents(in pickerView: UIPickerView) -> Int {
           let pickerData = [["01","02","03","04", "05","06","07","08","09","10", "11", "12", "13", "14",  "15", "16", "17", "18", "19", "21", "22", "23", "24", "25", "26", "27","28","29","30","31",],["ENE", "FEB", "MAR", "ABR", "MAY", "JUN", "JUL", "AGO", "SEP", "OCT", "NOV", "DIC"],pickerData3]
           
           for i in stride(from: 2008, to: 1940, by: -1){
               
               pickerData3.append("\(i)")
               print("\(i)")
               
           }
           
           return pickerData.count
          }
          
          func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
            let pickerData = [["01","02","03","04", "05","06","07","08","09","10", "11", "12", "13", "14",  "15", "16", "17", "18", "19", "21", "22", "23", "24", "25", "26", "27","28","29","30","31",],["ENE", "FEB", "MAR", "ABR", "MAY", "JUN", "JUL", "AGO", "SEP", "OCT", "NOV", "DIC"],pickerData3]
           return pickerData[component].count
          }
       
       
         func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
             let pickerData = [["01","02","03","04", "05","06","07","08","09","10", "11", "12", "13", "14",  "15", "16", "17", "18", "19", "21", "22", "23", "24", "25", "26", "27","28","29","30","31",],["ENE", "FEB", "MAR", "ABR", "MAY", "JUN", "JUL", "AGO", "SEP", "OCT", "NOV", "DIC"],pickerData3]
             return pickerData[component][row]
         }
       
       func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
           let singleton = PickerController.singleton
            let pickerData = [["01","02","03","04", "05","06","07","08","09","10", "11", "12", "13", "14",  "15", "16", "17", "18", "19", "21", "22", "23", "24", "25", "26", "27","28","29","30","31",],["ENE", "FEB", "MAR", "ABR", "MAY", "JUN", "JUL", "AGO", "SEP", "OCT", "NOV", "DIC"],pickerData3]
         
           if pickerData[component] == pickerData[0] {
           dateLabel.text = "\(pickerData[0][row])/MM/AAAA"
               singleton.diaS = pickerData[0][row]
           }
           if pickerData[component] == pickerData[1] {
            dateLabel.text = "\(singleton.diaS ?? "DD")/\(pickerData[1][row])/AAAA"
               singleton.mesS = pickerData[1][row]
           }
           if pickerData[component] == pickerData[2] {
            dateLabel.text = "\(singleton.diaS ?? "DD")/\(singleton.mesS ?? "MM" )/\(pickerData[2][row])"
               singleton.añoS = pickerData[2][row]
           }
           
       }

}
