//
//  RegistroController.swift
//  Readee.Beta
//
//  Created by Pablo Luis Velazquez Zamudio on 22/05/20.
//  Copyright © 2020 P.Z.Scorpion. All rights reserved.
//

import UIKit

class RegistroController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var principalScroll: UIScrollView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var fechaNAcimiento: UIButton!
    @IBOutlet weak var arrowPicker: UIImageView!
    @IBOutlet weak var pickerHide: UIView!
    @IBOutlet weak var nombre: UITextField!
    @IBOutlet weak var apellidos: UITextField!
    @IBOutlet weak var correo: UITextField!
    @IBOutlet weak var confirmaCorreo: UITextField!
    @IBOutlet weak var contraseña: UITextField!
    @IBOutlet weak var confirmaPass: UITextField!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var subMenuGenero: UIView!
    @IBOutlet var generoCollection: [UIButton]!
    @IBOutlet weak var generoButton: UIButton!
    @IBOutlet var alertMarkCollection: [UIImageView]!
    @IBOutlet weak var loadrView: UIView!
    @IBOutlet weak var gifLoader: UIImageView!
    
    var genero : String!
        var fechaNacimiento : String!
        var año : String!
        var mes : String!
        var dia : String!
        
    // MARK: - VARIABLES PIRCKER VIEW
        
        var pickerData : [String] = [String]()
        let customWidth: CGFloat = 100
        let customHeight: CGFloat = 100

        var titDia = "--"
        var titMes = "--"
        var titAño = "----"
        
        // MARK: - VARIABLES ID
        
        var idPicker = false
        var idPickerFecha = false
        var idLogin = false
        var idField = 0

        // MARK: - SINGLETON VARIABLE
        
        static let singleton = RegistroController()
        
        // MARK: - FUNCIONES OVERRIDE
        
        override func viewDidLoad() {
            super.viewDidLoad()
            
            let reg = RegistroController.singleton
            
            nombre.delegate = self
            apellidos.delegate = self
            correo.delegate = self
            confirmaCorreo.delegate = self
            contraseña.delegate = self
            confirmaPass.delegate = self
            
            nombre.returnKeyType = .next
            apellidos.returnKeyType = .next
            correo.returnKeyType = .next
            confirmaCorreo.returnKeyType = .next
            contraseña.returnKeyType = .next
            confirmaPass.returnKeyType = .done
            
            principalScroll.contentSize = CGSize(width: 374, height: 1000)
            subMenuGenero.layer.cornerRadius = 7
            generoCollection.forEach { (button) in
                button.layer.cornerRadius = 7
            }

            pickerData = ["Hombre", "Mujer", "Prefiero no decir"]

            contentView.layer.cornerRadius = 13
            nextButton.layer.cornerRadius = 20
            fechaNAcimiento.layer.cornerRadius = 20
            
             nombre.attributedPlaceholder = NSAttributedString(string: "Tu(s) nombre(s)", attributes: [NSAttributedString.Key.foregroundColor: UIColor.init(red: 121/255, green: 121/255, blue: 121/255, alpha: 1)])
            
            apellidos.attributedPlaceholder = NSAttributedString(string: "Tus apellidos", attributes: [NSAttributedString.Key.foregroundColor: UIColor.init(red: 121/255, green: 121/255, blue: 121/255, alpha: 1)])
            
            correo.attributedPlaceholder = NSAttributedString(string: "Tu correo electrónico", attributes: [NSAttributedString.Key.foregroundColor: UIColor.init(red: 121/255, green: 121/255, blue: 121/255, alpha: 1)])
            
            confirmaCorreo.attributedPlaceholder = NSAttributedString(string: "Confirma tu correo electrónico", attributes: [NSAttributedString.Key.foregroundColor: UIColor.init(red: 121/255, green: 121/255, blue: 121/255, alpha: 1)])
            
            contraseña.attributedPlaceholder = NSAttributedString(string: "Tu contraseña", attributes: [NSAttributedString.Key.foregroundColor: UIColor.init(red: 121/255, green: 121/255, blue: 121/255, alpha: 1)])
            
             confirmaPass.attributedPlaceholder = NSAttributedString(string: "Confirma tu contraseña", attributes: [NSAttributedString.Key.foregroundColor: UIColor.init(red: 121/255, green: 121/255, blue: 121/255, alpha: 1)])
            
        }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        AppUtility.lockOrientation(.portrait)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
       AppUtility.lockOrientation(.all)
    }
        
        override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
                 self.view.endEditing(true)
             }
    
    @IBAction func BackPicker(_ segue: UIStoryboardSegue) {
        let singleton = PickerController.singleton
        fechaNAcimiento.setTitle("\(singleton.diaS ?? "DD")/\(singleton.mesS ?? "MM")/\(singleton.añoS ?? "AAAA")", for: .normal)
        
    }
        
        // MARK: - FUNCIONES GENERALES
        
        func textFieldShouldReturn(_ textField: UITextField) -> Bool {
            textField.resignFirstResponder()
          
            if idField == 0 {
                apellidos.becomeFirstResponder()
                idField = 1
            }else if idField == 1 {
                correo.becomeFirstResponder()
                idField = 2
            }else if idField == 2 {
                confirmaCorreo.becomeFirstResponder()
                idField = 3
            }else if idField == 3{
                contraseña.becomeFirstResponder()
                idField = 4
            }else if idField == 4 {
            confirmaPass.becomeFirstResponder()
                idField = 5
            }else if idField == 5 {
                
                func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
                    self.view.endEditing(true)
                }
                idField = 0
            }
            return true
            
        }
        
        
        // MARK: - SERVICIOS REST API
        
        func Signin(nombre: String, apellidos: String, email: String, contraseña: String) {
        
            var num = ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"]
            let reg = RegistroController.singleton
            let pick = PickerController.singleton
            let login = LoginController()
            let logsingleton = LoginController.singletonLogin
            let generoA = reg.genero
            
            if pick.mesS == "ENE" {
                pick.mesS = num[0]
            }else if pick.mesS == "FEB" {
                pick.mesS = num[1]
            }else if pick.mesS == "MAR" {
                pick.mesS = num[2]
            }else if pick.mesS == "ABR" {
                pick.mesS = num[3]
            }else if pick.mesS == "MAY" {
               pick.mesS = num[4]
            }else if pick.mesS == "JUN" {
                pick.mesS = num[5]
            }else if pick.mesS == "JUL" {
                pick.mesS = num[6]
            }else if pick.mesS == "AGO" {
                pick.mesS = num[7]
            }else if pick.mesS == "SEP" {
                pick.mesS = num[8]
            }else if pick.mesS == "OCT" {
                pick.mesS = num[9]
            }else if pick.mesS == "NOV" {
                pick.mesS = num[10]
            }else if pick.mesS == "DIC" {
                pick.mesS = num[11]
            }
            
            guard let datos = URL(string: "https://testapi.ginmag.ga/v1/usuarios/signin") else { return }
            var request = URLRequest(url: datos)
            let body: [String : String] = [
                
                          "nombre" : nombre,
                          "apellidos" : apellidos,
                          "email" : email,
                          "password" : contraseña,
                          "genero" : generoA!,
                          "fecha_nacimiento" : "\(pick.añoS!)-\(pick.mesS!)-\(pick.diaS!)"

                    ]
                
                    request.encodeParameters(parameters:body)
                    request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")

            
            URLSession.shared.dataTask(with: request) { (data, response, _) in
        
                do {
                    
                    let json = try JSONSerialization.jsonObject(with: data!, options: .allowFragments)
                    print(json)
                    
                    DispatchQueue.main.async {
                       
                        logsingleton.IDsegue = 2
                        
                        login.login(email: email, password: contraseña)
                        
                        Timer.scheduledTimer(withTimeInterval: 2 , repeats: false){ (timer) in
                            
                            if reg.idLogin == true {
                                
                                UIView.animate(withDuration: 0.3, animations: {
                                   self.loadrView.alpha = 0
                                    
                                })
                               self.performSegue(withIdentifier: "TOSELECCIONAPLAN", sender: nil)
                                
                            }
                        }
                        
                    }
                    
                }catch{
                    
                    print("Error")
                    
                }
                
            }.resume()
        }
        
        // MARK: - ACCIONES BOTONES
    
    @IBAction func siguientePaso(_ sender: Any) {
        
        if nombre.text == "" {
             alertMarkCollection[0].alpha = 1
        }else{
            alertMarkCollection[0].alpha = 0
        }
        if apellidos.text == "" {
            alertMarkCollection[1].alpha = 1
        }else{
            alertMarkCollection[1].alpha = 0
        }
        if correo.text == "" {
            alertMarkCollection[2].alpha = 1
        }else{
            
            if correo.text == confirmaCorreo.text {
                alertMarkCollection[2].alpha = 1
            }else{
               alertMarkCollection[2].alpha = 0
            }
            
        }
        
        if confirmaCorreo.text == "" {
            alertMarkCollection[3].alpha = 1
        }else{
            alertMarkCollection[3].alpha = 0
        }
        
        if contraseña.text == "" {
            alertMarkCollection[4].alpha = 1
        }else{
            
            if contraseña.text == confirmaPass.text {
                alertMarkCollection[4].alpha = 0
            }else{
                alertMarkCollection[4].alpha = 1
            }
            
        }
        
        if confirmaPass.text == "" {
            alertMarkCollection[5].alpha = 1
        }else{
            alertMarkCollection[5].alpha = 0
            
            if contraseña.text == confirmaPass.text {
                
                if nombre.text != "" {
                    
                    if apellidos.text != "" {
                        
                        UIView.animate(withDuration: 0.3, animations: {
                            self.loadrView.alpha = 0.85
                            
                        })
                        gifLoader.loadGif(name: "loader_readee")
                        Signin(nombre: nombre.text!, apellidos: apellidos.text!, email: correo.text!, contraseña: contraseña.text!)
                        
                    }
                    
                }
                
            }
            
        }
        
    }
        
        @IBAction func ocultarPickerFecha(_ sender: Any) {
            
            if idPickerFecha == false {
                self.performSegue(withIdentifier: "TOPICKER", sender: nil)
            }
        
        }
        
    
    @IBAction func OcularPicker(_ sender: Any) {
        
        if idPicker == false {
            
            UIView.animate(withDuration: 0.3, animations: {
                self.subMenuGenero.alpha = 1
                self.generoCollection.forEach { (button) in
                    button.isHidden = !button.isHidden
                }
                
            })
            idPicker = true
            UIView.animate(withDuration: 0.3, animations: {
                self.arrowPicker.transform = self.arrowPicker.transform.rotated(by: CGFloat(Double.pi / 2))
            })
            
            
        }else{
            
            idPicker = false
            UIView.animate(withDuration: 0.3, animations: {
                self.subMenuGenero.alpha = 0
                self.generoCollection.forEach { (button) in
                    button.isHidden = !button.isHidden
                }
                
            })
            UIView.animate(withDuration: 0.3, animations: {
                self.arrowPicker.transform = self.arrowPicker.transform.rotated(by: CGFloat(Double.pi / -2 ))
                
            })
            
        }
        
    }
        
        
        func numberOfComponents(in pickerView: UIPickerView) -> Int {
            return 1
        }
        
        func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
            return pickerData.count
        }
        
        func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
            return pickerData[row]
        }
        
        func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
            
            let reg = RegistroController.singleton
            print(pickerData[row])
            pickerHide.isHidden = false
          //  GeneroLabel.text = pickerData[row]
            reg.genero = pickerData[row]
           
        }
    
    @IBAction func hombreButton(_ sender: Any) {
        let singleton = RegistroController.singleton
        idPicker = false
        singleton.genero = "Hombre"
        generoButton.setTitle("Hombre", for: .normal)
        UIView.animate(withDuration: 0.3, animations: {
            self.subMenuGenero.alpha = 0
            self.generoCollection.forEach { (button) in
                button.isHidden = !button.isHidden
            }
            
        })
        UIView.animate(withDuration: 0.3, animations: {
            self.arrowPicker.transform = self.arrowPicker.transform.rotated(by: CGFloat(Double.pi / -2 ))
            
        })
    }
    
    @IBAction func mujerButton(_ sender: Any) {
        let singleton = RegistroController.singleton
        idPicker = false
        singleton.genero = "Mujer"
        generoButton.setTitle("Mujer", for: .normal)
        UIView.animate(withDuration: 0.3, animations: {
            self.subMenuGenero.alpha = 0
            self.generoCollection.forEach { (button) in
                button.isHidden = !button.isHidden
            }
            
        })
        UIView.animate(withDuration: 0.3, animations: {
            self.arrowPicker.transform = self.arrowPicker.transform.rotated(by: CGFloat(Double.pi / -2 ))
            
        })
    }
    
    @IBAction func prefieroNoDecir(_ sender: Any) {
        let singleton = RegistroController.singleton
        idPicker = false
        singleton.genero = "Prefiero no decir"
        generoButton.setTitle("Prefiero no decir", for: .normal)
        UIView.animate(withDuration: 0.3, animations: {
            self.subMenuGenero.alpha = 0
            self.generoCollection.forEach { (button) in
                button.isHidden = !button.isHidden
            }
            
        })
        UIView.animate(withDuration: 0.3, animations: {
            self.arrowPicker.transform = self.arrowPicker.transform.rotated(by: CGFloat(Double.pi / -2 ))
            
        })
    }
    
        
    }

    // MARK: - EXTENSIONES

    extension URLRequest {
      
      private func percentEscapeString(_ string: String) -> String {
        var characterSet = CharacterSet.alphanumerics
        characterSet.insert(charactersIn: "-._* ")
        
        return string
          .addingPercentEncoding(withAllowedCharacters: characterSet)!
          .replacingOccurrences(of: " ", with: "+")
          .replacingOccurrences(of: " ", with: "+", options: [], range: nil)
      }
      
      mutating func encodeParameters(parameters: [String : String]) {
        httpMethod = "POST"
        
        let parameterArray = parameters.map { (arg) -> String in
          let (key, value) = arg
          return "\(key)=\(self.percentEscapeString(value))"
        }
        
        httpBody = parameterArray.joined(separator: "&").data(using: String.Encoding.utf8)
      }
    }
