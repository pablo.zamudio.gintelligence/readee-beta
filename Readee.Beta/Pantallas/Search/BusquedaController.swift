//
//  BusquedaController.swift
//  Readee.Beta
//
//  Created by Pablo Luis Velazquez Zamudio on 25/05/20.
//  Copyright © 2020 P.Z.Scorpion. All rights reserved.
//

import UIKit

class BusquedaController: UIViewController, UISearchBarDelegate {

    static let singleton = BusquedaController()
    
    var ejex : CGFloat = 8
    var ejexCorner : CGFloat = 106.5
    var ejexLabel : CGFloat = 10
    var ejexPeriodicidad = 10
    var tag = 0
    
    var ejex2 : CGFloat = 8
    var ejexCorner2 : CGFloat = 111.5
    var ejexLabel2 : CGFloat = 10
    var ejexPeriodicidad2 = 10
    var tag2 = 0
    
    var imagenes : [UIImageView] = Array()
    var titulos : [UILabel] = Array()
    var fechas : [UILabel] = Array()
    var arrayID : [String] = Array()
    
    var imagenes2 : [UIImageView] = Array()
    var titulos2 : [UILabel] = Array()
    var periodicidades : [UILabel] = Array()
    var arrayID2 : [String] = Array()
    
    var arrayOfA : [NSArray] = Array()
    
    var tagID : Int!
    var tagID2 : Int!
    var path : String!
    var idView = false
    var tag3 : Int!
    var arrayEtiquetas : [String] = Array()
    
    let backgorundColor = UIColor(red: 33/255, green: 33/255, blue: 33/255, alpha: 1)
    
    @IBOutlet weak var principalScroll: UIScrollView!
    @IBOutlet weak var primerScroll: UIScrollView!
    @IBOutlet weak var segundoScroll: UIScrollView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var revistasLabel: UILabel!
    @IBOutlet weak var numerosRevistas: UILabel!
    @IBOutlet weak var emptyState: UIImageView!
    @IBOutlet weak var labelEmpty: UILabel!
    @IBOutlet weak var loaderView: UIView!
    @IBOutlet weak var gifLoader: UIImageView!
    
    // MARK: - FUNCIONES OVERRIDE
        
        override func viewDidLoad() {
            super.viewDidLoad()
            gifLoader.loadGif(name: "loader_readee")
            searchBar.delegate = self
            principalScroll.contentSize = CGSize(width: 375, height: segundoScroll.frame.maxY)
            self.searchBar.text = ""
            UIView.animate(withDuration: 0.3, animations: {
                self.loaderView?.alpha = 0
            })
        }
        
        func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
            self.path = searchBar.text
            self.principalScroll.alpha = 0
            searchBar.resignFirstResponder()
            self.arrayID2.removeAll()
            self.arrayID.removeAll()
            self.emptyState.alpha = 0
            self.labelEmpty.alpha = 0
            UIView.animate(withDuration: 0.3, animations: {
                self.loaderView.alpha = 0.85
            })
            gifLoader.loadGif(name: "loader_readee")
            self.imagenes.forEach{ (imagen) in
                imagen.removeFromSuperview()
            }
            self.titulos.forEach{ (label) in
                label.removeFromSuperview()
            }
            self.fechas.forEach{ (label2) in
                label2.removeFromSuperview()
            }
            self.imagenes2.forEach{ (imagen) in
                imagen.removeFromSuperview()
            }
            self.titulos2.forEach{ (label3) in
                label3.removeFromSuperview()
            }
            self.periodicidades.forEach{ (label4) in
                label4.removeFromSuperview()
            }
             self.ejex = 8
             self.ejexCorner = 106.5
             self.ejexLabel = 10
             self.ejexPeriodicidad = 10
             self.tag = 0
            
             self.ejex2 = 8
             self.ejexCorner2 = 111.5
             self.ejexLabel2 = 10
             self.ejexPeriodicidad2 = 10
             self.tag2 = 0
             self.GetBusqueda()
            
        }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        AppUtility.lockOrientation(.portrait)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
       AppUtility.lockOrientation(.all)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        
        if segue.identifier == "TOREVISTA" {
            let destino = segue.destination as! RevistaInfoController
            destino.queryParamID = self.arrayID2[self.tagID2]
        }
        if segue.identifier == "TONUMEROS" {
            let destino2 = segue.destination as! NumerosController
            destino2.path = self.arrayID[self.tagID]
        }
        
    }
    
    @IBAction func Back(segue: UIStoryboardSegue) {
        self.emptyState?.alpha = 0
        self.principalScroll?.alpha = 0
        self.tabBarController?.tabBar.alpha = 0
        UIView.animate(withDuration: 0.3, animations: {
            self.loaderView?.alpha = 1
        })
        gifLoader?.loadGif(name: "loader_readee")
        let revsita = RevistaInfoController.singleton
        print("ETIQUETAS: \(revsita.newArrayEtiquetas)")
        Timer.scheduledTimer(withTimeInterval: 5, repeats: false) { (timer) in
            
            print("SOLO UNA ETIQUETA: \(revsita.newArrayEtiquetas[revsita.tagID])")
            self.searchBar.text = revsita.newArrayEtiquetas[revsita.tagID]
            self.path = self.searchBar.text
            self.arrayID2.removeAll()
            self.arrayID.removeAll()
            self.emptyState.alpha = 0
            self.labelEmpty.alpha = 0
            self.imagenes.forEach{ (imagen) in
                imagen.removeFromSuperview()
            }
            self.titulos.forEach{ (label) in
                label.removeFromSuperview()
            }
            self.fechas.forEach{ (label2) in
                label2.removeFromSuperview()
            }
            self.imagenes2.forEach{ (imagen) in
                imagen.removeFromSuperview()
            }
            self.titulos2.forEach{ (label3) in
                label3.removeFromSuperview()
            }
            self.periodicidades.forEach{ (label4) in
                label4.removeFromSuperview()
            }
            self.ejex = 8
            self.ejexCorner = 106.5
            self.ejexLabel = 10
            self.ejexPeriodicidad = 10
            self.tag = 0
            
            self.ejex2 = 8
            self.ejexCorner2 = 111.5
            self.ejexLabel2 = 10
            self.ejexPeriodicidad2 = 10
            self.tag2 = 0
            self.GetBusqueda()
        }
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
        
        // MARK: - SERVICIOS REST API
    
    func GetBusqueda() {
        
        if  self.searchBar.text == "" {
            
            UIView.animate(withDuration: 0.3, animations: {
                self.loaderView.alpha = 0
                self.principalScroll.alpha = 1
            })
            
        }
        guard let url = URL(string: "https://testapi.ginmag.ga/v1/busqueda/\(self.path.percentEscapeString(path) )")
            
            else { return }
        
        print("URL: \(url)")
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        URLSession.shared.dataTask(with: request) { (data, response, _) in
            
            do {
                
                let json = try JSONSerialization.jsonObject(with: data!, options: .allowFragments)
                print(json)
                
                DispatchQueue.main.async {
                    
                    let diccionarioPrincipal : NSDictionary = json as! NSDictionary
                    
                    let numeros : NSArray = diccionarioPrincipal["numeros"] as! NSArray
                    let revistas : NSArray = diccionarioPrincipal["revistas"] as! NSArray
                    self.arrayOfA.append(numeros)
                    self.arrayOfA.append(revistas)
                    
                    if revistas == NSArray() {
                        self.revistasLabel.alpha = 0
                        self.primerScroll.alpha = 0
                        self.tabBarController?.tabBar.alpha = 1
                        self.numerosRevistas.frame = self.revistasLabel.frame
                        self.segundoScroll.frame = self.primerScroll.frame
                        
                    }else{
                        self.revistasLabel.alpha = 1
                        self.primerScroll.alpha = 1
                        self.tabBarController?.tabBar.alpha = 1
                        self.numerosRevistas.frame = CGRect(x: 8, y: 378, width: 258, height: 42)
                        self.segundoScroll.frame = CGRect(x: 0, y: 426, width: 375, height: 382)
                    }
                    if numeros == NSArray() {
                        self.numerosRevistas.alpha = 0
                        self.tabBarController?.tabBar.alpha = 1
                        self.principalScroll.contentSize = CGSize(width: 375, height: self.primerScroll.frame.maxY)
                        
                    }else{
                        self.numerosRevistas.alpha = 1
                        self.tabBarController?.tabBar.alpha = 1
                        self.principalScroll.contentSize = CGSize(width: 375, height: self.segundoScroll.frame.maxY - 40)
                        
                    }
                    
                    self.arrayOfA.forEach{ (array) in
                        if array == NSArray() {
                            self.principalScroll.alpha = 0
                            self.emptyState.alpha = 1
                            self.labelEmpty.alpha = 1
                            self.tabBarController?.tabBar.alpha = 1
                            UIView.animate(withDuration: 0.3, animations: {
                                self.loaderView.alpha = 0
                            })
                        }else{
                            
                            self.principalScroll.alpha = 1
                            self.emptyState.alpha = 0
                            self.labelEmpty.alpha = 0
                            self.tabBarController?.tabBar.alpha = 1
                            UIView.animate(withDuration: 0.3, animations: {
                                self.loaderView.alpha = 0
                            })
                            
                        }
                    }
                    
                    if revistas != NSArray() {
                        self.emptyState.alpha = 0
                        self.labelEmpty.alpha = 0
                        self.primerScroll.alpha = 1
                        self.tabBarController?.tabBar.alpha = 1
                        // print("AQUI ESTA LA PRIMER REVISTA: \(revistas)")
                        for item in revistas {
                            let subDiccionario : NSDictionary = item as! NSDictionary
                            for dic in subDiccionario {
                                let key : String = dic.key as! String
                                let value = dic.value
                                if key == "_id" {
                                    let id : String = value as! String
                                    self.arrayID.append(id)
                                    print("ARRAY ID1 :\(self.arrayID)")
                                }
                                if key == "titulo" {
                                    let titulo : String = value as! String
                                    let labelTitulo = UILabel(frame: CGRect(x: self.ejexLabel, y: 16, width: 166, height: 21))
                                    labelTitulo.text = titulo
                                    labelTitulo.textColor = UIColor.white
                                    labelTitulo.font = UIFont.boldSystemFont(ofSize: 15)
                                    labelTitulo.adjustsFontSizeToFitWidth = true
                                    self.ejexLabel = self.ejexLabel + 200
                                    self.primerScroll.addSubview(labelTitulo)
                                    self.titulos2.append(labelTitulo)
                                    
                                }
                                if key == "imagen" {
                                    let imagen : String = value as! String
                                    let numeroImg = UIImageView(frame: CGRect(x: self.ejex, y: 39, width: 171, height: 213))
                                    let botonImagen = UIButton(frame: CGRect(x: self.ejex, y: 39, width: 171, height: 213))
                                    let imagenEsquina2 = UIImageView(frame: CGRect(x: self.ejexCorner2, y: 164, width: 67.34, height: 87.82))
                                    imagenEsquina2.contentMode = .scaleToFill
                                    imagenEsquina2.image = UIImage(named: "Hoja2")
                                    self.downloadImage(imagen, inView: numeroImg)
                                    numeroImg.backgroundColor = self.backgorundColor
                                    botonImagen.tag = self.tag
                                    self.tag = self.tag + 1
                                    self.ejex = self.ejex + 200
                                    self.ejexCorner2 = self.ejexCorner2 + 200
                                    self.primerScroll.addSubview(numeroImg)
                                    self.primerScroll.addSubview(botonImagen)
                                    self.primerScroll.addSubview(imagenEsquina2)
                                    self.primerScroll.contentSize = CGSize(width: self.ejex, height: 290)
                                    self.imagenes2.append(numeroImg)
                                    botonImagen.addTarget(self, action: #selector(self.ClickRevista2), for: .touchUpInside)
                                    
                                }
                                if key == "periodicidad" {
                                    let periodicidad : String = value as! String
                                    let labelPeriodicidad = UILabel(frame: CGRect(x: self.ejexPeriodicidad, y: 259, width: 166, height: 21))
                                    labelPeriodicidad.text = "Publicación:\(periodicidad)"
                                    labelPeriodicidad.textColor = UIColor.white
                                    labelPeriodicidad.font = UIFont.boldSystemFont(ofSize: 15)
                                    labelPeriodicidad.adjustsFontSizeToFitWidth = true
                                    self.ejexPeriodicidad = self.ejexPeriodicidad + 200
                                    self.primerScroll.addSubview(labelPeriodicidad)
                                    self.periodicidades.append(labelPeriodicidad)
                                    
                                }
                            }
                            
                        }
                        self.revistasLabel.isHidden = false
                        UIView.animate(withDuration: 0.3, animations: {
                            self.loaderView.alpha = 0
                        })
                    }
                    
                    if numeros != NSArray() {
                        for it in numeros{
                            
                            let subDicc : NSDictionary = it as! NSDictionary
                            
                            for dic2 in subDicc {
                                
                                let subkey : String = dic2.key as! String
                                let subvalue = dic2.value
                                
                                let mes : Int = subDicc["mes"] as! Int
                                let año : Int = subDicc["year"] as! Int
                                if subkey == "_id" {
                                    let id2 : String = subvalue as! String
                                    self.arrayID2.append(id2)
                                }
                                if subkey == "titulo" {
                                    let titulo2 : String = subvalue as! String
                                    let labelTitulo2 = UILabel(frame: CGRect(x: self.ejexLabel2, y: 16, width: 166, height: 21))
                                    labelTitulo2.text = titulo2
                                    labelTitulo2.textColor = UIColor.white
                                    labelTitulo2.font = UIFont.boldSystemFont(ofSize: 15)
                                    labelTitulo2.adjustsFontSizeToFitWidth = true
                                    self.ejexLabel2 = self.ejexLabel2 + 200
                                    self.segundoScroll.addSubview(labelTitulo2)
                                    self.titulos.append(labelTitulo2)
                                    
                                    let labelfecha = UILabel(frame: CGRect(x: self.ejexPeriodicidad2, y: 254, width: 166, height: 21))
                                    var mesRevista = ""
                                    var añoRevista = ""
                                    if mes == 1 {
                                        mesRevista = "enero"
                                    }else if mes == 2 {
                                        mesRevista = "febrero"
                                    }else if mes == 3 {
                                        mesRevista = "marzo"
                                    }else if mes == 4 {
                                        mesRevista = "abril"
                                    }else if mes == 5 {
                                        mesRevista = "mayo"
                                    }else if mes == 6 {
                                        mesRevista = "junio"
                                    }else if mes == 7 {
                                        mesRevista = "julio"
                                    }else if mes == 8 {
                                        mesRevista = "agosto"
                                    }else if mes == 9 {
                                        mesRevista = "septiembre"
                                    }else if mes == 10 {
                                        mesRevista = "octubre"
                                    }else if mes == 11 {
                                        mesRevista = "noviembre"
                                    }else if mes == 12 {
                                        mesRevista = "diciembre"
                                    }
                                    
                                    
                                    if año == 2015 {
                                        añoRevista = "2015"
                                    }else if año == 2016 {
                                        añoRevista = "2016"
                                    }else if año == 2017 {
                                        añoRevista = "2017"
                                    }else if año == 2018 {
                                        añoRevista = "2018"
                                    }else if año == 2019 {
                                        añoRevista = "2019"
                                    }else if año == 2020 {
                                        añoRevista = "2020"
                                    }else if año == 2021 {
                                        añoRevista = "2021"
                                    }else if año == 2022 {
                                        añoRevista = "2022"
                                    }else if año == 2023 {
                                        añoRevista = "2023"
                                    }else if año == 2024 {
                                        añoRevista = "2024"
                                    }else if año == 2025 {
                                        añoRevista = "2025"
                                    }else if año == 2026 {
                                        añoRevista = "2026"
                                    }else if año == 2027 {
                                        añoRevista = "2027"
                                    }else if año == 2028 {
                                        añoRevista = "2028"
                                    }else if año == 2029 {
                                        añoRevista = "2029"
                                    }else if año == 2030 {
                                        añoRevista = "2030"
                                    }
                                    
                                    let fechaRevista = "\(mesRevista) \(añoRevista)"
                                    labelfecha.text = fechaRevista
                                    labelfecha.textColor = UIColor.white
                                    labelfecha.font = UIFont.boldSystemFont(ofSize: 15)
                                    labelfecha.adjustsFontSizeToFitWidth = true
                                    self.ejexPeriodicidad2 = self.ejexPeriodicidad2 + 200
                                    self.segundoScroll.addSubview(labelfecha)
                                    self.fechas.append(labelfecha)
                                    
                                    
                                }
                                if subkey == "imagen" {
                                    let imagen2: String = subvalue as! String
                                    let numeroImg2 = UIImageView(frame: CGRect(x: self.ejex2, y: 39, width: 166, height: 208))
                                    let botonImagen2 = UIButton(frame: CGRect(x: self.ejex2, y: 39, width: 166, height: 208))
                                    let imagenEsquina = UIImageView(frame: CGRect(x: self.ejexCorner, y: 159, width: 67.34, height: 87.82))
                                    imagenEsquina.contentMode = .scaleToFill
                                    imagenEsquina.image = UIImage(named: "Hoja2")
                                    self.downloadImage2(imagen2, inView: numeroImg2)
                                    numeroImg2.backgroundColor = self.backgorundColor
                                    self.ejex2 = self.ejex2 + 200
                                    self.ejexCorner = self.ejexCorner + 200
                                    botonImagen2.tag = self.tag2
                                    self.tag2 = self.tag2 + 1
                                    self.segundoScroll.addSubview(numeroImg2)
                                    self.segundoScroll.addSubview(botonImagen2)
                                    self.segundoScroll.addSubview(imagenEsquina)
                                    self.segundoScroll.contentSize = CGSize(width: self.ejex2, height: self.segundoScroll.frame.height)
                                    self.imagenes.append(numeroImg2)
                                    botonImagen2.addTarget(self, action: #selector(self.ClickRevista), for: .touchUpInside)
                                }
                                if subkey == "periodicidad" {
                                    
                                    let periodcidad2 : String = subvalue as! String
                                    
                                }
                                
                            }
                            
                        }
                        self.tabBarController?.tabBar.alpha = 1
                        self.numerosRevistas.isHidden = false
                        UIView.animate(withDuration: 0.3, animations: {
                            self.loaderView.alpha = 0
                            
                        })
                        
                    }
                }
                
            }catch let error as NSError{
                
                DispatchQueue.main.async {
                    print(error)
                }
            }
        }.resume()
        
    }
    
    func downloadImage(_ uri : String, inView: UIImageView) {
        let url = URL(string: uri)
        
        let task = URLSession.shared.dataTask(with: url!) {responseData,response,error in
            if error == nil {
                if let data = responseData {
                    
                    DispatchQueue.main.async {
                        inView.image = UIImage(data: data)
                        
                        print("Fin del hilo imagen revista")
                    }
                    
                }else {
                    print("no data")
                }
            }else{
                print("error")
            }
        }
        task.resume()
    }
        
    
    func downloadImage2(_ uri : String, inView: UIImageView) {
        let url = URL(string: uri)
        
        let task = URLSession.shared.dataTask(with: url!) {responseData,response,error in
            if error == nil {
                if let data = responseData {
                    
                    DispatchQueue.main.async {
                        inView.image = UIImage(data: data)
                        
                        print("Fin del hilo imagen números")
                    }
                    
                }else {
                    print("no data")
                }
            }else{
                print("error")
            }
        }
        task.resume()
    }
        
        // MARK: - ACCIONES DE LOS BOTONES
    
    @objc func ClickRevista(sender: UIButton) {
        
        print(sender.tag)
        self.tagID2 = sender.tag
        self.performSegue(withIdentifier: "TOREVISTA", sender: nil)
        
    }
    
    @objc func ClickRevista2(sender: UIButton) {
        
        print(sender.tag)
        self.tagID = sender.tag
        self.performSegue(withIdentifier: "TONUMEROS", sender: nil)
        
    }
        
    }

extension String {
    
    mutating func percentEscapeString(_ string: String) -> String {
        var characterSet = CharacterSet.alphanumerics
        characterSet.insert(charactersIn: "-._* ")
        
        return string
            .addingPercentEncoding(withAllowedCharacters: characterSet)!
            .replacingOccurrences(of: " ", with: "%20")
        //.replacingOccurrences(of: " ", with: "+", options: [], range: nil)
    }
    
}




