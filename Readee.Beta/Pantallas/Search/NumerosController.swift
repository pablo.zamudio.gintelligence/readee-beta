//
//  NumerosController.swift
//  Readee.Beta
//
//  Created by Pablo Luis Velazquez Zamudio on 25/05/20.
//  Copyright © 2020 P.Z.Scorpion. All rights reserved.
//

import UIKit

class NumerosController: UIViewController {

    var path: String!
    var ejex : CGFloat = 12
    var ejexCorner : CGFloat = 108.5
    var ejeyCorner : CGFloat = 244
    var ejey : CGFloat = 112
    var tag = 0
    
    var ejextitulo : CGFloat = 14
    var ejeytitulo : CGFloat = 77
    
    var ejexfecha : CGFloat = 14
    var ejeyfecha : CGFloat = 337
    
    var arrayID : [String] = Array()
    var tagID : Int!
    
    let backGroundGray = UIColor(red: 33/255, green: 33/255, blue: 33/255, alpha: 1)
    
    @IBOutlet weak var numerosLabel: UILabel!
    @IBOutlet weak var scrollPrincipal: UIScrollView!
    
       // MARK: - FUNCIONES OVERRIDE
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        GetTodosNumeros()
        self.tag = 0
        let edgePan = UIScreenEdgePanGestureRecognizer(target: self, action: #selector(screenEdgeSwiped))
        edgePan.edges = .left
        view.addGestureRecognizer(edgePan)
        
    }
    
   override func viewDidAppear(_ animated: Bool) {
       super.viewDidAppear(animated)
       AppUtility.lockOrientation(.portrait)
   }
   
   override func viewWillDisappear(_ animated: Bool) {
       super.viewWillDisappear(animated)
      AppUtility.lockOrientation(.all)
   }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destino = segue.destination as! RevistaInfoController
        
        if segue.identifier == "TOREVISTAINFO" {
            destino.queryParamID = self.arrayID[self.tagID]
            
        }
    }
    
    @objc func screenEdgeSwiped(_ recognizer: UIScreenEdgePanGestureRecognizer) {
        navigationController?.popViewController(animated: true)
    }
    
    func GetTodosNumeros() {
        
        guard let url = URL(string: "https://testapi.ginmag.ga/v1/revistas/\(self.path ?? "ouch")/numeros")
            
            else { return }
        print("URL: \(url)")
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        URLSession.shared.dataTask(with: request) { (data, response, _) in
            
            do {
                
                let json = try JSONSerialization.jsonObject(with: data!, options: .allowFragments)
                print(json)
                
                DispatchQueue.main.async {
                    
                    let arrayNumeros : NSArray = json as! NSArray
                    
                    for item in arrayNumeros {
                        
                        let revista : NSDictionary = item as! NSDictionary
                        print("REVISTA: \(revista)")
                        
                        let mes : Int = revista["mes"] as! Int
                        let año : Int = revista["year"] as! Int
                        
                        for elemento in revista {
                            
                            let key : String = elemento.key as! String
                            let value = elemento.value
                            
                            if key == "_id" {
                                let id : String = value as! String
                                self.arrayID.append(id)
                                
                            }
                            
                            if key == "titulo" {
                                let titulo : String = value as! String
                                let tituloLabel = UILabel(frame: CGRect(x: self.ejextitulo, y: self.ejeytitulo, width: 158, height: 35))
                                let fechaLabel = UILabel(frame: CGRect(x: self.ejexfecha, y: self.ejeyfecha, width: 158, height: 35))
                                var mesRevista = ""
                                var añoRevista = ""
                                if mes == 1 {
                                    mesRevista = "enero"
                                }else if mes == 2 {
                                    mesRevista = "febrero"
                                }else if mes == 3 {
                                    mesRevista = "marzo"
                                }else if mes == 4 {
                                    mesRevista = "abril"
                                }else if mes == 5 {
                                    mesRevista = "mayo"
                                }else if mes == 6 {
                                    mesRevista = "junio"
                                }else if mes == 7 {
                                    mesRevista = "julio"
                                }else if mes == 8 {
                                    mesRevista = "agosto"
                                }else if mes == 9 {
                                    mesRevista = "septiembre"
                                }else if mes == 10 {
                                    mesRevista = "octubre"
                                }else if mes == 11 {
                                    mesRevista = "noviembre"
                                }else if mes == 12 {
                                    mesRevista = "diciembre"
                                }
                                
                                
                                if año == 2015 {
                                    añoRevista = "2015"
                                }else if año == 2016 {
                                    añoRevista = "2016"
                                }else if año == 2017 {
                                    añoRevista = "2017"
                                }else if año == 2018 {
                                    añoRevista = "2018"
                                }else if año == 2019 {
                                    añoRevista = "2019"
                                }else if año == 2020 {
                                    añoRevista = "2020"
                                }else if año == 2021 {
                                    añoRevista = "2021"
                                }else if año == 2022 {
                                    añoRevista = "2022"
                                }else if año == 2023 {
                                    añoRevista = "2023"
                                }else if año == 2024 {
                                    añoRevista = "2024"
                                }else if año == 2025 {
                                    añoRevista = "2025"
                                }else if año == 2026 {
                                    añoRevista = "2026"
                                }else if año == 2027 {
                                    añoRevista = "2027"
                                }else if año == 2028 {
                                    añoRevista = "2028"
                                }else if año == 2029 {
                                    añoRevista = "2029"
                                }else if año == 2030 {
                                    añoRevista = "2030"
                                }
                                
                                let fechaRevista = "\(mesRevista) \(añoRevista)"
                                fechaLabel.text = fechaRevista
                                fechaLabel.numberOfLines  = 2
                                fechaLabel.textColor = UIColor.white
                                fechaLabel.font = UIFont.boldSystemFont(ofSize: 15)
                                fechaLabel.adjustsFontSizeToFitWidth = true
                                self.scrollPrincipal.addSubview(fechaLabel)
                                
                                tituloLabel.text = titulo
                                tituloLabel.textColor = UIColor.white
                                tituloLabel.font = UIFont.boldSystemFont(ofSize: 15)
                                tituloLabel.adjustsFontSizeToFitWidth = true
                                self.scrollPrincipal.addSubview(tituloLabel)
                                
                                let neweje : CGFloat = self.view.frame.width - 172
                                if self.ejextitulo == neweje {
                                    self.ejextitulo = 14
                                    self.ejeytitulo = self.ejeytitulo + 330
                                    
                                }else{
                                    self.ejextitulo = self.ejextitulo + 228
                                }
                                
                                if self.ejexfecha == neweje {
                                    self.ejexfecha = 14
                                    self.ejeyfecha = self.ejeyfecha + 330
                                }else{
                                    self.ejexfecha = self.ejexfecha + 228
                                }
                            }
                            
                            if key == "imagen" {
                                let imagen : String = value as! String
                                let imgNumero = UIImageView(frame: CGRect(x: self.ejex, y: self.ejey, width: 164, height: 220))
                                let botonImage = UIButton(frame: CGRect(x: self.ejex, y: self.ejey, width: 164, height: 220))
                                let imagenEsquina2 = UIImageView(frame: CGRect(x: self.ejexCorner, y: self.ejeyCorner, width: 67.34, height: 87.82))
                                imagenEsquina2.contentMode = .scaleToFill
                                imagenEsquina2.image = UIImage(named: "Hoja2")
                                imgNumero.backgroundColor = self.backGroundGray
                                self.downloadImage(imagen, inView: imgNumero)
                                self.scrollPrincipal.addSubview(imgNumero)
                                self.scrollPrincipal.addSubview(botonImage)
                                self.scrollPrincipal.addSubview(imagenEsquina2)
                                let newEjex : CGFloat = self.view.frame.width - 176
                                if self.ejex == newEjex {
                                    self.ejex = 12
                                    self.ejexCorner = 108.5
                                    self.ejey = self.ejey + 330
                                    self.ejeyCorner = self.ejeyCorner + 330
                                }else{
                                    self.ejex = self.ejex + 226
                                    self.ejexCorner = self.ejexCorner + 226
                                }
                                
                                botonImage.tag = self.tag
                                self.tag = self.tag + 1
                                self.scrollPrincipal.contentSize = CGSize(width: 375, height: imgNumero.frame.maxY + 20)
                                botonImage.addTarget(self, action: #selector(self.ClickRevista), for: .touchUpInside)
                                
                            }
                            
                        }
                        
                    }
                    
                }
                
            }catch let error as NSError{
                
                DispatchQueue.main.async {
                    print(error)
                }
            }
        }.resume()
        
    }

    
    func downloadImage(_ uri : String, inView: UIImageView) {
        let url = URL(string: uri)
        
        let task = URLSession.shared.dataTask(with: url!) {responseData,response,error in
            if error == nil {
                if let data = responseData {
                    
                    DispatchQueue.main.async {
                        inView.image = UIImage(data: data)
                        
                        print("Fin del hilo imagen muestra")
                    }
                    
                }else {
                    print("no data")
                }
            }else{
                print("error")
            }
        }
        task.resume()
    }
    
    @objc func ClickRevista(sender: UIButton) {
        
        print(sender.tag)
        self.tagID = sender.tag
        self.performSegue(withIdentifier: "TOREVISTAINFO", sender: nil)
        
    }
}
