//
//  PDFViewerController.swift
//  Readee.Beta
//
//  Created by Pablo Luis Velazquez Zamudio on 25/05/20.
//  Copyright © 2020 P.Z.Scorpion. All rights reserved.
//

import UIKit
import PDFKit

class PDFViewerController: UIViewController {

    var thumbnailView = PDFThumbnailView()
    var outlineButton = UIButton()
    var dismissButton = UIButton()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //  LoadPDF()
        setupThumbnailView()
        setupDismissButton()
        let pdfView = PDFView(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height))
        let fileURL = Bundle.main.url(forResource: "playboy", withExtension: "pdf")
        view.addSubview(pdfView)
        pdfView.displayDirection = .horizontal
        pdfView.usePageViewController(true)
        pdfView.autoScales = true
        pdfView.document = PDFDocument(url: fileURL!)
        thumbnailView.pdfView = pdfView
        thumbnailView.backgroundColor = UIColor(displayP3Red: 179/255, green: 179/255, blue: 179/255, alpha: 0.5)
        thumbnailView.layoutMode = .horizontal
        thumbnailView.thumbnailSize = CGSize(width: 80, height: 150)
        thumbnailView.contentInset = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
        view.addSubview(thumbnailView)
        let touch = UITapGestureRecognizer(target: self, action: #selector(toggleTools))
        pdfView.addGestureRecognizer(touch)
        
        dismissButton = UIButton(frame: CGRect(x: 10, y: 30, width: 40, height: 40))
        dismissButton.layer.cornerRadius = dismissButton.frame.width/2
        dismissButton.setTitle("<", for: .normal)
        dismissButton.titleLabel?.font = UIFont(name: "AppleSDGothicNeo-Bold", size: 30)
        dismissButton.titleEdgeInsets = UIEdgeInsets(top: 5, left: 0, bottom: 0, right: 0)
        dismissButton.setTitleColor(.white, for: .normal)
        dismissButton.backgroundColor = UIColor.init(red: 66/255, green: 66/255, blue: 66/255, alpha: 0.7)
        dismissButton.alpha = 0.7
        view.addSubview(dismissButton)
        dismissButton.addTarget(self, action: #selector(BackToMagazine), for: .touchUpInside)
        pdfView.backgroundColor = UIColor.init(red: 33/255, green: 33/255, blue: 33/255, alpha: 1)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        AppUtility.lockOrientation(.portrait)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
       AppUtility.lockOrientation(.all)
    }
    
    override func viewDidLayoutSubviews() {
        let pdfView = PDFView(frame: self.view.bounds)
        super.viewDidLayoutSubviews()
        pdfView.frame = view.safeAreaLayoutGuide.layoutFrame
        let thumbanilHeight: CGFloat = 120
        thumbnailView.translatesAutoresizingMaskIntoConstraints = false
        view.addConstraintsWithFormat(format: "H:|[v0]|", views: thumbnailView)
        view.addConstraintsWithFormat(format: "V:[v0(\(thumbanilHeight))]|", views: thumbnailView)
    }
    
    @objc func toggleTools() {
        if outlineButton.alpha != 0 {
            UIView.animate(withDuration: 0.3, animations: {
                self.outlineButton.alpha = 0
                self.thumbnailView.alpha = 0
                self.dismissButton.alpha = 0
            }, completion: nil)
        } else {
            UIView.animate(withDuration: 0.3, animations: {
                self.outlineButton.alpha = 1
                self.thumbnailView.alpha = 1
                self.dismissButton.alpha = 1
            }, completion: nil)
        }
    }
        
    
    private func setupOutlineButton() {
        outlineButton = UIButton(frame: CGRect(x: view.frame.maxX - 90, y: 45, width: 60, height: 60))
        outlineButton.layer.cornerRadius = outlineButton.frame.width/2
        outlineButton.setTitle("三", for: .normal)
        outlineButton.titleLabel?.font = UIFont(name: "AppleSDGothicNeo-Bold", size: 30)
        outlineButton.titleEdgeInsets = UIEdgeInsets(top: 5, left: 0, bottom: 0, right: 0)
        outlineButton.setTitleColor(.white, for: .normal)
        outlineButton.backgroundColor = .black
        outlineButton.alpha = 0.8
        view.addSubview(outlineButton)
        //outlineButton.addTarget(self, action: #selector(toggleOutline(sender:)), for: .touchUpInside)
    }
    
    private func setupDismissButton() {
        dismissButton = UIButton(frame: CGRect(x: 30, y: 45, width: 60, height: 60))
        dismissButton.layer.cornerRadius = dismissButton.frame.width/2
        dismissButton.setTitle("<", for: .normal)
        dismissButton.titleLabel?.font = UIFont(name: "AppleSDGothicNeo-Bold", size: 30)
        dismissButton.titleEdgeInsets = UIEdgeInsets(top: 5, left: 0, bottom: 0, right: 0)
        dismissButton.setTitleColor(.white, for: .normal)
        dismissButton.backgroundColor = .black
        dismissButton.alpha = 0.8
        view.addSubview(dismissButton)
        dismissButton.addTarget(self, action: #selector(BackToMagazine), for: .touchUpInside)
    }
        
    
    private func setupThumbnailView() {
        let pdfView = PDFView(frame: self.view.bounds)
        thumbnailView.pdfView = pdfView
        thumbnailView.backgroundColor = UIColor(displayP3Red: 179/255, green: 179/255, blue: 179/255, alpha: 0.5)
        thumbnailView.layoutMode = .horizontal
        thumbnailView.thumbnailSize = CGSize(width: 80, height: 100)
        thumbnailView.contentInset = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
        view.addSubview(thumbnailView)
    }
    
    func LoadPDF() {
        
        let fileURL = Bundle.main.url(forResource: "playboy", withExtension: "pdf")
        //Convertimos el archivo a NSData
        let dataAsNs = NSData.init(contentsOf: fileURL!)
        //Lo convertimos a un texto legible.
        let stringOfDataNS = String(data: dataAsNs as! Data, encoding: String.Encoding.utf8)
        //Imprimimos ambos casos:
        //   print("data loaded as NSData: \(dataAsNs)")
        print("data loaded as NSData to string : \(stringOfDataNS)")
        //Luego, como NSData puede ser representado como un arreglo de bytes, lo representamos.
        var arrayOfBytes = [UInt8](dataAsNs as! Data)
        print("data loaded as byte array: \(arrayOfBytes)")
        //Este arreglo de bytes hemos de convertir de vuelta a NSData.
        //Convirtamos el arreglo de bytes a NSData.
        var dataAsNsAgain = NSData(bytes: &arrayOfBytes, length: arrayOfBytes.count)
        //Transformemoslo de nuevo a un texto legible.
        let stringOfDataNSAgain = String(data: dataAsNsAgain as! Data, encoding: String.Encoding.utf8)
        //E imprimamos para ver que nada pasó:
        print("data loaded as NSData -Again-: \(dataAsNsAgain)")
        print("data loaded as NSData -Again- ToString: \(stringOfDataNSAgain)")
        
        
    }
    
    @IBAction func BackButton(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    @objc func BackToMagazine(sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
}

extension UIView {
    func addConstraintsWithFormat(format: String, views: UIView...) {
        var viewsDictionary = [String: UIView]()
        for (index, view) in views.enumerated() {
            let key = "v\(index)"
            view.translatesAutoresizingMaskIntoConstraints = false
            viewsDictionary[key] = view
        }
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: format, options: NSLayoutConstraint.FormatOptions(), metrics: nil, views: viewsDictionary))
    }
}
